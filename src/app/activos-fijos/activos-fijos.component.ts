import { Component, OnInit, ViewChild } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UtilityService } from '../utility.service';
import { ActivoFijo } from '../models/admin/activo-fijo/activo-fijo';
declare var $: any;

@Component({
  selector: 'app-activos-fijos',
  templateUrl: './activos-fijos.component.html',
  styleUrls: ['./activos-fijos.component.scss']
})
export class ActivosFijosComponent implements OnInit {

  activoFijo: ActivoFijo;

  // -------------- Array para llenar la tabla principal
  arrayActivos = new MatTableDataSource<ActivoFijo>();
  arrayTipoActivo: any;

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'codigo','nombre', 'tipoActivo', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.activoFijo = new ActivoFijo();
    this.arrayTipoActivo = [{vl: 'CP', op: 'Computador Portatil'},{vl: 'CE', op: 'Computador Escritorio'},
                            {vl: 'DV', op: 'Dispositivo de Video'},{vl: 'DR', op: 'Dispositivo de Red'},
                            {vl: 'DA', op: 'Dispositivo de Audio'},{vl: 'IM', op: 'Impresora'},
                            {vl: 'SW', op: 'Software'},{vl: 'SC', op: 'SIM Card'},
                            {vl: 'VE', op: 'Vestuario'},{vl: 'OT', op: 'Otros'}];
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayActivos.paginator = this.paginator;
    // ---------- Sort
    this.arrayActivos.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getActivosFijos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayActivos.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.activoFijo = new ActivoFijo();
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    this.apiServices.get('administracion/getActivoFijo/'+id)
    .subscribe((resp: ActivoFijo) => {
      this.activoFijo = resp;
      $('.loading').addClass('hide-loading');
      $('#modalActivo').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.activoFijo, 'administracion/saveActivoFijo')
      .subscribe((resp: ActivoFijo) => {
        this.activoFijo = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('.loading').addClass('hide-loading');
        $('#modalActivo').modal('hide');
        this.get();
      }, (err) => this.util.messageError(err));
    }
  }

  validate(): boolean {
    const cd = this.activoFijo.codigo != '';
    const nm = this.activoFijo.nombre != '';
    const es = this.activoFijo.estado != '';

    return cd && nm && es;
  }

  exportExcel() {
    if (this.arrayActivos.data != undefined && this.arrayActivos.data.length > 0)
      this.util.exportAsExcelFile(this.arrayActivos.data, 'ActivosFijos');
  }

  applyFilter(filterValue: string) {
    this.arrayActivos.filter = filterValue.trim().toLowerCase();
  }

}
