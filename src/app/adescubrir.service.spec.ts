import { TestBed } from '@angular/core/testing';

import { AdescubrirService } from './adescubrir.service';

describe('AdescubrirService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdescubrirService = TestBed.get(AdescubrirService);
    expect(service).toBeTruthy();
  });
});
