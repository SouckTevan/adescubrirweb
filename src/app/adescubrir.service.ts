import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuarioSesion } from './models/usuarioSesion/usuario-sesion';

@Injectable({
  providedIn: 'root'
})
export class AdescubrirService {

  private usuarioSesion: UsuarioSesion;
  private loggedIn: boolean = false;
  private token = '';

  set _usuarioSesion(value: any) { this.usuarioSesion = value; }
 
  get _usuarioSesion(): any { return this.usuarioSesion; }

  setLoggedIn(value: boolean) { this.loggedIn = value; }
 
  isLoggedIn(): boolean { return this.loggedIn; }

  constructor(private http: HttpClient) {}

  public get(ruta: string) {
    const headers = new HttpHeaders().set('Authorization', this.getToken());
    
    return this.http.get<any>(environment.apiUrl + ruta, {headers});
  }

  downloadFileById(ruta: string): Observable<any>{
    const headers = new HttpHeaders().set('Authorization', this.getToken());

		return this.http.get(environment.apiUrl + ruta, {headers, responseType: 'blob'});
  }

  downloadFile(ruta: string): Observable<any>{
    // let body = JSON.stringify(data);

    // const httpOptions = {
      const headers = new HttpHeaders().set('Authorization', this.getToken()).set('usuario', ''+this._usuarioSesion.id);
        // 'Authorization': this.getToken(),
        // 'usuario': JSON.stringify(this._usuarioSesion)
    // };

		return this.http.get(environment.apiUrl + ruta, {headers, responseType: 'blob'});
  }

  public post(data: any, ruta: string) {
    let body = JSON.stringify(data);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Authorization': this.getToken(),
        'usuario': ''+this._usuarioSesion.id
      })
    };
    
    return this.http.post(environment.apiUrl + ruta, body, httpOptions);
  }

  public postByData(data: any, ruta: string) {
    let body = JSON.stringify(data);

      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Authorization': this.getToken(),
        'usuario': "1"
      })
    
    return this.http.post(environment.apiUrl + ruta, body, {headers, responseType: 'blob'});
  }

  public postWithFile(ruta: string, data: any) {
    let ht = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
        'Authorization': this.getToken(),
        'usuario': ''
      })
    }  
    return this.http.post(environment.apiUrl + ruta, data, ht);
  }

  generateToken(req) {
    return this.http.post(environment.apiUrl+'administracion/generateToken', req, {responseType: 'text' as 'json'});
  }

  setToken(token: string) {
    this.token = 'Bearer '+token;
  }

  getToken() {
    return this.token;
  }
}
