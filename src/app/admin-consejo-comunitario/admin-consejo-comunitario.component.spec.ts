import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConsejoComunitarioComponent } from './admin-consejo-comunitario.component';

describe('AdminConsejoComunitarioComponent', () => {
  let component: AdminConsejoComunitarioComponent;
  let fixture: ComponentFixture<AdminConsejoComunitarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminConsejoComunitarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConsejoComunitarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
