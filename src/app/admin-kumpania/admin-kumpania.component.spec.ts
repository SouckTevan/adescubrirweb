import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminKumpaniaComponent } from './admin-kumpania.component';

describe('AdminKumpaniaComponent', () => {
  let component: AdminKumpaniaComponent;
  let fixture: ComponentFixture<AdminKumpaniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminKumpaniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminKumpaniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
