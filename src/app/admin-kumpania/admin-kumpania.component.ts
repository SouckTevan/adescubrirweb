import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Kumpania } from '../models/eventos/kumpania/kumpania';
declare var $: any;

@Component({
  selector: 'app-admin-kumpania',
  templateUrl: './admin-kumpania.component.html',
  styleUrls: ['./admin-kumpania.component.scss']
})
export class AdminKumpaniaComponent implements OnInit {

  departamento = 0;
  arrayData = new MatTableDataSource<Kumpania>();
  kumpania: Kumpania = new Kumpania();
  _disabled = false;
  showCols = ['id','estado','nombre','dane', 'accion'];

  @ViewChild('municipioDatos', {static: true}) municipioDatos: SelectGeneralComponent;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiService: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.reset();
    this.get();

    // ---------- Paginador
    this.arrayData.paginator = this.paginator;
    // ---------- Sort
    this.arrayData.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.arrayData.filter = filterValue.trim().toLowerCase();
  }

  changeSelectGeneral(id: any, table: string): void {
    switch(table) {
      case 'datos':
        this.departamento = id;
        this.municipioDatos.onChangePadre(id);
        this.municipioDatos._val = this.kumpania.idMunicipio;
        break;
    }
  }

  get(): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get('administracion/getKumpanias')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayData.data = res;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  open(id: number): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get('administracion/getKumpania/'+id)
    .subscribe((res) => {
      this.kumpania = res;
      $('.loading').addClass('hide-loading');
      $('#modalKumpania').modal('show');
    }, err => this.util.messageError(err));
  }

  save(): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validar()) {
      this.apiService.post(this.kumpania, 'administracion/saveKumpania')
      .subscribe((res: Kumpania) => {
        this.kumpania = res;
        this.util.notification('success','Kumpania','Datos Guardados!');
        $('#modalKumpania').modal('hide');
        this.get();
        $('.loading').addClass('hide-loading');
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning','Validación','Los campos marcados con (*) son obligatorios');
    }
  }

  reset(): void {
    this.departamento = 0;
    this.kumpania = new Kumpania();
  }

  validar(): boolean {
    let nombre = this.kumpania.nombre != '',
    dane  = this.kumpania.dane != '',
    estado = this.kumpania.estado != '';

    return nombre && dane && estado;
  }
}
