import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminResguardoComponent } from './admin-resguardo.component';

describe('AdminResguardoComponent', () => {
  let component: AdminResguardoComponent;
  let fixture: ComponentFixture<AdminResguardoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminResguardoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminResguardoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
