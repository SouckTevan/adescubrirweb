import { Component, OnInit, ViewChild } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { Resguardo } from '../models/eventos/resguardo/resguardo';
declare var $: any;

@Component({
  selector: 'app-admin-resguardo',
  templateUrl: './admin-resguardo.component.html',
  styleUrls: ['./admin-resguardo.component.scss']
})
export class AdminResguardoComponent implements OnInit {

  departamento = 0;
  arrayData = new MatTableDataSource<Resguardo>();
  resguardo: Resguardo = new Resguardo();
  _disabled = false;
  showCols = ['id','estado','nombre','dane', 'municipio', 'accion'];

  @ViewChild('municipioDatos', {static: true}) municipioDatos: SelectGeneralComponent;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiService: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.reset();
    this.get();

    // ---------- Paginador
    this.arrayData.paginator = this.paginator;
    // ---------- Sort
    this.arrayData.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.arrayData.filter = filterValue.trim().toLowerCase();
  }

  changeSelectGeneral(id: any, table: string): void {
    switch(table) {
      case 'datos':
        this.departamento = id;
        this.municipioDatos.onChangePadre(id);
        this.municipioDatos._val = this.resguardo.idMunicipio;
        break;
    }
  }

  get(): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get('administracion/getResguardos')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayData.data = res;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  open(id: number): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get('administracion/getResguardo/'+id)
    .subscribe((res) => {
      this.resguardo = res;
      this.changeSelectGeneral(this.resguardo.municipio.departamento_id, 'datos');
      $('.loading').addClass('hide-loading');
      $('#modalResguardo').modal('show');
    }, err => this.util.messageError(err));
  }

  save(): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validar()) {
      this.apiService.post(this.resguardo, 'administracion/saveResguardo')
      .subscribe((res: Resguardo) => {
        this.resguardo = res;
        this.util.notification('success','Resguardo','Datos Guardados!');
        $('#modalResguardo').modal('hide');
        this.get();
        $('.loading').addClass('hide-loading');
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning','Validación','Los campos marcados con (*) son obligatorios');
    }
  }

  reset(): void {
    this.departamento = 0;
    this.municipioDatos.reset();
    this.resguardo = new Resguardo();
  }

  validar(): boolean {
    let nombre = this.resguardo.nombre != '',
    dane  = this.resguardo.dane != '',
    estado = this.resguardo.estado != '',
    municipio = this.resguardo.idMunicipio != 0;

    return nombre && dane && estado && municipio;
  }
}
