import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { Rol } from '../models/admin/security/rol/rol';
import { Opcion } from '../models/admin/security/opcion/opcion';
import { RolOpcion } from '../models/admin/security/rol-opcion/rol-opcion';
declare var $: any;

@Component({
  selector: 'app-admin-rol',
  templateUrl: './admin-rol.component.html',
  styleUrls: ['./admin-rol.component.scss']
})
export class AdminRolComponent implements OnInit {

  arrayData = new MatTableDataSource<Rol>();
  arrayDataOpt = new MatTableDataSource<Opcion>();
  dataContratos = [];
  rol = new Rol();
  opcion = new Opcion();
  showCols = ['id','estado','nombre','accion'];
  showColsOpt = ['id','nombre','accion'];
  isRol = true;

  searchAllOp = '';
  searchCurrOp = '';

  dataOptions = [];
  dataCurrentOptions = [];
  optionsTemp = [];
  currentDirection = 'right';
  DIR_RIGHT = 'right';
  DIR_LEFT = 'left';

  title: string = 'Roles';

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginatorOp: MatPaginator;
  @ViewChild(MatSort, {static: true}) sortOp: MatSort;

  constructor(private apiService: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.get('administracion/getRoles');
    this.jquery();
     // ---------- Paginador
    this.arrayData.paginator = this.paginator;
    this.arrayDataOpt.paginator = this.paginatorOp;
     // ---------- Sort
    this.arrayData.sort = this.sort;
    this.arrayDataOpt.sort = this.sortOp;
  }

  jquery() {
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip({
        sanitizeFn: function (content) {return content;}
      });
    })
  }

  applyFilter(filterValue: string) {
    if (this.isRol)
      this.arrayData.filter = filterValue.trim().toLowerCase();
    else
      this.arrayDataOpt.filter = filterValue.trim().toLowerCase();
  }

  get(api: string): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get(api)
    .subscribe((res: Rol[]) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      if (this.isRol) {
        this.arrayData.data = res;
      }else {
        this.arrayDataOpt.data = res;
      }
      $('.loading').addClass('hide-loading');
    }, err => {
      this.util.messageError(err);
    })
  }

  getContratos() {
    this.apiService.get('/administracion/getNombresContratosPorEstado/A')
    .subscribe((res) => {
      this.dataContratos = res;
    })
  }

  open(ruta: string): void {
    $('.nav-pills-modal a[href="#rol"]').tab('show');
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.get(ruta)
    .subscribe((res: Rol) => {
      $('.loading').addClass('hide-loading');
      if (this.isRol) {
        this.rol = res;
        $('#modalRol').modal('show');
        this.getContratos();
        this.getOptions();
      }else {
        this.opcion = res;
        $('#modalOpcion').modal('show');
      }
    }, err => {
      this.util.messageError(err);
    })
  }

  getOptions() {
    this.dataCurrentOptions = [];
    this.apiService.get('administracion/getOpciones')
      .subscribe((res) => {
        this.dataOptions = res;
        if (this.rol.opciones != null && this.rol.opciones.length > 0) {
          for (const curr of this.rol.opciones) {
            for (const all of this.dataOptions) {
              if (curr.opcion === all.nombre) {
                this.dataCurrentOptions.push(all);
                let index = this.dataOptions.indexOf(all);
                this.dataOptions.splice(index,1);
              }
            }
          }
        }
        $('#modalRol').modal('show');
      },
      (err) => {
        this.util.messageError(err);
      });
  }

  reset() {
    if (this.isRol) {
      this.rol = new Rol();
      $('.nav-pills-modal a[href="#rol"]').tab('show');
      this.getContratos();
      this.getOptions();
    }else {
      this.opcion = new Opcion();
    }
    this.title = (this.isRol)?'Roles':'Opciones';
  }

  moveAllOptions(direction: string) {
    if (direction === this.DIR_RIGHT) {
      for (const itm of this.dataOptions) {
        this.dataCurrentOptions.push(itm);
      }
      this.dataOptions = [];
    }else if (direction === this.DIR_LEFT) { // ---- Move option(s) to
      for (const itm of this.dataCurrentOptions) {
        this.dataOptions.push(itm);
      }
      this.dataCurrentOptions = [];
    }
  }

  addToMoveOption(opt: any, direction: string, elem: any) {
    this.currentDirection = direction;
    let index = this.optionsTemp.indexOf(opt);
    if (index === -1) {
      elem.target.classList.add('selected-li');
      this.optionsTemp.push(opt);
    }else {
      elem.target.classList.remove('selected-li');
      this.optionsTemp.splice(index, 1);
    }
  }

  moveOption(direction: string) {
    if (this.currentDirection === direction) {
      for (const opt of this.optionsTemp) {
        if (direction === this.DIR_RIGHT) { // --- Add Option(s)
          this.dataCurrentOptions.push(opt);
          this.dataOptions = this.dataOptions.filter((dt) => dt.id !== opt.id);
        }else if (direction === this.DIR_LEFT) { // ---- Remove Option(s)
          this.dataOptions.push(opt);
          this.dataCurrentOptions = this.dataCurrentOptions.filter((dt) => dt.id !== opt.id);
        }
      }
      this.optionsTemp = [];
    }
  }

  save(api: string) {
    this.rol.opciones = [];
    let obj: any;
    if (this.isRol) {
      for (const op of this.dataCurrentOptions) {
        let rolOp = new RolOpcion();
        rolOp.opcion = op.nombre;
        this.rol.opciones.push(rolOp);
      }
      obj = this.rol;
    }else {
      obj = this.opcion;
    }

    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    this.apiService.post(obj,api)
    .subscribe((res: any) => {
      if (res.id !== 0) {
        $('#modalRol, #modalOpcion').modal('hide');
        this.util.notification('success','', (this.isRol ? 'Rol guardado': 'Opción guardada')+' con exito!');
        if (this.isRol)
          this.get('administracion/getRoles');
        else
          this.get('administracion/getOpciones');
        $('.loading').addClass('hide-loading');
      }
    }, err => {
      this.util.messageError(err);
    });
  }
}
