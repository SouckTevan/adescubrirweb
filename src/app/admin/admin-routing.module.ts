import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProveedoresComponent } from '../proveedores/proveedores.component';
import { ContratoComponent } from '../contrato/contrato.component';
import { ActivosFijosComponent } from '../activos-fijos/activos-fijos.component';
import { ConceptosComponent } from '../conceptos/conceptos.component';
import { EstadosComponent } from '../estados/estados.component';
import { UsuariosComponent } from '../usuarios/usuarios.component';
import { AdminRolComponent } from '../admin-rol/admin-rol.component';
import { AdminKumpaniaComponent } from '../admin-kumpania/admin-kumpania.component';
import { AdminResguardoComponent } from '../admin-resguardo/admin-resguardo.component';
import { AdminConsejoComunitarioComponent } from '../admin-consejo-comunitario/admin-consejo-comunitario.component';
import { AuthGuard } from '../auth.guard';
import { TiposConceptoComponent } from '../tipos-concepto/tipos-concepto.component';
import { TiposEventoComponent } from '../tipos-evento/tipos-evento.component';
import { TiposProveedorComponent } from '../tipos-proveedor/tipos-proveedor.component';
import { TiposTarifaComponent } from '../tipos-tarifa/tipos-tarifa.component';
import { TiposContactoComponent } from '../tipos-contacto/tipos-contacto.component';
import { TiposDocumentoComponent } from '../tipos-documento/tipos-documento.component';
import { BancosComponent } from '../bancos/bancos.component';
import { RangoNumeracionComponent } from '../rango-numeracion/rango-numeracion.component';
import { TipoDocumentoContableComponent } from '../tipo-documento-contable/tipo-documento-contable.component';
import { IndicadorRetencionComponent } from '../indicador-retencion/indicador-retencion.component';
import { MediosPagoComponent } from '../medios-pago/medios-pago.component';

const routes: Routes = [
  {path: 'proveedores', component: ProveedoresComponent, canActivate: [AuthGuard]},
  {path: 'contrato', component: ContratoComponent, canActivate: [AuthGuard]},
  {path: 'activos', component: ActivosFijosComponent, canActivate: [AuthGuard]},
  {path: 'conceptos', component: ConceptosComponent, canActivate: [AuthGuard]},
  {path: 'estados', component: EstadosComponent, canActivate: [AuthGuard]},
  {path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard]},
  {path: 'roles', component: AdminRolComponent, canActivate: [AuthGuard]},
  {path: 'kumpania', component: AdminKumpaniaComponent, canActivate: [AuthGuard]},
  {path: 'resguardo', component: AdminResguardoComponent, canActivate: [AuthGuard]},
  {path: 'consejo', component: AdminConsejoComunitarioComponent, canActivate: [AuthGuard]},
  {path: 'tipoConcepto', component: TiposConceptoComponent, canActivate: [AuthGuard]},
  {path: 'tipoEvento', component: TiposEventoComponent, canActivate: [AuthGuard]},
  {path: 'tipoProveedor', component: TiposProveedorComponent, canActivate: [AuthGuard]},
  {path: 'tipoTarifa', component: TiposTarifaComponent, canActivate: [AuthGuard]},
  {path: 'tipoContacto', component: TiposContactoComponent, canActivate: [AuthGuard]},
  {path: 'tipoDocumento', component: TiposDocumentoComponent, canActivate: [AuthGuard]},
  {path: 'entidades', component: BancosComponent, canActivate: [AuthGuard]},
  {path: 'rangoNumeracion', component: RangoNumeracionComponent, canActivate: [AuthGuard]},
  {path: 'tipoDocContable', component: TipoDocumentoContableComponent, canActivate: [AuthGuard]},
  {path: 'indicadorRetencion', component: IndicadorRetencionComponent, canActivate: [AuthGuard]},
  {path: 'mediosPago', component: MediosPagoComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
