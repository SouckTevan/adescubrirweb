import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ProveedoresComponent } from '../proveedores/proveedores.component';
import { SharedModule } from '../shared/shared.module';
import { AdminRolComponent } from '../admin-rol/admin-rol.component';
import { UsuariosComponent } from '../usuarios/usuarios.component';
import { EstadosComponent } from '../estados/estados.component';
import { ConceptosComponent } from '../conceptos/conceptos.component';
import { ActivosFijosComponent } from '../activos-fijos/activos-fijos.component';
import { ContratoComponent } from '../contrato/contrato.component';
import { AdminKumpaniaComponent } from '../admin-kumpania/admin-kumpania.component';
import { AdminResguardoComponent } from '../admin-resguardo/admin-resguardo.component';
import { AdminConsejoComunitarioComponent } from '../admin-consejo-comunitario/admin-consejo-comunitario.component';
import { TiposConceptoComponent } from '../tipos-concepto/tipos-concepto.component';
import { TiposEventoComponent } from '../tipos-evento/tipos-evento.component';
import { TiposProveedorComponent } from '../tipos-proveedor/tipos-proveedor.component';
import { TiposTarifaComponent } from '../tipos-tarifa/tipos-tarifa.component';
import { TiposContactoComponent } from '../tipos-contacto/tipos-contacto.component';
import { TiposDocumentoComponent } from '../tipos-documento/tipos-documento.component';
import { BancosComponent } from '../bancos/bancos.component';
import { RangoNumeracionComponent } from '../rango-numeracion/rango-numeracion.component';
import { TipoDocumentoContableComponent } from '../tipo-documento-contable/tipo-documento-contable.component';
import { IndicadorRetencionComponent } from '../indicador-retencion/indicador-retencion.component';
import { MediosPagoComponent } from '../medios-pago/medios-pago.component';

@NgModule({
  declarations: [
    ProveedoresComponent,
    ContratoComponent,
    ActivosFijosComponent,
    ConceptosComponent,
    EstadosComponent,
    UsuariosComponent,
    AdminRolComponent,
    AdminKumpaniaComponent,
    TiposConceptoComponent,
    TiposProveedorComponent,
    TiposEventoComponent,
    TiposTarifaComponent,
    TiposContactoComponent,
    TiposDocumentoComponent,
    BancosComponent,
    RangoNumeracionComponent,
    AdminResguardoComponent,
    AdminConsejoComunitarioComponent,
    TipoDocumentoContableComponent,
    IndicadorRetencionComponent,
    MediosPagoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
