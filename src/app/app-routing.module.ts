import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
// import { ProveedoresComponent } from './proveedores/proveedores.component';
// import { UsuariosComponent } from './usuarios/usuarios.component';
// import { ContratoComponent } from './contrato/contrato.component';
// import { ActivosFijosComponent } from './activos-fijos/activos-fijos.component';
// import { RadicadoDocumentosComponent } from './radicado-documentos/radicado-documentos.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './auth.guard';
// import { ConceptosComponent } from './conceptos/conceptos.component';
// import { EstadosComponent } from './estados/estados.component';
// import { TiposConceptoComponent } from './tipos-concepto/tipos-concepto.component';
// import { TiposEventoComponent } from './tipos-evento/tipos-evento.component';
// import { TiposProveedorComponent } from './tipos-proveedor/tipos-proveedor.component';
// import { TiposTarifaComponent } from './tipos-tarifa/tipos-tarifa.component';
// import { TiposContactoComponent } from './tipos-contacto/tipos-contacto.component';
// import { TiposDocumentoComponent } from './tipos-documento/tipos-documento.component';
// import { BancosComponent } from './bancos/bancos.component';
// import { RangoNumeracionComponent } from './rango-numeracion/rango-numeracion.component';
// import { EventosComponent } from './eventos/eventos.component';
// import { AdminKumpaniaComponent } from './admin-kumpania/admin-kumpania.component';
// import { AdminResguardoComponent } from './admin-resguardo/admin-resguardo.component';
// import { AdminConsejoComunitarioComponent } from './admin-consejo-comunitario/admin-consejo-comunitario.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], runGuardsAndResolvers: 'always', children: [ //
    { path: '', component: WelcomeComponent}, //canActivate: [AuthGuard],
    { path: 'admin', loadChildren: () => import(`./admin/admin.module`).then(m => m.AdminModule), data: { preload: true } },
    { path: 'evento', loadChildren: () => import('./evento/evento.module').then(m => m.EventoModule) },
    { path: 'tesoreria', loadChildren: () => import('./tesoreria/tesoreria.module').then(m => m.TesoreriaModule) }
  //   {path: 'tipoConcepto', component: TiposConceptoComponent, outlet: 'dash'},
  //   {path: 'tipoEvento', component: TiposEventoComponent, outlet: 'dash'},
  //   {path: 'tipoProveedor', component: TiposProveedorComponent, outlet: 'dash'},
  //   {path: 'tipoTarifa', component: TiposTarifaComponent, outlet: 'dash'},
  //   {path: 'tipoContacto', component: TiposContactoComponent, outlet: 'dash'},
  //   {path: 'tipoDocumento', component: TiposDocumentoComponent, outlet: 'dash'},
  //   {path: 'entidadFinanciera', component: BancosComponent, outlet: 'dash'},
  //   {path: 'rangoNumeracion', component: RangoNumeracionComponent, outlet: 'dash'},
  //   {path: 'eventos', component: EventosComponent, outlet: 'dash'},
  //   {path: '404', component: LoginComponent}
  ]},
  { path: '404', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false, preloadingStrategy: PreloadAllModules})], // useHash: true, onSameUrlNavigation: 'reload', 
  exports: [RouterModule]
})
export class AppRoutingModule { }
