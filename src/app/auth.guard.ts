import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AdescubrirService } from './adescubrir.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private apiService: AdescubrirService, private _router: Router) {
  }

  canActivate() {
    if (!this.apiService.isLoggedIn()) {
      // navigate to login page
      this._router.navigate(['/']);
      return false;
    }

    return true;
  }
  
}
