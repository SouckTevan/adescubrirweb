import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { Banco } from '../models/admin/banco/banco';
declare var $:any;

@Component({
  selector: 'app-bancos',
  templateUrl: './bancos.component.html',
  styleUrls: ['./bancos.component.scss']
})
export class BancosComponent implements OnInit {

  banco: Banco;

  errCod: boolean = false;

  // -------------- Array para llenar la tabla principal
  arrayBanco = new MatTableDataSource<Banco>();

  // -------------- Columnas
  showColsBanco: string[] = ['id', 'nombre', 'codigoBancario', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.banco = new Banco();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayBanco.paginator = this.paginator;
    // ---------- Sort
    this.arrayBanco.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getBancos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayBanco.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.banco = new Banco();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getBanco/'+id)
    .subscribe((resp: Banco) => {
      this.banco = resp;
      $('.loading').addClass('hide-loading');
      $('#modalBanco').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.banco, 'administracion/saveBanco')
      .subscribe((resp: Banco) => {
        this.banco = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalBanco').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validateLength() {
    if (this.banco.codigoBancario.length >= 5) this.errCod = true;
    else this.errCod = false;
  }

  validate(): boolean {
    const nm = this.banco.nombre != '';
    const cb = this.banco.codigoBancario != '';
    const es = this.banco.estado != '';
    const lg = this.banco.codigoBancario.length <= 5;

    return nm && cb && es && lg;
  }

  exportExcel() {
    if (this.arrayBanco.data != undefined && this.arrayBanco.data.length > 0)
      this.util.exportAsExcelFile(this.arrayBanco.data, 'Entidades Financieras');
  }

  applyFilter(filterValue: string) {
    this.arrayBanco.filter = filterValue.trim().toLowerCase();
  }

}
