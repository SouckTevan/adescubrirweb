import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { Concepto } from '../models/admin/concepto/concepto';
declare var $: any;

@Component({
  selector: 'app-conceptos',
  templateUrl: './conceptos.component.html',
  styleUrls: ['./conceptos.component.scss']
})
export class ConceptosComponent implements OnInit {

  // -------------- Array para llenar la tabla principal
  arrayConceptos = new MatTableDataSource<Concepto>();
  concepto: Concepto;

  // -------------- Columnas
  showCols: string[] = ['id', 'nombre', 'nombreConcepto', 'estado', 'accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit() {
    this.get();
    this.concepto = new Concepto();

    // ---------- Paginador
    this.arrayConceptos.paginator = this.paginator;
    // ---------- Sort
    this.arrayConceptos.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getConceptos/*')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayConceptos.data = res;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.concepto = new Concepto();
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    this.apiServices.get('administracion/getConceptoById/'+id)
    .subscribe((resp: Concepto) => {
      this.concepto = resp;
      $('#modalConcepto').modal('show');
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.concepto, 'administracion/saveConcepto')
      .subscribe((resp: Concepto) => {
        this.concepto = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalConcepto').modal('hide');
        this.get();
        $('.loading').addClass('hide-loading');
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos que tienen "*" son obligatorios!');
    }
  }

  validate(): boolean {
    const nm = this.concepto.nombre != '';
    const tc = this.concepto.tipoConcepto_id != 0;
    const ii = this.concepto.indicador_id != 0;

    return nm && tc && ii;
  }

  exportExcel() {
    if (this.arrayConceptos.data != undefined && this.arrayConceptos.data.length > 0)
      this.util.exportAsExcelFile(this.arrayConceptos.data, 'Conceptos');
  }

  applyFilter(filterValue: string) {
    this.arrayConceptos.filter = filterValue.trim().toLowerCase();
  }

}
