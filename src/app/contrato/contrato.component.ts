import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UtilityService } from '../utility.service';
import { Municipio } from '../models/admin/municipio/municipio';
import { Concepto } from '../models/admin/concepto/concepto';
import { TarifaContrato } from '../models/eventos/tarifa-contrato/tarifa-contrato';
import { Contrato } from '../models/eventos/contrato/contrato';
import { RegistroObservacion } from '../models/admin/registro-observacion/registro-observacion';
import { Parametro } from '../models/admin/parametro/parametro';
import { DatePipe } from '@angular/common';
import {  read, utils } from 'xlsx';
declare var $: any;

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.scss']
})
export class ContratoComponent implements OnInit {

  state: {};
  currentDirection = 'right';
  DIR_RIGHT = 'right';
  DIR_LEFT = 'left';

  // ----- Municipio
  arrayMunicipios: Municipio[] = [];
  arrayMunAgregados: Municipio[] = [];
  middleMunicipios: Municipio[] = [];

  // ----- Concepto
  arrayConceptos: Concepto[] = [];
  arrayConAgregados: Concepto[] = [];
  middleConceptos: Concepto[] = [];

  arrayTarifas: TarifaContrato[] = [];
  arrayAnios: number[] = [];

  // ---------- Contadores ---
  contadorObservacion: number = 1;

  // ------------- Objetos de tipo
  contrato: Contrato;
  tarifa: TarifaContrato;
  contratoObs: RegistroObservacion;

  // -------------- Array para llenar tablas
  arrayContrato = new MatTableDataSource<Contrato>();
  arrayObservaciones = new MatTableDataSource<RegistroObservacion>();

  // -------------- Columnas
  showColsContrato: string[] = ['id', 'nombre', 'estado','accion'];
  showColsContratoObs: string[] = ['id', 'fecha', 'hora', 'usuario','observacion','accion'];

  //---- Cargue Excel
  file: File;
  // base64Data = '';
  nameFile = '';

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public apiService: AdescubrirService, public util: UtilityService, private date : DatePipe) {
    this.state = [{opt: 'Selecciona una opción', val: 'S'}, {opt: 'Activo', val: Parametro.ESTADO_ACTIVO}, {opt: 'Inactivo', val: Parametro.ESTADO_INACTIVO}];
    this.tarifa = new TarifaContrato();
    this.contrato = new Contrato();
    this.contratoObs = new RegistroObservacion();
  }

  ngOnInit() {
    this.getContratos();

    // ---------- Paginador
    this.arrayContrato.paginator = this.paginator;
    // ---------- Sort
    this.arrayContrato.sort = this.sort;

    this.jqueryFunctions();
  }

  addContract() {
    $('#nav-tab a:first-child').tab('show');
    this.contrato = new Contrato();
    this.tarifa = new TarifaContrato();
    this.arrayObservaciones.data = [];
    this.arrayMunAgregados = [];
    this.arrayConAgregados = [];
    this.arrayTarifas = [];
    this.arrayMunicipios = [];
    this.arrayConceptos = []
    this.contratoObs = new RegistroObservacion();
  }

  jqueryFunctions() {
    let dt = new Date().getFullYear();
    let base = 2010;

    while(base <= dt) {
      this.arrayAnios.push(base);
      base++;
    }

    $(() => {
      $('[data-toggle="tooltip"]').tooltip();

      $('.container-btn-mp').on('click', 'button', function() {
        $(this).tooltip('hide');
      })


      $('#modalContrato').on('shown.bs.modal', function() {
        $('#btn-excel').click(function() {
          $('#fileExcel').click();
        });
      });
    })
  }

  changeSelect(table: string, id: number) {
    if (table === 'municipio' && id > 0) {
      this.arrayMunicipios = [];
      this.apiService.get('administracion/getMunicipioByDep/'+id).subscribe((res) => {
        for (const currMun of this.contrato.municipios) {
          for (const allMun of res) {
            if (currMun.id === allMun.id) {
              res.splice(res.indexOf(allMun), 1);
            }
          }
        }
        res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
        this.arrayMunicipios = res;
      });
    }else if (table === 'municipio') {
      this.arrayMunicipios = [];
    }

    if (table === 'concepto' && id > 0) {
      this.arrayConceptos = [];
      this.apiService.get('administracion/getConceptoByTpConcepto/'+id).subscribe((res) => {
        for (const currCon of this.contrato.conceptos) {
          for (const allCon of res) {
            if (currCon.id === allCon.id) {
              res.splice(res.indexOf(allCon), 1);
            }
          }
        }
        res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
        this.arrayConceptos = res;
      });
    }else if (table === 'concepto') {
      this.arrayConceptos = [];
    }
  }

  getContratos() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.arrayContrato.data = [];
    this.apiService.get('administracion/getContratos')
      .subscribe((resp: Contrato[]) => {
        resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        for (const key in resp) {
          const element = resp[key];
          this.arrayContrato.data = [...this.arrayContrato.data, element];
        }
        $('.loading').addClass('hide-loading');
      },
      (err:HttpErrorResponse)=>{
        this.util.messageError(err);
      })
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.contrato = new Contrato();
    $('#nav-tab a:first-child').tab('show');
    this.apiService.get('administracion/getContrato/'+id)
      .subscribe((resp: Contrato) => {
        this.contrato = resp;
        this.contrato.valorTotal = this.util.miles(this.contrato.valorTotal);
        this.contrato.fechaFirma = this.date.transform(this.contrato.fechaFirma, 'yyyy-MM-dd');
        this.contrato.fechaInicial = this.date.transform(this.contrato.fechaInicial, 'yyyy-MM-dd');
        this.contrato.fechaFin = this.date.transform(this.contrato.fechaFin, 'yyyy-MM-dd');
        this.arrayObservaciones.data = this.contrato.observaciones;
        this.arrayMunAgregados = this.contrato.municipios;
        this.arrayConAgregados = this.contrato.conceptos;
        this.arrayTarifas = this.contrato.tarifas;
        for (const tar of this.arrayTarifas) {
          tar.valor = this.util.miles(tar.valor);
        }
        $('.loading').addClass('hide-loading');
        $('#modalContrato').modal('show');
      },err => {
        this.util.messageError(err);
      });
  }

  // ------- Agregar una tarifa
  addTarifa() {
    if (this.validar('tarifa')) {
      this.tarifa.concepto_id = this.tarifa.concepto.id;
      this.tarifa.municipio_id = this.tarifa.municipio.id;
      //this.arrayTarifas.push(this.tarifa)
      this.arrayTarifas = [...this.arrayTarifas, this.tarifa];
      this.tarifa = new TarifaContrato();
    } else {
      this.util.notification('info','Validación','Los campos marcados con (*) son obligatorios');
    }
  }

  // ------- Agregar una observacion
  addObservation() {
    if (this.contratoObs.observacion != '') {
      this.contratoObs.contador = this.contadorObservacion;
      this.contratoObs.fechaHora = new Date();
      this.contratoObs.usuario = this.apiService._usuarioSesion;
      this.contratoObs.usuario_id = this.apiService._usuarioSesion.id;
      this.arrayObservaciones.data = [...this.arrayObservaciones.data, this.contratoObs];
      this.contratoObs = new RegistroObservacion();
      this.contadorObservacion++;
    }else {
      this.util.notification('warning', '', 'Debe de ingresar una observacion!');
    }
  }

  save() {
    if (this.validar('save')) {
      $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando Contrato...');
      this.contrato.valorTotal = this.util.removeMiles(this.contrato.valorTotal);
      this.contrato.observaciones = this.arrayObservaciones.data;
      this.contrato.municipios = this.arrayMunAgregados;
      this.contrato.conceptos = this.arrayConAgregados;
      this.contrato.fechaFirma = new Date(this.contrato.fechaFirma+'T00:00:00');
      this.contrato.fechaInicial = new Date(this.contrato.fechaInicial+'T00:00:00');
      this.contrato.fechaFin = new Date(this.contrato.fechaFin+'T00:00:00');

      for (const tar of this.arrayTarifas) {
        tar.valor = this.util.removeMiles(tar.valor);
      }
      this.contrato.tarifas = this.arrayTarifas;
      this.apiService.post(this.contrato, 'administracion/saveContrato')
      .subscribe((res: any) => {
        this.contrato.fechaFirma = this.date.transform(res.fechaFirma, 'yyyy-MM-dd');
        this.contrato.fechaInicial = this.date.transform(res.fechaInicial, 'yyyy-MM-dd');
        this.contrato.fechaFin = this.date.transform(res.fechaFin, 'yyyy-MM-dd');
        $('.loading').addClass('hide-loading');
        this.util.notification('success', '', 'Contrato guardado!');
        $('#modalContrato').modal('hide');
        this.getContratos();
      }, (err) => {
        this.util.messageError(err);
      });
    }else {
      this.util.notification('info','Validación','Los campos marcados con (*) son obligatorios');
    }
  }

  validar(type: string): boolean {
    if (type == 'save') {
      let nm = this.contrato.nombre != '', co = this.contrato.contratante_id != 0,
      vl = this.contrato.valorTotal != 0, ff = this.contrato.fechaFirma != null,
      fi = this.contrato.fechaInicial != null, fn = this.contrato.fechaFin != null;

      return nm && co && vl && ff && fi && fn;
    }
    if (type == 'tarifa') {
      let m = this.tarifa.municipio != null,
      c = this.tarifa.concepto != null,
      t = this.tarifa.tipotarifa_id != 0,
      v = this.tarifa.valor > 0;

      return m && c && t && v;
    }
  }

  applyFilter(filterValue: string) {
    this.arrayContrato.filter = filterValue.trim().toLowerCase();
  }

  edit(table: string, data: any) {
    if (table === 'observacion') {
      this.contratoObs = data;
      this.arrayObservaciones.data = this.arrayObservaciones.data.filter(dt => dt !== data);
    }
  }

  delete(table: string, data: any) {
    if (table === 'observacion') {
      this.arrayObservaciones.data = this.arrayObservaciones.data.filter(dt => dt !== data);
      this.contadorObservacion--;
    }
  }

  addAll(table: string, direction: string) {
    if (table == 'municipio' && this.arrayMunicipios.length > 0) {
      if (direction === this.DIR_RIGHT) {
        for (const mun of this.arrayMunicipios) {
          this.arrayMunAgregados.push(mun);
        }

        this.arrayMunicipios = [];
        this.middleMunicipios = [];
      }
    }else if (table === 'municipio') {
      this.util.notification('info','','Debe de seleccionar un Departamento');
    }

    if (table === 'concepto' && this.arrayConceptos.length > 0) {
      if (direction === this.DIR_RIGHT) {
        for (const con of this.arrayConceptos) {
          this.arrayConAgregados.push(con);
        }

        this.arrayConceptos = [];
        this.middleConceptos = [];
      }
    } else if (table === 'concepto') {
      this.util.notification('info','','Debe de seleccionar un Concepto');
    }
  }

  add(table: string, data: any, cl: string, direction: string) {
    $('.'+cl).toggleClass('active-mp');
    this.currentDirection = direction;
    if (table === 'municipio' && this.arrayMunicipios.length > 0) {
      let index = this.middleMunicipios.indexOf(data);

      if (index === -1)
        this.middleMunicipios.push(data);
      else
        this.middleMunicipios = this.middleMunicipios.filter(dt => dt !== data);

    } else if (table === 'municipio') {
      this.util.notification('info','','Debe de seleccionar un Departamento');
    }

    if (table === 'concepto' && this.arrayConceptos.length > 0) {

      let index = this.middleConceptos.indexOf(data);

      if (index === -1)
        this.middleConceptos.push(data);
      else
        this.middleConceptos = this.middleConceptos.filter(dt => dt !== data);
    } else if (table === 'concepto') {
      this.util.notification('info','','Debe de seleccionar un Concepto');
    }
  }

  passSelected(table: string, direction: string) {
    if (table === 'municipio' && this.arrayMunicipios.length > 0) {
      if (this.currentDirection === direction) {
        if (direction === this.DIR_RIGHT) { // Agregar
          for (const mun of this.middleMunicipios) {
            this.arrayMunAgregados.push(mun);
            this.arrayMunicipios = this.arrayMunicipios.filter(dt => dt !== mun);
          }
          this.middleMunicipios = [];
        }else if (direction === this.DIR_LEFT) {  // Remover
          for (const mun of this.middleMunicipios) {
            this.arrayMunicipios.push(mun);
            this.arrayMunAgregados = this.arrayMunAgregados.filter(dt => dt !== mun);
          }

          this.middleMunicipios = [];
        }
      }
    }else if (table === 'municipio') {
      this.util.notification('info','','Debe de seleccionar un Departamento');
    }

    if (table === 'concepto' && this.arrayConceptos.length > 0) {
      if (this.currentDirection === direction) {
        if (direction === this.DIR_RIGHT) {
          for (const cp of this.middleConceptos) {
            this.arrayConAgregados.push(cp);
            this.arrayConceptos = this.arrayConceptos.filter(dt => dt !== cp);
          }
          this.middleConceptos = [];
        }else if (direction === this.DIR_LEFT) {  // Remover
          for (const cp of this.middleConceptos) {
            this.arrayConceptos.push(cp);
            this.arrayConAgregados = this.arrayConAgregados.filter(dt => dt !== cp);
          }
          this.middleConceptos = [];
        }
      }
    } else if (table === 'concepto') {
      this.util.notification('info','','Debe de seleccionar un Concepto');
    }
  }

  removeAll(table: string, direction: string) {
    if (table === 'municipio' && this.arrayMunicipios.length > 0) {
      if (direction === this.DIR_LEFT) {
        for (const mun of this.arrayMunAgregados) this.arrayMunicipios.push(mun);

        this.arrayMunAgregados = [];
      }
    }else if (table === 'municipio') {
      this.util.notification('info','','Debe de seleccionar un Departamento');
    }

    if (table === 'concepto' && this.arrayConceptos.length > 0) {
      if (direction === this.DIR_LEFT) {
        for (const con of this.arrayConAgregados) this.arrayConceptos.push(con);

        this.arrayConAgregados = [];
      }
    } else if (table === 'concepto') {
      this.util.notification('info','','Debe de seleccionar un Concepto');
    }
  }

  // ---- Subir / Bajar al inicio
  topDownAll(table: string, isTop: boolean) {
    if (table === 'municipio') {
      for (const mm of this.middleMunicipios) {
        this.arrayMunAgregados = this.arrayMunAgregados.filter(dt => dt !== mm);
        if (isTop) // Arriba
          this.arrayMunAgregados.unshift(mm);
        else // Abajo
          this.arrayMunAgregados.push(mm);
      }
    }else if (table === 'concepto') {
      for (const mc of this.middleConceptos) {
        this.arrayConAgregados = this.arrayConAgregados.filter(dt => dt !== mc);
        if (isTop) // Arriba
          this.arrayConAgregados.unshift(mc);
        else // Abajo
          this.arrayConAgregados.push(mc);
      }
    }
  }

  // ---- Subir / Bajar
  topDown(table: string, isTop: boolean) {
    if (table === 'municipio') {
      for (const mm of this.middleMunicipios) {
        const ind = this.arrayMunAgregados.indexOf(mm);
        this.arrayMunAgregados = this.arrayMunAgregados.filter(dt => dt !== mm);

        if (isTop) // Arriba
          this.arrayMunAgregados.splice((ind-1),0,mm);
        else // Abajo
          this.arrayMunAgregados.splice((ind+1),0,mm);
      }
    }else if (table === 'concepto') {
      for (const mc of this.middleConceptos) {
        const ind = this.arrayConAgregados.indexOf(mc);
        this.arrayConAgregados = this.arrayConAgregados.filter(dt => dt !== mc);

        if (isTop) // Arriba
          this.arrayConAgregados.splice((ind-1),0,mc);
        else // Abajo
          this.arrayConAgregados.splice((ind+1),0,mc);
      }
    }
  }

  // --------- Cargue Excel
  loadExcel(file: any) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.file = file[0];
    this.nameFile = file[0].name;
    this.convertExcelToJson(file[0]);
  }

  async convertExcelToJson(file: any) {
    let reader = new FileReader();
    let workbookkk;
    let XL_row_object;
    let json_object;
    reader.readAsBinaryString(file);

    await new Promise((resolve, reject) => {
      reader.onload = function() {
        let data = reader.result;
        workbookkk=read(data,{type: 'binary'});

        workbookkk.SheetNames.forEach(function(sheetName) {
          if (workbookkk.Sheets[sheetName]['!ref'] !== undefined) {
            // Here is your object
            XL_row_object = utils.sheet_to_json(workbookkk.Sheets[sheetName]);
            // console.log(XL_row_object);
            // json_object = JSON.stringify(XL_row_object);
            resolve(XL_row_object);
          }
        });
      };
    });
    this.sendBurden(XL_row_object);
  }

  async sendBurden(jsonExcel: any) {
    let objectKeysToLowerCase = function (origObj: any) {
      return Object.keys(origObj).reduce(function (newObj, key) {
        let val = origObj[key];
        let newVal = (typeof val === 'object') ? objectKeysToLowerCase(val) : val;
        newObj[key.toLowerCase()] = newVal;
        return newObj;
      }, {});
    }
    let arrJson = [];
    await jsonExcel.filter((rw: any) => { arrJson.push(objectKeysToLowerCase(rw)); });
    $('#fileExcel').val('');
    let b = {tarifas: arrJson, nombreArchivo: this.nameFile, idContrato: this.contrato.id};
    console.log(b);

    this.apiService.post(b, 'administracion/dinamic/CargaMasivaTarifas/')
    .subscribe((res: any) => {
      console.log(res);
      $('.loading').addClass('hide-loading');
      if (res.success) {
        this.util.notification('success','','La carga se ha realizado con exito!');
        this.getContratoTarifas();
      }else {
        this.util.notification('info','Cargue excel',res.error);
      }
    },err => {
      this.util.messageError(err);
    });
  }

  getContratoTarifas() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando Tarifas...');
    this.apiService.get('administracion/getContratoTarifa/'+this.contrato.id)
      .subscribe(async(res: TarifaContrato[]) => {
        console.log(res);
        this.arrayTarifas = res;
        for await (const tar of this.arrayTarifas) {
          tar.valor = this.util.miles(tar.valor);
        }
        $('.loading').addClass('hide-loading');
    },err => {
      this.util.messageError(err);
    });
  }
}
