import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.scss']
})
export class CustomSelectComponent implements OnInit {
  
  @Output() res = new EventEmitter<any>();
  @Output() obj = new EventEmitter<any>();

  showContent: boolean = false;
  
  // ----- Array para cargar datos
  loadFields: string[];
  data: any[];
  allData: any[];
  datos: any[];

  letter: string = '';
  disabled: boolean;
  multiple: boolean;

  // If multiple is true
  addComma = false;
  returnValueList = [];

  @Input()
  set _multiple(vl: boolean) {
    this.multiple = vl;
  }

  @Input()
  returnValue: string;

  @Input()
  separator: string = ' ';

  @Input()
  set _disabled(value: boolean) {
    this.disabled = value;
  }

  @Input()
  set _data(_data: any[]) {
    this.allData = _data;
    this.data = _data;
    this.orderData(_data, this.loadFields);
  }

  @Input()
  set _setVal(val: any) {
    // if (val != undefined && val != '') {
      this.letter = '';
      if (val !== 0 || val !== '0') {
        this.allData.filter((dt: any) => {
          if (dt[this.returnValue] === val) {
            for (const f of this.loadFields) {
              this.letter += dt[f]+this.separator
            }
          }
        });
      }
    // }
  }

  @Input()
  set _showFileds(_fields: string[]) {
    this.loadFields = _fields;
    this.orderData(this.data, _fields);
  }

  constructor() { }

  ngOnInit() { }

  orderData(_data: any[], _fields: string[]) {
    if (_data != undefined && _fields != undefined) {
      this.datos = [];
      this.data = [];
      for (const dt in _data) {
        let register = '';
        let saerchField = '';
        for (const ite of _fields) {
          register += _data[dt][ite]+this.separator;
          if (saerchField == '') {
            saerchField = ite+'_'+_data[dt][ite];
          }
        }

        this.datos.push({name: register.trim(), val: saerchField});
        saerchField = '';
      }
      this.data = this.datos;
    }
  }

  onKeyUp(wd: any) {
    let datosTemp = [];
    this.datos = this.data;
    
    if (wd != '' && this.datos.length > 0) {
      for (const word of this.datos) {
        if (word.name.toLowerCase().indexOf(wd.toLowerCase()) != -1) {
          datosTemp.push(word);
        }
      }
      this.datos = datosTemp;
      this.showContent = true;
    }else {
      this.showContent = false;
    }
  }

  itemSelected(valSelected: any) {
    let values: string[] = valSelected.val.split('_');
    let valToReturn = this.allData.filter((dt: any) => {
      if (dt[values[0]] === values[1]) {
        return dt;
      }
    })[0];
    this.showContent = !this.showContent;
    this.obj.emit(valToReturn);
    this.res.emit(valToReturn[this.returnValue.trim()]);
    // this.letter = valSelected.name;
  }
  
  addToList(valSelected: any) {
    let values: string[] = valSelected.val.split('_');
    let valToReturn = this.allData.filter((dt: any) => {
      if (dt[values[0]] === values[1]) {
        return dt;
      }
    })[0];
    
    let index = this.returnValueList.indexOf(valToReturn[this.returnValue]);
    
    if (index === -1) {
      this.returnValueList.push(valToReturn[this.returnValue]);

      if (this.addComma) {
        this.letter += ', ';
      }
  
      this.letter += valSelected.name;
    }else {
      this.addComma = false;
      this.returnValueList.splice(index, 1);
      let arrLetter = this.letter.split(',');
      arrLetter.splice(index, 1);
      this.letter = '';
      for (const itm of arrLetter) {
        
        if (this.addComma) {
          this.letter += ', ';
        }else {
          this.addComma = true;
        }

        this.letter += itm;
      }
    }
    
    if (this.returnValueList.length > 0) {
      this.addComma = true;
    }else {
      this.addComma = false;
    }
  }

}
