import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  itemToShow: any;
  isSubMenuOpen = false;

  constructor() { }

  ngOnInit() {
    $('.loading').addClass('hide-loading');
    $(document).ready(function() {
      $('body').on('click', 'table tbody tr', function() {
        if($(this).hasClass('bg-tab')) {
          $(this).toggleClass('bg-tab');
        }else {
          $('body table tbody tr').removeClass('bg-tab');
          $(this).addClass('bg-tab');
        }
      });
    })
  }

}
