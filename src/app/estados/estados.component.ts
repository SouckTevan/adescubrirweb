import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { WriteSearchComponent } from '../write-search/write-search.component';
import { UtilityService } from '../utility.service';
import { Estado } from '../models/admin/estado/estado';
declare var $: any;

@Component({
  selector: 'app-estados',
  templateUrl: './estados.component.html',
  styleUrls: ['./estados.component.scss']
})
export class EstadosComponent implements OnInit {

  // -------------- Array para llenar la tabla principal
  arrayEstados = new MatTableDataSource<Estado>();
  estado: Estado;

  // -------------- Columnas
  showCols: string[] = ['id', 'modulo', 'nombre', 'estado', 'accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('nvResp', {static: true}) nvResp: WriteSearchComponent;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit() {
    this.get();

    this.estado = new Estado();

    // ---------- Paginador
    this.arrayEstados.paginator = this.paginator;

    // ---------- Sort
    this.arrayEstados.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getEstados')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayEstados.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.estado = new Estado();
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    this.apiServices.get('administracion/getEstado/'+id)
    .subscribe((resp: Estado) => {
      this.estado = resp;
      $('.loading').addClass('hide-loading');
      $('#modalEstado').modal('show');
    });
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.estado, 'administracion/saveEstado')
      .subscribe((resp: Estado) => {
        this.estado = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('.loading').addClass('hide-loading');
        $('#modalEstado').modal('hide');
        this.get();
      }, (err) => this.util.messageError(err));
    }else {
      this.util.notification('warning', '', 'Los campos que tienen "*" son obligatorios!');
    }
  }

  validate(): boolean {
    const nm = this.estado.nombre != '';
    const es = this.estado.estado != '';
    const md = this.estado.modulo != '';

    return nm && es && md;
  }

  exportExcel() {
    if (this.arrayEstados.data != undefined && this.arrayEstados.data.length > 0)
      this.util.exportAsExcelFile(this.arrayEstados.data, 'Estados');
  }

  applyFilter(filterValue: string) {
    this.arrayEstados.filter = filterValue.trim().toLowerCase();
  }

}
