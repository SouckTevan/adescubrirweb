import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoTemplateComponent } from './evento-template.component';

describe('EventoTemplateComponent', () => {
  let component: EventoTemplateComponent;
  let fixture: ComponentFixture<EventoTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
