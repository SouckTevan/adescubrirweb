import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { AdescubrirService } from '../adescubrir.service';
import { Persona } from '../models/admin/persona/persona';
import { Anticipo } from '../models/contable/anticipo/anticipo';
import { DocumentoContable } from '../models/contable/documento-contable/documento-contable';
import { Bitacora } from '../models/eventos/bitacora/bitacora';
import { CotizacionEvento } from '../models/eventos/cotizacion-evento/cotizacion-evento';
import { EventoDocumentos } from '../models/eventos/evento-documento/evento-documentos';
import { EventoObservaciones } from '../models/eventos/evento-observaciones/evento-observaciones';
import { Evento } from '../models/eventos/evento/evento';
import { LegalizacionGasto } from '../models/eventos/legalizacion-gasto/legalizacion-gasto';
import { ObservacionFacturacion } from '../models/eventos/observacion-facturacion/observacion-facturacion';
import { TarifaCotizacion } from '../models/eventos/tarifa-cotizacion/tarifa-cotizacion';
import { TarifaEvento } from '../models/eventos/tarifa-evento/tarifa-evento';
import { TiqueteEvento } from '../models/eventos/tiquete/tiquete-evento';
import { Viatico } from '../models/eventos/viaticos/viatico';
import { ProvTemplateComponent } from '../prov-template/prov-template.component';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { UtilityService } from '../utility.service';
declare var $: any;

@Component({
  selector: 'app-evento-template',
  templateUrl: './evento-template.component.html',
  styleUrls: ['./evento-template.component.scss']
})
export class EventoTemplateComponent implements OnInit {

  evento: Evento = new Evento();
  cotizacion: CotizacionEvento = new CotizacionEvento();
  tiquete: TiqueteEvento = new TiqueteEvento();
  anticipo: Anticipo = new Anticipo();

  arrayDocumentos = new MatTableDataSource<EventoDocumentos>();
  arrayConceptoTarifa = new MatTableDataSource<TarifaEvento>();
  eventoDocumento: EventoDocumentos = new EventoDocumentos();

  userSession = null;

  tipoComunidad = {
    indigena: false,
    afrocolombianas: false,
    rrom: false
  }

  marcoPolitico = {
    asistencia: false,
    atencion: false,
    prevencion: false,
    proteccion: false,
    reparacion: false,
    verdad: false,
    justicia: false
  }

  _disabled = false;
  openReporteBuscador = false;

  dataContratos = [];

  // ---- Anticipos
  dataBancos = [];

  totalViaticos = 0;
  showBtnSave = true;
  hiddeModalEvento = false;

  // ---- Facturación
  noCalcularInter = false;
  noCalcularDesc = false;

  dataConsumos = [];
  dataEjecucion = [];

  arrayConceptos = [];

  showInfoFactura = false;
  nombreSede = '';

  departamento = '0';
  currentTab = 1;

  cotizaciones = [];
  dataGastos: LegalizacionGasto[] = [];
  dataViaticos: Viatico[] = [];
  dataTiquete = [];

  horaI = {
    hora: '01',
    minuto: '00'
  }

  horaF = {
    hora: '01',
    minuto: '00'
  }

  tiqueteHoraS = {
    hora: '01',
    minuto: '00'
  }

  tiqueteHoraR = {
    hora: '01',
    minuto: '00'
  }

  anticipoPagos = [];
  fieldAnticipos = 0;

  nombreContrato: string = '';

  // ------ File of Documents
  files: File[] = [];
  filesTemp: File[] = [];

  arrHours = [];
  arrMinutes = [];

  totalValTiquetes = 0;

  showFacCoti = false;

  tarifaEvento: TarifaEvento = new TarifaEvento();
  bitacora: Bitacora = new Bitacora();
  observacion: EventoObservaciones = new EventoObservaciones();
  observacionFac: ObservacionFacturacion = new ObservacionFacturacion();
  gasto: LegalizacionGasto = new LegalizacionGasto();
  viatico: Viatico = new Viatico();

  assetUrl: SafeResourceUrl;

  onlyCloseCurrentModal: boolean = false;

  // ----- Cotizacion
  dataProvMun = [];
  dataSucProv = [];
  dataCalTarProve = [];
  cotizacionesSeleccionadas = [];

  showColsDocumento: string[] = ['fecha','usuario', 'tipoDocumento', 'archivo', 'accion'];
  showColsConcepTarifa: string[] = ['concepto','descuento','valor','accion'];

  // ----- Loading
  loading = {anticipo:true,factura:true,cotizaciones:true,bitacora:true,tiquetes:true,documentos:true,observacion:true,observacionFac:true,legalizacion:true,viaticos:true};

  @ViewChild('municipioDatos', {static: true}) municipioDatos: SelectGeneralComponent;
  @ViewChild('veredasDep', {static: true}) veredasDep: SelectGeneralComponent;
  @ViewChild('resguardoDep', {static: true}) resguardoDep: SelectGeneralComponent;
  @ViewChild('consejoComDep', {static: true}) consejoComDep: SelectGeneralComponent;
  @ViewChild('kumpaniaDep', {static: true}) kumpaniaDep: SelectGeneralComponent;
  @ViewChild(ProvTemplateComponent, {static: true}) provModal: ProvTemplateComponent;

  @Output() refresh = new EventEmitter<any>();

  constructor(private apiService: AdescubrirService, public util: UtilityService, private datePipe: DatePipe, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.userSession = this.apiService._usuarioSesion;
    this.loadTime();

    this.get('administracion/getNombresContratosPorEstado/A', (dt: any) => {
      dt.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      for (const userRol of this.userSession.roles) {
        for (const it of dt) {
          if ((userRol.contratoId === -1 || userRol.contratoId === Number(it.contratoId)) && this.dataContratos.indexOf(it) === -1) {
            this.dataContratos.push(it);
          }
        }
      }
    });

    this.jqueryFunction();
  }

  addEvento() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.reset();
    this.showBtnSave = true;
    this.apiService.get('administracion/getConceptos/A')
    .subscribe((res) => {
      this.arrayConceptos = res;
      // this.evento.idContrato = this.searchI.idContrato;
      $('.loading').addClass('hide-loading');
      $('#modalEvento').modal('show');
    });
  }

  jqueryFunction() {
    $('[data-tooltip="true"]').tooltip({
      container: 'body',
      placement:'bottom'
    });

    $('.modal-body').on('click', '.btn-tab', function(e: any) {
      e.preventDefault();
      var dir = $(this).attr('dir'), $elem = $('.nav-pills'), newScrollLeft = $elem.scrollLeft();

      if (dir === 'left') {
        if (newScrollLeft != 0) {
          $elem.animate({
            scrollLeft: '-=150px'
          }, 'slow');
        }
      }
      if (dir == 'right') {
        var width = $elem.outerWidth(), scrollWidth=$elem.get(0).scrollWidth;
        if (scrollWidth-newScrollLeft != width) {
          $elem.animate({
            scrollLeft: '+=150px'
          }, 'slow');
        }
      }
    });

    $('#modalCotizacion, #modalCalTarEvento, #modalBuyDocuContable, #modalPDF').on('shown.bs.modal', function (e) {
      $('#modalEvento').addClass('z-index-1');
    });
    $('#modalCotizacion, #modalCalTarEvento, #modalAddLegalizacion, #modalTiquete, #modalBuyDocuContable, #modalViewPayments').on('hidden.bs.modal', () => {
        this.hiddeModalEvento = false;
      $('#modalEvento').removeClass('z-index-1');
    });
  }

  closeModal() {
    if (this.onlyCloseCurrentModal) {
      $('#modalEvento').removeClass('z-index-1');
      this.onlyCloseCurrentModal = false;
    }
  }

  loadTime() {
    for (let i = 1; i <= 24; i++) {
      let h = ''+i;
      if (i < 10) {
        h = '0'+i;
      }
      this.arrHours.push(h);
    }

    for (let i = 0; i <= 59; i++) {
      let m = ''+i;
      if (i < 10) {
        m = '0'+i;
      }
      this.arrMinutes.push(m);
    }
  }

  setContrato() {
    const contratoName = this.dataContratos.find((contrato: any) => contrato.contratoId == this.evento.idContrato);
    this.nombreContrato = (contratoName != undefined)?contratoName.nombre:'';
  }

  open(id: number) {
    this.reset();
    $('.loading').removeClass('hide-loading').find('.text-load').text('Buscando...');
    this.apiService.get('eventos/getEvento/'+id)
    .subscribe(async(res) => {
      if (res != null) {
        this.evento = res;
        this.arrayDocumentos.data = [];
        this.arrayConceptoTarifa.data = [];
        this.totalViaticos = 0;
        // ------ load the rest of data
        this.get('eventos/getAnticipos/'+this.evento.id, (dt: any) => {this.loading.anticipo = false; this.evento.anticipos = dt;});
        this.get('eventos/getFactura/'+this.evento.id+'/0', (dt: any) => {this.loading.factura = false; this.evento.factura = dt
          if (this.evento.factura != null && this.evento.factura.id != 0) {
            this.showInfoFactura = true;
            this.evento.factura.fecha = this.datePipe.transform(this.evento.factura.fecha, 'yyyy-MM-dd');
            this.evento.factura.fechaVencimiento = this.datePipe.transform(this.evento.factura.fechaVencimiento, 'yyyy-MM-dd');
          }
        });
        this.post(this.evento.rangoFechas, 'eventos/getEventoCotizaciones/'+this.evento.id, (dt: any) => {this.loading.cotizaciones = false; this.cotizaciones = dt});
        this.get('eventos/getBitacora/'+this.evento.id, (dt: any) => {this.loading.bitacora = false; this.evento.bitacora = dt});
        this.get('eventos/getEventoDocumentos/'+this.evento.id, async(dt: any) => {this.loading.documentos = false;
          if (dt != null && dt != undefined && dt.length > 0) {
            this.evento.documentos = dt;
            for await (const doc of dt) { this.arrayDocumentos.data = [...this.arrayDocumentos.data, doc]; };
          }
        });
        this.get('eventos/getObservacion/'+this.evento.id, (dt: any) => {this.loading.observacion = false; this.evento.eventoObservacion = dt});
        this.get('eventos/getObsFacturacion/'+this.evento.id, (dt: any) => {this.loading.observacionFac = false; this.evento.facturacion = dt});
        this.get('eventos/getLegalizaciones/'+this.evento.id, (dt: any) => {this.loading.legalizacion = false; this.dataGastos = dt});
        this.get('eventos/getViaticos/'+this.evento.id, (dt: any) => {this.loading.viaticos = false; this.dataViaticos = dt; (dt != null)?dt.filter((r: any)=> this.totalViaticos += r.valor):this.dataViaticos = [];});
        this.get('eventos/getTiquetes/'+this.evento.id, (dt: any) => {this.loading.tiquetes = false; this.dataTiquete = dt; if (dt != null) this.dataTiquete.filter((rs) => this.totalValTiquetes += rs.total)});
        // ------ Proveedor Cotizaciones
        this.getProveedorCotizacion();
        this.setEvento();
        this.setContrato();

        let hI = new Date(this.evento.horaInicioEvento), hF = new Date(this.evento.horaFinEvento);
        this.horaI = {hora: (hI.getHours() < 10 ? '0'+hI.getHours(): ''+hI.getHours()), minuto: (hI.getMinutes() == 0 ? '0'+hI.getMinutes(): ''+hI.getMinutes())}
        this.horaF = {hora: (hF.getHours() < 10 ? '0'+hF.getHours(): ''+hF.getHours()), minuto: (hF.getMinutes() == 0 ? '0'+hF.getMinutes(): ''+hF.getMinutes())}
        this.changeSelectGeneral(this.evento.sede.departamento_id, 'datos');
        this.changeSelectGeneral(this.evento.sede.id, 'municipio');
        this.apiService.get('administracion/getConceptos/A').subscribe((res) => this.arrayConceptos = res);
        $('#modalEvento').modal('show');
      }
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  showModalViewAnt() {
    this.apiService.get('administracion/getBancos')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      this.dataBancos = res;
      $('#modalBuyDocuContable').modal('show');
    }, err => this.util.messageError(err));
  }

  changeSelectGeneral(id: any, table: string) {
    switch(table) {
      case 'datos':
        this.departamento = id;
        this.municipioDatos.onChangePadre(id);
        this.municipioDatos._val = this.evento.idSede;
        break;

      case 'municipio':
        this.veredasDep.onChangePadre(id);
        this.resguardoDep.onChangePadre(id);
        this.consejoComDep.onChangePadre(id);
        this.kumpaniaDep.onChangePadre(id);
        break;
    }
  }

  get(ruta: string, callback: any) {
    this.apiService.get(ruta)
    .subscribe((res: any) => {
      callback(res);
    })
  }

  post(obj: any, api: string, callback: any) {
    this.apiService.post(obj, api)
      .subscribe((res: Anticipo) => {
        callback(res);
      }, err => this.util.messageError(err));
  }

  getProveedorCotizacion() {
    this.dataProvMun = [];
    this.get('administracion/getProveedoresByMunicipio/'+this.evento.idSede, (res: any) => {
      if (res != null) {
        res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
        this.dataProvMun = res;
      }
    });
  }

  setEvento() {
    this.nombreSede = this.evento.sede.nombre;
    this.evento.fechaInicioEvento = this.datePipe.transform(this.evento.fechaInicioEvento, 'yyyy-MM-dd');
    this.evento.fechaFinEvento = this.datePipe.transform(this.evento.fechaFinEvento, 'yyyy-MM-dd');
    this.evento.fechaSolicitud = this.datePipe.transform(this.evento.fechaSolicitud, 'yyyy-MM-dd');
    if (this.evento.tarifasDelEvento != null) {
      this.evento.tarifasDelEvento.filter((dt) => dt.valor = this.util.miles(dt.valor));
      this.arrayConceptoTarifa.data = this.evento.tarifasDelEvento;
    }
    this.evento.numeroAsistentes = this.util.miles(this.evento.numeroAsistentes);
    this.evento.valorPresupuesto = this.util.miles(this.evento.valorPresupuesto);
    this.evento.texto3 = (this.evento.texto3 == 'true');

    if (this.evento.tipoComunidad !== null && this.evento.tipoComunidad !== '')
      this.tipoComunidad = JSON.parse(this.evento.tipoComunidad);

    if (this.evento.marcoPolitico !== null && this.evento.marcoPolitico !== '')
      this.marcoPolitico = JSON.parse(this.evento.marcoPolitico);

    this.orderConsumos(true, this.evento.eventoConsumos);
    this.orderConsumos(false, this.evento.eventoEjecucion);
  }

  add(tabla: string) {
    let isPost = false, obj = null, ruta = '', tipo = '';
    switch (tabla) {
      case 'documento':
        if (this.eventoDocumento.idDocumento != 0 && this.filesTemp.length > 0) {
          $('.loading').removeClass('hide-loading').find('p').text('Guardando Documento...');
          this.eventoDocumento.idUsuario = this.apiService._usuarioSesion.id;
          this.eventoDocumento.idEvento = this.evento.id;
          this.eventoDocumento.usuario.nombres = this.apiService._usuarioSesion.nombre;
          this.eventoDocumento.fechaHora = new Date();
          this.eventoDocumento.fileType = this.filesTemp[0].type;
          this.eventoDocumento.nombreArchivo = this.filesTemp[0].name;
          this.files.push(this.filesTemp[0]);

          delete this.eventoDocumento.usuario.socialUser;
          delete this.eventoDocumento.usuario.municipio['codigo'];
          let prop = 'webkitRelativePath';
          delete this.files[0][prop];

          const formData = new FormData();

          formData.append('file', this.files[0], this.files[0].name);

          formData.append('documento', JSON.stringify(this.eventoDocumento));

          this.apiService.postWithFile('eventos/saveEventoDocumento', formData)
          .subscribe((res: any) => {
            $('.loading').addClass('hide-loading');
            if (res != null) {
              this.arrayDocumentos.data = [...this.arrayDocumentos.data, res];
              this.eventoDocumento = new EventoDocumentos();
              this.filesTemp = [];
              this.files = [];
              this.util.notification('success','','Documento guardado');
            }
          }, err => this.util.messageError(err));
        }else {
          this.util.notification('warning','','Debe agregar un docmento y el tipo documento es obligatorio');
        }
        break;

      case 'conceptoTarifa':
        isPost = true;
        tipo = 'Concepto';
        ruta = 'eventos/saveTarifa';
        this.tarifaEvento.valor = Number(this.util.removeMiles(this.tarifaEvento.valor));
        this.tarifaEvento.evento_id = this.evento.id;
        this.arrayConceptoTarifa.data = [...this.arrayConceptoTarifa.data, this.tarifaEvento];
        obj = this.tarifaEvento;
      break;

      case 'updateTarifa':
        isPost = true;
        tipo = 'Concepto';
        ruta = 'eventos/saveTarifa';
        $('.custom-control input').attr('disabled','true');
        this.tarifaEvento.conDescuento = !this.tarifaEvento.conDescuento;
        this.tarifaEvento.valor = Number(this.util.removeMiles(this.tarifaEvento.valor));
        obj = this.tarifaEvento;
        break;

      case 'bitacora':
        isPost = true;
        tipo = 'Bitacora';
        ruta = 'eventos/saveBitacora';
        this.bitacora.evento_id = this.evento.id;
        this.bitacora.usuario_id = this.apiService._usuarioSesion.id;
        obj = this.bitacora;
        break;

      case 'observacion':
        isPost = true;
        tipo = 'Observación';
        ruta = 'eventos/saveObservacion';
        this.observacion.evento_id = this.evento.id;
        this.observacion.usuario_id = this.apiService._usuarioSesion.id;
        obj = this.observacion;
        break;

      case 'observacionFac':
        isPost = true;
        tipo = 'Obs. Facturación';
        ruta = 'eventos/saveObsFacturacion';
        this.observacionFac.evento_id = this.evento.id;
        this.observacionFac.usuario_id = this.apiService._usuarioSesion.id;
        obj = this.observacionFac;
        break;

      case 'consumos':
        tipo = 'Consumo';
        ruta = 'eventos/consultarConsumos';
        break;

      case 'ejecucion':
        tipo = 'Ejecucion';
        ruta = 'eventos/consultarEjecucion';
        break;
    }

    if (tabla == 'consumos' || tabla == 'ejecucion') {
      isPost = true;
      let evTemp = new Evento();
      evTemp.id = this.evento.id;
      evTemp.fechaInicioEvento = this.evento.fechaInicioEvento;
      evTemp.idContrato = this.evento.idContrato;
      evTemp.idSede = this.evento.idSede;
      evTemp.idTipoTarifa = this.evento.idTipoTarifa;
      evTemp.rangoFechas = this.evento.rangoFechas;
      obj = evTemp;
    }

    if (isPost) {
      this.post(obj, ruta, (rs: any) => {
        if (rs != null) {
          let showN = true;
          if (tabla == 'bitacora') {
            this.bitacora = new Bitacora();
            this.evento.bitacora.push(rs);
          }else if (tabla == 'observacion') {
            this.observacion = new EventoObservaciones();
            this.evento.eventoObservacion.push(rs);
          }else if (tabla == 'observacionFac') {
            this.observacionFac = new ObservacionFacturacion();
            this.evento.facturacion.push(rs);
          }else if (tabla == 'conceptoTarifa' || tabla == 'updateTarifa') {
            this.tarifaEvento = new TarifaEvento();
            this.arrayConceptoTarifa.data.filter((res) => {res.valor = this.util.miles(res.valor)});
            $('.custom-control input').removeAttr('disabled');
            if (tabla == 'conceptoTarifa') {
              this.add('consumos');
            }
          }else if(tabla == 'consumos') {
            showN = false;
            this.evento.eventoConsumos = rs;
            this.orderConsumos(true, this.evento.eventoConsumos);
            this.add('ejecucion');
          }else if (tabla == 'ejecucion') {
            showN = false;
            this.evento.eventoEjecucion = rs;
            this.orderConsumos(false, this.evento.eventoEjecucion);
          }
          if (showN) {
            this.util.notification('success',tipo, tipo+' agregada');
          }
        }
      });
    }
  }

  orderConsumos(isConsumos: boolean, arrayData: any) {
    isConsumos ? this.dataConsumos = [] : this.dataEjecucion = [];
    // tab == 1 ? this.dataConsumos = [] : (tab == 2) ? this.dataEjecucion = [] : null;
    for (const key in arrayData) {
      const element = arrayData[key].cantidades;
      let bdy = {conceptoId: arrayData[key].conceptoId,
                nombreConcepto: arrayData[key].nombreConcepto,
                tarifa: arrayData[key].tarifa,
                totalTarifa: arrayData[key].totalTarifa};
      let arrCantidadesAc = [];
      let arr = [];
      let total = 0;
      for (const fC in element) {
        arrCantidadesAc.push(fC);
      }
      for (const rFecha of this.evento.rangoFechas) {
        let bdy = {fecha: rFecha};

        let is = arrCantidadesAc.includes(rFecha);

        if (is) {
          bdy['cantidad'] = element[rFecha];
          total = total + Number(element[rFecha]);
        } else {
          bdy['cantidad'] = 0;
        }
        arr.push(bdy);
      }
      bdy['total'] = total;
      bdy['cantidades'] = arr;
      // tab == 1 ? this.dataConsumos.push(bdy) : (tab == 2) ? this.dataEjecucion.push(bdy) : (tab == 3) ? this.cotizacion['dataConsumos'].push(bdy) : null;
      isConsumos ? this.dataConsumos.push(bdy) : this.dataEjecucion.push(bdy);
    }
  }

  sumarConsumos(consumo: any) {
    let total = 0;
    for (const vl of consumo.cantidades) {
      total = total + Number(vl.cantidad);
    }
    consumo.totalTarifa = (total * consumo.tarifa);
    consumo.total = total;
  }

  loadPayments(idAnticipo: number, isNew: boolean) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    let api = (!isNew)?'getAnticipoPagos/'+idAnticipo:'getDocumentoPagos/'+idAnticipo;

    this.apiService.get('documentoContable/'+api)
    .subscribe((res: any) => {
      console.log(res);
      if (res != null && res != undefined) {
        // this.openPyment(res.soporte);
        this.anticipoPagos = res;
        $('#modalViewPayments').modal('show');
        this.hiddeModalEvento = true;
      }
      $('.loading').addClass('hide-loading');
    }, (e:any) => this.util.messageError(e));
  }

  openPyment(path: string) {
    this.apiService.postByData({path}, 'pdfServices/openFile')
      .subscribe((res: any) => {
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, '', 'application/pdf', '', false));
        $('#modalPDF').modal('show');
      }, err => this.util.messageError(err));
  }

  onSelect(event: any) {
    if (this.filesTemp.length < 1) {
      let fileName = event.addedFiles[0].name.replace(/[^a-zA-Z 0-9.]+/g,'');
      fileName = fileName.replace(/\s/g, '');
      Object.defineProperty(event.addedFiles[0], 'name', {
        writable: true,
        value: fileName
      });
      this.filesTemp.push(...event.addedFiles);
      setTimeout(() => {
        var chi = $('.document-dropzone ngx-dropzone-preview:last-child');
        var tp = chi.find('ngx-dropzone-label').attr('tp');
        var imgLoad = 'icon_image-min.png';
        if (tp === 'application/pdf') {
          imgLoad = 'icono_pdf-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
          imgLoad = 'icono_excel.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
          imgLoad = 'icon_word-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
          imgLoad = 'icono_powerpoint.png';
        } else if (tp === 'text/plain') {
          imgLoad = 'icono_txt-min.png';
        }
        chi.css({'background':'url(./assets/img/'+imgLoad+') no-repeat','background-size':'contain'});
      },1000);
    }
  }

  onRemove(event: any) {
    this.filesTemp.splice(this.filesTemp.indexOf(event), 1);
  }

  openFile(elem: EventoDocumentos, isDownload: boolean) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiService.downloadFile('administracion/getFileSystemRuta/'+elem.idEvento+'/'+elem.idDocumento)
    .subscribe((res: any) => {
      if (res != null) {
        if (isDownload) {
          $('.loading').addClass('hide-loading');
          this.util.saveAsFile(res,elem.idEvento+"_"+elem.idDocumento+"_"+elem.nombreArchivo, elem.fileType,'', true);
        } else {
          this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, elem.idEvento+"_"+elem.idDocumento+"_"+elem.nombreArchivo, 'application/pdf', '', false));
          $('#modalPDF').modal('show');
          this.onlyCloseCurrentModal = true;
        }
        $('.loading').addClass('hide-loading');
      }
    }, err => this.util.messageError(err));
  }

  generarPresupuestos(isResumido: boolean, idEvento: number) {
    this.apiService.postByData({idEvento: idEvento}, (isResumido)?'pdfServices/generarPDFPresupuesto/':'pdfServices/generarPDFPresupuestoDetallado/')
    .subscribe((res): any => {
      this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, '', 'application/pdf', '', false));
      $('#modalPDF').modal('show');
      this.onlyCloseCurrentModal = true;
    });
  }

  generarAnticipo(anticipo: Anticipo, txt: string, isGenerar: boolean): void {
    $('.loading').removeClass('hide-loading').find('.text-load').text(txt+' Anticipo...');
    if (anticipo != null || this.validarAnticipo()) {
      let save = true;
      console.log(this.evento);

      if (anticipo != null)
        this.anticipo = anticipo;

      this.anticipo.generar = isGenerar;
      this.anticipo.idEvento = this.evento.id;
      this.anticipo.idCreador = this.apiService._usuarioSesion.id;
      this.anticipo.fechaVencimiento = new Date(this.anticipo.fechaVencimiento);
      this.anticipo.valor = Number(this.util.removeMiles(this.anticipo.valor));

      let cot = undefined;
      if (this.cotizaciones != null) {
        cot = this.cotizaciones.filter((data: CotizacionEvento) => data.idProveedor === this.anticipo.idBeneficiario)[0]
      }

      if (cot != undefined) {
        if (this.anticipo.valor > cot.totalCotizacion) {
          save = false;
          this.util.notification('info','Ups!','El valor del anticipo no puede superar el total de la cotización ($'+this.util.miles(cot.totalCotizacion)+')');
        }
      }

      if (save) {
        console.log(this.anticipo);

        this.apiService.post(this.anticipo, 'eventos/generarAnticipo')
        .subscribe((res: Anticipo) => {
          $('.loading').addClass('hide-loading');
          if (res != null) {
            // this.util.notification('success','', (anticipo != null)?'Se ha reenviado el anticipo':this.anticipo.generar ? 'Se ha generado el anticipo':'Se ha solicitado el anticipo');
            this.util.notification('success','', (this.anticipo.generar)?'Se ha generado el anticipo':'Se ha reenviado el anticipo');
            if (this.evento.anticipos.length > 0)
              this.evento.anticipos = this.evento.anticipos.filter((rs: any) => rs.id != res.id);
            // if (anticipo == null || anticipo.id == 0)
            this.evento.anticipos.push(res);
            this.anticipo = new Anticipo();
          }else {
            this.util.notification('warning','', 'Validar si el usuario tiene email o cuenta bancaria');
            this.util.notification('info','Editar los datos','Del botón al lado del beneficiario del pago');
          }
        }, err => this.util.messageError(err));
      }
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning','Validar','Los campos marcados con (*) son obligatorios!');
    }
  }

  validarAnticipo(): boolean {
    let idPersona = this.anticipo.idBeneficiario != 0, conceptoPago = this.anticipo.concepto != '',
    total = this.anticipo.valor != null, fechaVencimiento = this.anticipo.fechaVencimiento != null,
    observaciones = this.anticipo.observacion != '', prioridad = this.anticipo.prioridad != '';

    return idPersona && conceptoPago && total && fechaVencimiento && observaciones && prioridad;
  }

  initProcessAnticipo(anticipo: Anticipo) {
    anticipo.estado = 'AUTORIZACION';
    this.anticipo = anticipo;
    this.anticipo.generar = true;
    this.generarAnticipo(this.anticipo, 'Generando', true);
  }

  enviarAnticipoAGastos(anticipo: Anticipo) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Enviando...');
    this.apiService.post(anticipo, 'eventos/enviarAnticipoAGastos')
    .subscribe((res) => {
      $('.loading').addClass('hide-loading');
      if (res == null) {
        this.util.notification('info','','El Anticipo ya esta cargado en los gastos');
      }else {
        this.util.notification('success','','Se ha enviado el Anticipo a gastos');
      }
    }, (e: any) => this.util.messageError(e));
  }

  calcularAgrupado(isAgrupado: any) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Calculando...');
    for (const ite of this.evento.tarifasDelEvento) {
      ite.valor = Number(this.util.removeMiles(ite.valor))
    }
    this.evento.factura.fecha = this.datePipe.transform(this.evento.factura.fecha, 'yyyy-MM-dd');
    this.evento.factura.fechaVencimiento = this.datePipe.transform(this.evento.factura.fechaVencimiento, 'yyyy-MM-dd');
    let b = {
      noCalcularInter: this.noCalcularInter,
      noCalcularDesc: this.noCalcularDesc,
      isAgrupar: isAgrupado,
      evento: this.evento
    }
    if (b.evento.valorPresupuesto != null && b.evento.valorPresupuesto.toString().includes('.')) {
      b.evento.valorPresupuesto = Number(this.util.removeMiles(b.evento.valorPresupuesto));
    }
    this.apiService.post(b, 'eventos/calcularFacturaUnConcepto')
    .subscribe((res: any) => {
      console.log(res);

      res.fecha = this.datePipe.transform(res.fecha, 'yyyy-MM-dd');
      res.fechaVencimiento = this.datePipe.transform(res.fechaVencimiento, 'yyyy-MM-dd');
      this.evento.factura = res;
      this.showInfoFactura = true;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  generateInvoice() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando...');
    if (this.evento.factura.numero != null && this.evento.factura.numero != '') {
      let b = {
        idEvento: this.evento.id,
        documentoContable: this.evento.factura
      }
      this.apiService.post(b, 'eventos/generarFactura')
      .subscribe((res: any) => {
        res.fecha = this.datePipe.transform(res.fecha, 'yyyy-MM-dd');
        this.evento.factura = res;
        $('.loading').addClass('hide-loading');
        this.util.notification('success','Genial!', 'Factura Generada...');
      }, err => this.util.messageError(err));
    }else {
      this.util.notification('info','Obligatorio', 'El numero es obligatorio');
    }
  }

  generatePDFInvoice() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando...');
    if (this.evento.factura.numero != null && this.evento.factura.numero != '') {
      this.apiService.postByData(this.evento.factura, 'pdfServices/generarPDFDocumentoContable')
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, '', 'application/pdf', '', false));
        $('#modalPDF').modal('show');
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('info','Obligatorio', 'El numero es obligatorio');
    }
  }

  saveFacturation() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.evento.factura.numero != null && this.evento.factura.numero != '') {
      this.evento.factura.fecha = this.datePipe.transform(this.evento.factura.fecha, 'yyyy-MM-dd');
      this.evento.factura.fechaVencimiento = this.datePipe.transform(this.evento.factura.fechaVencimiento, 'yyyy-MM-dd');

      let b = {
        noCalcularInter: this.noCalcularInter,
        evento: this.evento
      }

      this.apiService.post(b, 'eventos/saveFactura')
      .subscribe((res: any) => {
        if (res != null) {
          res.fecha = this.datePipe.transform(res.fecha, 'yyyy-MM-dd');
          this.evento.factura = res;
          $('.loading').addClass('hide-loading');
          this.util.notification('success', 'Genial!','Factura guardada!');
        }
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('info','Obligatorio!','El campo número es obligatorio');
    }
  }

  reset() {
    $('#pills-tab li:first-child a').tab('show');
    this.showInfoFactura = false;
    this.loading = {anticipo:true,factura:true,cotizaciones:true,bitacora:true,
                    tiquetes:true,documentos:true,observacion:true,
                    observacionFac:true,legalizacion:true,viaticos:true};
    this.dataConsumos = [];
    this.dataEjecucion = [];
    this.departamento = '0';
    this.arrayDocumentos.data = [];
    this.evento = new Evento();

    this.tipoComunidad = {
      indigena: false,
      afrocolombianas: false,
      rrom: false
    }

    this.marcoPolitico = {
      asistencia: false,
      atencion: false,
      prevencion: false,
      proteccion: false,
      reparacion: false,
      verdad: false,
      justicia: false
    }

    this.horaI = {
      hora: '01',
      minuto: '00'
    }

    this.horaF = {
      hora: '01',
      minuto: '00'
    }

    this.totalValTiquetes = 0;
  }

  save() {
    let ruta = 'eventos/saveEvento', obj = null, dataCon = [], arrCon = [];
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    this.evento.valorPresupuesto = this.util.removeMiles(this.evento.valorPresupuesto);
    if (this.evento.tarifasDelEvento != null) {
      for (const ite of this.evento.tarifasDelEvento) {
        ite.valor = Number(this.util.removeMiles(ite.valor))
      }
    }
    if (this.currentTab == 1 || this.currentTab == 7) {
      if (this.validar('datos')) {
        this.evento.numeroAsistentes = this.util.removeMiles(this.evento.numeroAsistentes);
        this.evento.fechaInicioEvento = this.datePipe.transform(this.evento.fechaInicioEvento, 'yyyy-MM-dd');
        this.evento.fechaFinEvento = this.datePipe.transform(this.evento.fechaFinEvento, 'yyyy-MM-dd');
        this.evento.fechaSolicitud = this.datePipe.transform(this.evento.fechaSolicitud, 'yyyy-MM-dd');

        const hI = new Date(),hF = new Date();
        hI.setHours(Number(this.horaI.hora));
        hI.setMinutes(Number(this.horaI.minuto));
        hF.setHours(Number(this.horaF.hora));
        hF.setMinutes(Number(this.horaF.minuto));
        this.evento.horaInicioEvento = hI;
        this.evento.horaFinEvento = hF;
        this.evento.tipoComunidad = JSON.stringify(this.tipoComunidad);
        this.evento.marcoPolitico = JSON.stringify(this.marcoPolitico);

        for (const dataC of this.dataEjecucion) {
          let cantidades = this.evento.eventoEjecucion[dataC.conceptoId].cantidades;
          this.evento.eventoEjecucion[dataC.conceptoId].total = dataC.total;
          for (const c of dataC.cantidades) {
            cantidades[c.fecha] = Number(c.cantidad);
            if (cantidades[c.fecha] <= 0) {
              delete cantidades[c.fecha];
            }
          }
        }
      }else {
        $('.loading').addClass('hide-loading');
        this.util.notification('warning','Validar','Los campos marcados con (*) son obligatorios');
      }
    }else if (this.currentTab == 2) {
      ruta = 'eventos/saveConsumos';
      this.evento.valorPresupuesto = Number(this.util.removeMiles(this.evento.valorPresupuesto));
      if (this.evento.tarifasDelEvento != null) {
        for (const ite of this.evento.tarifasDelEvento) {
          ite.valor = Number(this.util.removeMiles(ite.valor))
        }
      }
      dataCon = this.dataConsumos;
      arrCon = this.evento.eventoConsumos;
    }else if (this.currentTab == 4) {
      ruta = 'eventos/saveEjecuciones';
      dataCon = this.dataEjecucion;
      arrCon = this.evento.eventoEjecucion;
    }

    for (const dataC of dataCon) {
      let cantidades = arrCon[dataC.conceptoId].cantidades;

      arrCon[dataC.conceptoId].total = dataC.total;
      for (const c of dataC.cantidades) {
        cantidades[c.fecha] = Number(c.cantidad);
        if (cantidades[c.fecha] <= 0) {
          delete cantidades[c.fecha];
        }
      }
    }
    obj = this.evento;
    console.log(obj);

    this.apiService.post(obj, ruta)
      .subscribe((res: any) => {
        let message = 'Evento guardado';
        if (this.currentTab == 1 || this.currentTab == 7) {
          this.evento = res;
          this.setEvento();
          // this.search();
          message = 'Evento guardado';
        }else if (this.currentTab == 2) {
          message = 'Consumos guardados';
          this.evento.eventoConsumos = res;
          this.orderConsumos(true, this.evento.eventoConsumos);
        }else if (this.currentTab == 4) {
          message = 'Ejecuciones guardadas';
          this.evento.eventoEjecucion = res;
        }
        $('.loading').addClass('hide-loading');
        if (res != null) {
          this.util.notification('success','',message);
        }
      }, err => this.util.messageError(err));
  }

  delete(tabla: string, obj: any) {
    let ruta = '', ob = null;
    switch (tabla) {
      case 'documento':
        this.arrayDocumentos.data = this.arrayDocumentos.data.filter(ob => ob !== obj);
        break;

      case 'concetoTarifa':
        ob = obj;
        ruta = 'eventos/deleteTarifa';
        this.arrayConceptoTarifa.data = this.arrayConceptoTarifa.data.filter(ob => ob !== obj);
        break;

      case 'cotizacion':
        ob = obj;
        ruta = 'eventos/deleteCotizacion';
        this.cotizaciones = this.cotizaciones.filter(ob => ob.id !== obj);
        break;

      case 'anticipo':
        ob = obj;
        ruta = 'eventos/deleteAnticipo';
        this.evento.anticipos = this.evento.anticipos.filter(_ob => _ob.id !== obj);
        break;
    }

    if (ob != null && ob != 0) {
      this.apiService.post(ob, ruta)
      .subscribe(() => {
        this.util.notification('success','Genial','Se ha eliminado con exito');
        this.add('consumos');
      }, err => this.util.messageError(err));
    }
  }

  validar(tab: string): boolean {
    if (tab == 'datos') {
      let contrato = this.evento.idContrato != 0,
      nombre = this.evento.nombre != '',
      tpEvento = this.evento.idTipoEvento != 0,
      sede = this.evento.idSede != 0,
      fInicio = this.evento.fechaInicioEvento != undefined,
      fFin = this.evento.fechaFinEvento != undefined,
      tpTarifa = this.evento.idTipoTarifa != 0,
      fSolicitud = this.evento.fechaSolicitud != undefined

      return contrato && nombre && tpEvento && sede && fInicio && fFin && tpTarifa && fSolicitud;
    }
    if (tab == 'cotizacion') {
      let t = this.cotizacion.tipo != '0', p = this.cotizacion.idProveedor != '0',
      s = this.cotizacion.idSucursal != '0', i = this.cotizacion.idTipoTarifa != '0';

      return t && p && s && i;
    }

    if (tab == 'fac_cotizacion') {
      let n = this.cotizacion.factura.numero != '';
      return n;
    }

    if (tab == 'tiquete') {
      let ps = this.tiquete.nombrePasajero != '', rt = this.tiquete.ruta != '', fs = this.tiquete.fechaSalida != null, hs = this.tiquete.horaSalida != null,
      fr = this.tiquete.fechaRegreso != null, hr = this.tiquete.horaRegreso != null, ae = this.tiquete.idAerolinea != 0, lz = this.tiquete.localizador != '',
      tq = this.tiquete.numero != '', vl = this.tiquete.valor != null && this.tiquete.valor != 0, im = this.tiquete.impuestos != null && this.tiquete.impuestos != 0,
      ts = this.tiquete.tasas != null && this.tiquete.tasas != 0, ad = this.tiquete.administrativo != null && this.tiquete.administrativo != 0,
      fe = this.tiquete.fee != null && this.tiquete.fee != 0, fc = this.tiquete.factura != '';

      return ps && rt && fs && hs && fr && hr && ae && lz && tq && vl && im && ts && ad && fe && fc;
    }
  }

  // ------- Cotización ------- //
  newCotizacion() {
    this.cotizacion = new CotizacionEvento();
  }

  openCotizacion(idCotizacion: any, isCopy: boolean) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.cotizacion.factura.detalle = [];
    this.get('eventos/getEventoCotizacion/'+idCotizacion+'/'+this.evento.rangoFechas, (cotizacion: any) => {
      $('.nav-pills-pr li:first-child a').tab('show');
      this.cotizacion = cotizacion;
      this.showFacCoti = false;
      for (const itm of this.cotizacion.tarifas) {
        for (const key in itm.consumos) {
            const elem = itm.consumos[key];
            itm.totalCantidad = elem.total;
        }
      }

      this.llenarSucursal(cotizacion.idProveedor);
      if (isCopy) {
        this.cotizacion.id = 0;
        for (const itm of this.cotizacion.tarifas) {
          itm.idCotizacion = 0;
        }
      }

      this.get('eventos/getFactura/0/'+this.cotizacion.idFactura, (res: any) => {
        if (res != null) {
          this.showFacCoti = true;
        }
        this.cotizacion.factura = res;
        $('.loading').addClass('hide-loading');
        $('#modalCotizacion').modal('show');
      });
    });
  }

  llenarSucursal(id: any) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.get('administracion/getSucursales/'+id, (res: any) => {
      this.dataSucProv = res;
      $('.loading').addClass('hide-loading');
    });
  }

  calcularCotizacion() {
    this.cotizacion.tarifas = [];
    if (this.cotizacion.tipo != '0' && this.cotizacion.idProveedor != '0' && this.cotizacion.idSucursal != '0' && this.cotizacion.idTipoTarifa != '0') {
      this.cotizacion.consumos = this.evento.eventoConsumos;

      for (const it of this.evento.tarifasDelEvento) {
        let tarifaCot = new TarifaCotizacion();
        tarifaCot.valor = it.valor;
        tarifaCot.concepto = it.concepto;
        tarifaCot.idConcepto = it.concepto_id;
        tarifaCot.valor = Number(this.util.removeMiles(tarifaCot.valor));
        this.cotizacion.tarifas.push(tarifaCot);
      }
      if (this.cotizacion.id == 0){
        this.cotizacion.idCreador = this.userSession.id;
      }

      this.cotizacion.idProveedor = Number(this.cotizacion.idProveedor);
      this.cotizacion.idSucursal = Number(this.cotizacion.idSucursal);
      this.cotizacion.idTipoTarifa = Number(this.cotizacion.idTipoTarifa);
      this.cotizacion.factura.persona = new Persona();
      let b = {
        evento: this.evento,
        cotizacion: this.cotizacion
      };

      this.apiService.post(b, 'eventos/calcularCotizacion')
      .subscribe((res: any) => {
        this.cotizacion = res;
      });
    }else {
      this.util.notification('warning','Validación', 'Los campos con (*) son obligatorios');
    }
  }

  calcCotizacion(val: any, item: any, isDate: boolean) {
    if (isDate) {
      let con = item.consumos;
      item.totalCantidad = 0;
      for (const key in con) {
        let elem = con[key].cantidades;
        elem[val.key] = Number(val.value);
        for (const k in elem) {
          item.totalCantidad += elem[k];
        }
      }
    } else
      item.valor = Number(val);

    item.totalTarifa = (item.totalCantidad*item.valor);
  }

  saveCotizacion(isAprobado: boolean) {
    if (this.validar('cotizacion') || isAprobado) {
      if (this.cotizacion.tarifas.length > 0 || isAprobado) {
        if (!isAprobado)
          this.cotizacion.nombreSucursal = (this.dataSucProv.filter((res: any) => res.municipio_id == this.cotizacion.idSucursal)[0]).nombre;
        this.apiService.post(this.cotizacion, 'eventos/saveCotizacion/'+this.evento.id)
        .subscribe((res: any) => {
          this.cotizacion = res;

          if (this.cotizaciones == null)
            this.cotizaciones = [];

          let d = {
            id: this.cotizacion.id,
            estado: this.cotizacion.estado,
            aprobado: this.cotizacion.aprobado,
            idFactura: this.cotizacion.idFactura,
            nombreProveedor: this.cotizacion.proveedor.nombre,
            nombreSucursal: this.cotizacion.sucursal.nombre,
            nombreTarifa: this.cotizacion.tipoTarifa.nombre,
            idProveedor: this.cotizacion.idProveedor
          };

          this.cotizaciones.push(d);
          this.util.notification('success','Genial!','Cotización guardada!');
        }, err => this.util.messageError(err));
      }else {
        this.util.notification('info','Ups!','No hay consumos calculados');
      }
    }else {
      this.util.notification('warning','Validación', 'Los campos con (*) son obligatorios');
    }
  }

  generatePdfCotizacion() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando...');

    this.evento.tarifasDelEvento.filter((dt) => dt.valor = Number(this.util.removeMiles(dt.valor)));
    let b = {
      evento: this.evento,
      idCotSelected: this.cotizacion.id
    };

    this.apiService.postByData(b, 'pdfServices/generarPDFCotizacion')
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, '', 'application/pdf', '', false));
        $('#modalPDF').modal('show');
        this.evento.tarifasDelEvento.filter((dt) => dt.valor = this.util.miles(dt.valor));
      }, err => this.util.messageError(err));
  }

  sendEmailCotizacion() {
    let b = {
      evento: this.evento,
      cotizacion: this.cotizacion,
      email: this.userSession.email
    };

    this.apiService.post(b, 'emailServices/enviarMailCotizacion')
    .subscribe((res: any) => {
      if (res != null && res != '') {
        this.util.notification('warning','Ups!', res);
      }else if (res == null || res == '') {
        this.util.notification('success','genial!', 'Email enviado');
      }
    }, err => this.util.messageError(err));
  }

  selectedCotizacion(id: number) {
    let i = this.cotizacionesSeleccionadas.indexOf(id);

    if (i == -1) {
      this.cotizacionesSeleccionadas.push(id);
    }else {
      this.cotizacionesSeleccionadas.splice(i, 1);
    }
  }

  generateComparativo() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando...');
    if (this.cotizacionesSeleccionadas.length > 0) {
      let b = {
        idEvento: this.evento.id,
        cotiSeleccionadas: this.cotizacionesSeleccionadas
      };

      this.apiService.postByData(b, 'pdfServices/generarPDFCompatarivoProveedores')
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        $('#nav-cotizaciones').find('table input[type="checkbox"]').each(function(i) { $(this).prop('checked',false) });
        this.cotizacionesSeleccionadas = [];
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, 'comparativo', 'application/pdf', '', false));
        $('#modalPDF').modal('show');
        this.onlyCloseCurrentModal = true;
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning','Validación','Debe de seleccionar mínimo una cotización');
    }
  }

  setLugar(idCotizacion: number) {
    this.evento.idLugar = idCotizacion;
    this.save();
  }

  aprobar(item: any) {
    let co = new CotizacionEvento();
    co.id = item.id;
    co.aprobado = !item.aprobado;
    this.post(co, 'eventos/saveCotizacionEvento/true', (data: any)=>{
      item.aprobado = data.aprobado;
      if (data.aprobado) {
        this.util.notification('success', 'Cotización','Cotización aprobada');
      }else {
        this.util.notification('success', 'Cotización','Cotización desaprobada');
      }
    });
  }

  exportCotizacion(idCot: any) {
    let b = {
      idEvento: this.evento.id,
      idCotizacion: idCot
    };
    this.apiService.postByData(b, 'pdfServices/generarExcelCotizacion')
    .subscribe((res: any) => {
      this.util.saveAsFile(res, 'Cotizacion_'+idCot, 'application/vnd.ms-excel', '.xls', true);
    }, err => this.util.messageError(err));
  }

  calTarifasEvento() {
    // for (const ite of this.evento.tarifasDelEvento) {
    //   ite.valor = Number(this.util.removeMiles(ite.valor))
    // }
    $('.loading').removeClass('hide-loading').find('.text-load').text('Calculando...');

    this.apiService.post({idEvento: this.evento.id}, 'eventos/calcularTarifasEvento')
    .subscribe((res: any) => {
      if (res.length > 0) {
        res.filter((dt: any) => {dt.valorFacturado = this.util.miles(Number(dt.valorFacturado)); dt.valorEvento = this.util.miles(Number(dt.valorEvento))});
        this.dataCalTarProve = res;
        $('#modalCalTarEvento').modal('show');
      }else {
        this.util.notification('info','Vaya!','No hay ningun lugar seleccionado del evento!');
      }
      $('.loading').addClass('hide-loading');
      // this.evento.tarifasDelEvento.filter((dt) => dt.valor = this.util.miles(dt.valor));
    }, err => this.util.messageError(err));
  }

  calIncrementoCot(porcentaje: number, objCal: any) {
    porcentaje = Number(porcentaje);

    if (porcentaje != 0) {
      objCal.valorEvento = this.util.miles((this.util.removeMiles(objCal.valorFacturado) * (1 + (porcentaje / 100))));
    }
  }

  saveCalTarEvento() {
    for (const itm of this.dataCalTarProve) {
      if (Number(this.util.removeMiles(itm.valorEvento)) > 0) {
        let elem = this.evento.tarifasDelEvento.filter((d: any) => d.concepto_id == Number(itm.idConcepto))[0];

        if (elem != null && elem != undefined) {
          elem.valor = Number(this.util.removeMiles(itm.valorEvento));
        }else {
          let tarifa: TarifaEvento = new TarifaEvento();
          tarifa.evento_id = this.evento.id;
          tarifa.concepto_id = Number(itm.idConcepto);
          tarifa.valor = Number(itm.valorEvento);
          this.evento.tarifasDelEvento.push(tarifa);
        }
      }
    }

    this.apiService.post(this.evento.tarifasDelEvento, 'eventos/saveTarifas')
    .subscribe((res: any) => {
      this.evento.tarifasDelEvento = res;
      this.util.notification('success','Genial!','Datos guardados!');
    }, err => this.util.messageError(err));
  }

  calFacturaCotizacion() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Calculando...');
    this.cotizacion.factura = new DocumentoContable();
    let b = {
      evento: this.evento,
      cotizacion: this.cotizacion
    }
    this.evento.valorPresupuesto = this.util.removeMiles(this.evento.valorPresupuesto);
    if (this.evento.tarifasDelEvento != null) {
      for (const ite of this.evento.tarifasDelEvento) {
        ite.valor = Number(this.util.removeMiles(ite.valor))
      }
    }
    this.post(b, 'eventos/calcularFacturaProveedor', (facturaProveedor: any) => {
      this.cotizacion.factura = facturaProveedor;
      this.showFacCoti = true;
      $('.loading').addClass('hide-loading');
    });
  }

  saveFacCotizacion() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validar('fac_cotizacion')) {
      this.post(this.cotizacion, 'eventos/saveFacturaCotizacion', (res: any) => {
        this.cotizacion = res;
        $('.loading').addClass('hide-loading');
        this.util.notification('success', 'Genial', 'Factura Guardada!');
      });
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', 'Validación', 'El campo número es obligatorio');
    }
  }

  legalizarGasto() {
    let b = {
      evento: this.evento,
      cotizacion: this.cotizacion,
      idUsuario: this.userSession.id,
      tipo: 'gastos'
    };

    this.post(b, 'eventos/agregarGastos',(res: any) => {
      this.evento.gastos.push(res);
    })
  }

  // ------- End Cotización ------- //

  openBeneficiario() {
    if (this.anticipo.idBeneficiario != null && this.anticipo.idBeneficiario != 0) {
      this.provModal.getProveedor(this.anticipo.idBeneficiario, true);
      $('#modalProvider').on('shown.bs.modal', function (e) {
        $('#modalEvento').addClass('z-index-1');
      });
      $('#modalProvider').on('hidden.bs.modal', function (e) {
        $('#modalEvento').removeClass('z-index-1');
      });
    }
    else {
      //this.provModal.addProveedor();
    }
  }

  // ------- Legalizacion Gastos y Viaticos ------- //

  resetGasto() {
    this.hiddeModalEvento = true;
    this.gasto = new LegalizacionGasto();

    if (this.currentTab == 11)
      this.viatico = new Viatico();
  }

  generatePDFCollectionAccount() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando Cuenta Cobro...');
    let j = {
      idEvento: this.evento.id,
      idLegalizacion: this.gasto.id,
      sede: this.evento.sede.nombre.substr(0, this.evento.sede.nombre.indexOf(' ')),
      nombreEvento: this.evento.nombre,
      idContrato: this.evento.idContrato
    };
    let api = (this.currentTab == 11)?'generarPDFViaticoCuentaCobro':'generarPDFGastoCuentaCobro';
    this.apiService.postByData(j, 'pdfServices/'+api)
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, 'Cuenta Cobro', 'application/pdf', '.pdf', false));
        $('#modalPDF').modal('show');
      }, err => this.util.messageError(err));
  }

  sumaValorGasto(val: string) {
    let tempVal = Number(this.util.removeMiles(val));
    this.gasto.valor = this.util.miles(tempVal);
  }

  saveLegalizacion() {
    let txtLoad = (this.currentTab == 11)?'Viatico':'Legalización';
    let api = (this.currentTab == 11)?'saveViatico':'saveLegalizacion';
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando '+txtLoad+'...');

    if (this.gasto.idUsuario == 0)
      this.gasto.idUsuario = this.userSession.id;

    if (this.gasto.idEvento == 0)
      this.gasto.idEvento = this.evento.id;

    if (this.currentTab == 11) {
      this.gasto.nombres = (this.userSession.nombres == null) ? '':this.userSession.nombres;
      this.gasto.apellidos = (this.userSession.apellidos == null) ? '':this.userSession.apellidos;
      this.gasto.identificacion = (this.userSession.identificacion == null) ? '':this.userSession.identificacion;
    }

    if (this.gasto.tipo == 'TRANSPORTE' || this.gasto.tipo == 'TRANSPORTE_VIATICOS')
      this.gasto.valorTransporte = Number(this.util.removeMiles(this.gasto.valorTransporte));

    if (this.gasto.tipo == 'ALOJAMIENTO' || this.gasto.tipo == 'ALOJAMIENTO_VIATICOS')
      this.gasto.valorAlojamiento = Number(this.util.removeMiles(this.gasto.valorAlojamiento));

    if (this.gasto.tipo == 'ALIMENTACION' || this.gasto.tipo == 'ALIMENTACION_VIATICOS')
      this.gasto.valorAlimentacion = Number(this.util.removeMiles(this.gasto.valorAlimentacion));

    this.gasto.valorEvento = Number(this.util.removeMiles(this.gasto.valorEvento));
    this.gasto.valor = Number(this.util.removeMiles(this.gasto.valor));
    this.gasto.nombreUsuario = this.userSession.nombres;
    this.gasto.fecha = new Date(this.gasto.fecha+'T00:00:00');
    console.log(this.gasto);

    this.apiService.post(this.gasto, 'eventos/'+api)
    .subscribe((res: any) => {
      if (res) {
        if (this.currentTab == 11) {
          this.totalViaticos += res.valor;

          let vtc = this.dataViaticos.filter((dt) => dt.id == res.id)[0];
          if (vtc != null || vtc != undefined) {
            this.dataViaticos = this.dataViaticos.filter((dt) => dt.id !== res.id);
          }
          this.dataViaticos.push(res);
        }else {
          this.evento.totalGastos = res.totalGastosEvento;

          let gts = this.dataGastos.filter((dt) => dt.id == res.id)[0];
          if (gts != null || gts != undefined) {
            this.dataGastos = this.dataGastos.filter((dt) => dt.id !== res.id);
          }
          this.dataGastos.push(res);
        }
        this.gasto = res;
        if (this.gasto.tipo == 'TRANSPORTE' || this.gasto.tipo == 'TRANSPORTE_VIATICOS')
          this.gasto.valorTransporte = this.util.miles(this.gasto.valorTransporte);

        if (this.gasto.tipo == 'ALOJAMIENTO' || this.gasto.tipo == 'ALOJAMIENTO_VIATICOS')
          this.gasto.valorAlojamiento = this.util.miles(this.gasto.valorAlojamiento);

        if (this.gasto.tipo == 'ALIMENTACION')
          this.gasto.valorAlimentacion = this.util.miles(this.gasto.valorAlimentacion);

        this.gasto.valorEvento = this.util.miles(this.gasto.valorEvento);
        this.gasto.valor = this.util.miles(this.gasto.valor);
        this.gasto.fecha = this.datePipe.transform(res.fecha, 'yyyy-MM-dd');
        this.refresh.emit(res);
        $('#modalAddLegalizacion').modal('hide');
        $('.loading').addClass('hide-loading');
        this.util.notification('success','Genial!','Se ha agregado la Legalización');
      }
    }, err => this.util.messageError(err));
  }

  countDays(f1: string, f2: string): Number {
    const fechaini = new Date(f1),
    fechafin = new Date(f2),
    dias = fechafin.getTime()-fechaini.getTime(),
    countDias = (Math.round(dias/(1000*60*60*24))+1);
    return countDias;
  }

  getLegalizacion(id: number) {
    let txtLoad = (this.currentTab == 11)?'Viatico':'Legalización';
    let api = (this.currentTab == 11)?'getViatico':'getLegalizacion';
    $('.loading').removeClass('hide-loading').find('.text-load').text('Buscando '+txtLoad+'...');
    this.resetGasto();
    this.apiService.get('eventos/'+api+'/'+this.evento.id+'/'+id)
    .subscribe((res) => {
      if (res != null) {

        if (res.tipo == 'TRANSPORTE')
          res.valorTransporte = this.util.miles(res.valorTransporte);

        if (res.tipo == 'ALOJAMIENTO')
          res.valorAlojamiento = this.util.miles(res.valorAlojamiento);

        if (res.tipo == 'ALIMENTACION')
          res.valorAlimentacion = this.util.miles(res.valorAlimentacion);

        res.valorEvento = this.util.miles(res.valorEvento);
        res.valor = this.util.miles(res.valor);
        res.fecha = this.datePipe.transform(res.fecha, 'yyyy-MM-dd');
        this.gasto = res;
        $('.loading').addClass('hide-loading');
        $('#modalAddLegalizacion').modal('show');
      }else {
        this.util.notification('info','Ups!','Legalización no encontrada');
      }
    }, err => this.util.messageError(err));
  }

  documentoEquivalente(docEquivalente: any) {
    let api = (this.currentTab == 11)?'generarDocumentoEquivalenteViaticos':'generarDocumentoEquivalenteGastos';
    let id = (docEquivalente != null)?docEquivalente.id:0;

    this.apiService.get('eventos/'+api+'/'+this.evento.id+'/'+id)
    .subscribe((res) => {
      if (res) {
        if (docEquivalente != null)
          docEquivalente.numeroDocEquivalente = res.numeroDocEquivalente;

        this.generatePDFDocuEquivalente(id);
      }
    }, err => this.util.messageError(err));
  }

  generatePDFDocuEquivalente(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando Documento Equivalente...');
    let j = {
      idEvento: this.evento.id,
      idContrato: this.evento.idContrato,
      idLegalizacion: id,
      idUsuario: this.userSession.id
    }
    let api = (this.currentTab == 11)?'generarPDFDocumentoEquivalenteViatico':'generarPDFDocumentoEquivalenteGasto';

    this.apiService.postByData(j, 'pdfServices/'+api)
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, 'Documento Equivalente', 'application/pdf', '.pdf', false));
        $('#modalPDF').modal('show');
        this.onlyCloseCurrentModal = true;
      }, err => this.util.messageError(err));
  }

  exportLegalizacion() {
    this.apiService.postByData({idEvento: this.evento.id}, 'pdfServices/generarExcelLegalizaciones')
    .subscribe((res: any) => {
      this.util.saveAsFile(res, 'Gastos_'+this.evento.id, 'application/vnd.ms-excel', '.xls', true);
    }, err => this.util.messageError(err));
  }

  generarPDFLegalizacionGastos() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Generando Legalización Gastos...');
    let j = {
      idEvento: this.evento.id,
      idContrato: this.evento.idContrato,
      idUsuario: this.userSession.id,
      anticipo: this.util.removeMiles(this.fieldAnticipos).toString()
    }

    this.apiService.postByData(j, 'pdfServices/generarPDFLegalizacionGastos')
      .subscribe((res: any) => {
        $('.loading').addClass('hide-loading');
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, 'Legalización Gastos', 'application/pdf', '.pdf', false));
        $('#modalPDF').modal('show');
      }, err => this.util.messageError(err));
  }

  addTiquete() {
    this.hiddeModalEvento = true;
    this.tiquete = new TiqueteEvento();
    $('#modalTiquete').modal('show');
  }

  saveTiquete() {
    const hS = new Date(),hR = new Date();
    hS.setHours(Number(this.tiqueteHoraS.hora));
    hS.setMinutes(Number(this.tiqueteHoraS.minuto));
    hR.setHours(Number(this.tiqueteHoraR.hora));
    hR.setMinutes(Number(this.tiqueteHoraR.minuto));

    this.tiquete.horaSalida = hS;
    this.tiquete.horaRegreso = hR;

    if (this.validar('tiquete')) {
      this.tiquete.valor = Number(this.util.removeMiles(this.tiquete.valor));
      this.tiquete.tasas = Number(this.util.removeMiles(this.tiquete.tasas));
      this.tiquete.fee = Number(this.util.removeMiles(this.tiquete.fee));
      this.tiquete.valorRevisado = Number(this.util.removeMiles(this.tiquete.valorRevisado));
      this.tiquete.impuestos = Number(this.util.removeMiles(this.tiquete.impuestos));
      this.tiquete.administrativo = Number(this.util.removeMiles(this.tiquete.administrativo));
      this.tiquete.modificacion = Number(this.util.removeMiles(this.tiquete.modificacion));
      this.tiquete.total = (this.tiquete.valor + this.tiquete.impuestos + this.tiquete.tasas + this.tiquete.administrativo + this.tiquete.fee + this.tiquete.modificacion + this.tiquete.valorRevisado)
      this.tiquete.idEvento = this.evento.id;
      this.tiquete.fechaRegreso = new Date(this.tiquete.fechaRegreso+'T00:00:00');
      this.tiquete.fechaSalida = new Date(this.tiquete.fechaSalida+'T00:00:00');

      let bdy = {
        estado: this.evento.idEstado,
        nombre: this.evento.nombre,
        idUsuario: this.userSession.id,
        fechaInicio: this.evento.fechaInicioEvento,
        tiquete: this.tiquete
      };

      this.apiService.post(bdy,'eventos/saveTiquete')
      .subscribe((res: TiqueteEvento) => {

        if (res) {
          this.setTiquete(res);
          this.totalValTiquetes += res.total;

          let tqt = this.dataTiquete.filter((dt) => dt.localizador == res.localizador)[0];
          if (tqt != null || tqt != undefined) {
            this.dataTiquete = this.dataTiquete.filter((dt) => dt.localizador !== res.localizador);
          }

          this.dataTiquete.push(res);
          this.util.notification('success','Genial!','Tiquete Generado');
        }
      }, err => this.util.messageError(err));
    } else {
      this.util.notification('warning', 'Ups!', 'Los campos con (*) son obligatorios');
    }
  }

  setTiquete(res: any) {
    this.tiquete = res;
    this.tiquete.fechaRegreso = this.datePipe.transform(this.tiquete.fechaRegreso, 'yyyy-MM-dd');
    this.tiquete.fechaSalida = this.datePipe.transform(this.tiquete.fechaSalida, 'yyyy-MM-dd');
    this.tiquete.valor = this.util.miles(this.tiquete.valor);
    this.tiquete.tasas = this.util.miles(this.tiquete.tasas);
    this.tiquete.fee = this.util.miles(this.tiquete.fee);
    this.tiquete.valorRevisado = this.util.miles(this.tiquete.valorRevisado);
    this.tiquete.impuestos = this.util.miles(this.tiquete.impuestos);
    this.tiquete.administrativo = this.util.miles(this.tiquete.administrativo);
    this.tiquete.modificacion = this.util.miles(this.tiquete.modificacion);
    this.tiquete.total = this.util.miles(this.tiquete.total);
    let hS = new Date(this.tiquete.horaSalida), hR = new Date(this.tiquete.horaRegreso);
    this.tiqueteHoraS = {hora: (hS.getHours() < 10 ? '0'+hS.getHours(): ''+hS.getHours()), minuto: (hS.getMinutes() == 0 ? '0'+hS.getMinutes(): ''+hS.getMinutes())}
    this.tiqueteHoraR = {hora: (hR.getHours() < 10 ? '0'+hR.getHours(): ''+hR.getHours()), minuto: (hR.getMinutes() == 0 ? '0'+hR.getMinutes(): ''+hR.getMinutes())}
  }

  openTiquete(idEvento: number, localizador: string) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.get('eventos/getTiquete/'+idEvento+'/'+localizador, (res: any) => {
      this.setTiquete(res);
      this.hiddeModalEvento = true;
      $('.loading').addClass('hide-loading');
      $('#modalTiquete').modal('show');
    });
  }

  exportTiquete() {
    this.apiService.postByData({idEvento: this.evento.id}, 'pdfServices/generarExcelTiquetes')
    .subscribe((res: any) => {
      this.util.saveAsFile(res, 'Tiquetes_'+this.evento.id, 'application/vnd.ms-excel', '.xls', true);
    }, err => this.util.messageError(err));
  }
}
