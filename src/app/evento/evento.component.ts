import { AfterViewInit, Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SearchI } from '../models/eventos/searchI';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import * as CanvasJS from './../../assets/js/canvasjs.min';
import { DatePipe } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { EventoHomeI } from '../models/eventos/evento/evento-homeI';
import { Evento } from '../models/eventos/evento/evento';
import { EventoTemplateComponent } from '../evento-template/evento-template.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';
declare var $: any;

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.scss']
})
export class EventoComponent implements OnInit, AfterViewInit {

  arrayEventos = new MatTableDataSource<EventoHomeI>();
  arrayFilterEventos = new MatTableDataSource<EventoHomeI>();
  arrayReports = new MatTableDataSource<EventoHomeI>();

  // ----- Objeto Principal
  evento: Evento = new Evento();
  showInfoFactura = false;

  arrAgrupados = [];
  agrupador = '';
  openReporteBuscador = false;
  hiddeModalEvento = false;

  searchI: SearchI = {
    idContrato: 0,
    idEvento: 0,
    fechaInicio: null,
    fechaFin: null,
    idMunicipio: '0',
    idEstado: '0'
  };

  sumatoriasDetail = {
    totalContratado: 0,
    totalEjecutado: 0,
    totalFacturado: 0,
    totalProveedores: 0,
    totalGastos: 0
  }

  // ---- Propertys of custom select
  _disabled = false;

  // ---- Select de contratos
  dataContratos = [];

  // ---- Facturación
  noCalcularInter = false;
  noCalcularDesc = false;

  showColsEvento: string[] = ['id', 'nombreEstado', 'nombre', 'nombreContrato', 'cdp', 'orden', 'fechaInicioEvento', 'dias', 'nombreTipoEvento', 'nombreMunicipio', 'nroAsistentes','accion'];
  showColsReporte: string[] = ['id', 'nombreContrato', 'nombre', 'nombreMunicipio', 'fechaInicioEvento', 'cdp', 'nroAsistentes', 'responsableActual', 'nombreEstado', 'totalContratado', 'totalEjecutado', 'totalFacturado', 'totalProveedores', 'totalGastos'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sortReports: MatSort;
  @ViewChild(EventoTemplateComponent, {static: true}) eventoTempModal: EventoTemplateComponent;
  @ViewChildren('empTbSort') empTbSort: QueryList<MatSort>;
  @ViewChild('empTbSortPr') empTbSortPr: MatSort;

  constructor(private apiService: AdescubrirService, public util: UtilityService, private datePipe: DatePipe, private _liveAnnouncer: LiveAnnouncer) {}

  ngAfterViewInit() {
    this.arrayEventos.sort = this.empTbSortPr;
    this.arrayReports.sort = this.empTbSort.last;
  }

  ngOnInit(): void {
    const userSession = this.apiService._usuarioSesion;

    this.get('administracion/getNombresContratosPorEstado/A', (dt: any) => {
      dt.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      for (const userRol of userSession.roles) {
        for (const it of dt) {
          if ((userRol.contratoId === -1 || userRol.contratoId === Number(it.contratoId)) && this.dataContratos.indexOf(it) === -1) {
            this.dataContratos.push(it);
          }
        }
      }
    });
    this.jqueryFunction();
  }

  jqueryFunction() {
    $('#modalAgrupador').on('shown.bs.modal', function (e) {
      $('#modalReporte').addClass('z-index-1');
    });
    $('#modalAgrupador').on('hidden.bs.modal', () => {
      $('#modalReporte').removeClass('z-index-1');
    });
  }

  addEvento() {
    this.eventoTempModal.addEvento();
  }

  open(idEvento: number) {
    this.eventoTempModal.open(idEvento);
  }

  get(ruta: string, callback: any) {
    this.apiService.get(ruta)
    .subscribe((res: any) => {
      callback(res);
    })
  }

  search() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Buscando...');
    this.apiService.post(this.searchI, 'eventos/homeEvento')
    .subscribe((res: any) => {
      $('#modalSearch').modal('hide');
      for (const it of res) {
        it['dias'] = this.countDays(it.fechaInicioEvento, it.fechaFinEvento);
      }
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayEventos.data = res;
      this.arrayReports.data = res;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  countDays(f1: string, f2: string): Number {
    const fechaini = new Date(f1),
    fechafin = new Date(f2),
    dias = fechafin.getTime()-fechaini.getTime(),
    countDias = (Math.round(dias/(1000*60*60*24))+1);
    return countDias;
  }

  agrupar(agrupador: string, filtro: string) {
    let obj = {}, grupo = '';
    this.arrAgrupados = [];
    this.agrupador = agrupador;

    if (filtro === 'EJECUTADO') {
      grupo = 'EJECUTADO,FACTURADO';
    }
    if (filtro === 'PROGRAMADO') {
      grupo = 'SOLICITADO';
    }

    for (const eventoLocal of this.arrayEventos.data) {
      const clave = this.getClave(eventoLocal, agrupador);

      if (filtro === '' || grupo.includes(eventoLocal.nombreEstado)) {
        if (obj[clave] !== undefined) {
          obj[clave].cantidad += 1;
          obj[clave].totalContratado += eventoLocal.totalContratado;
          obj[clave].totalFacturado += eventoLocal.totalFacturado;
          obj[clave].totalEjecutado += eventoLocal.totalEjecutado;
          obj[clave].totalProveedores += eventoLocal.totalProveedores;
          obj[clave].totalGastos += eventoLocal.totalGastos;
        }else {
          obj[clave] = {
            clave: clave,
            cantidad: 1,
            totalContratado: eventoLocal.totalContratado,
            totalEjecutado: eventoLocal.totalEjecutado,
            totalFacturado: eventoLocal.totalFacturado,
            totalProveedores: eventoLocal.totalProveedores,
            totalGastos: eventoLocal.totalGastos
          };
        }
      }
    }

    for (const key in obj) {
      this.arrAgrupados.push(obj[key]);
    }

    this.generateChart();
    $('#modalAgrupador').modal('show');
  }

  getClave(evento: any, campo: string) {
      if (campo === 'estado')
      {
        return evento.nombreEstado;
      }
      if (campo === 'dependencia')
      {
        return evento.texto1;
      }
      if (campo === 'departamento')
      {
        return evento.nombreDepartamento;
      }
      if (campo === 'municipio')
      {
        return evento.nombreMunicipio;
      }
      if (campo === 'vereda')
      {
        return evento.nombreVereda;
      }
      if (campo === 'resguardo')
      {
        return evento.nombreResguardo;
      }
      if (campo === 'comunidad')
      {
        return evento.nombreComunidad;
      }
      if (campo === 'kumpania')
      {
        return evento.nombreKumpania;
      }
      if (campo == 'coordinador')
      {
        return evento.nombreCoordinador;
      }
      return '';
  }

  generateChart() {
    let ar = [];
    for (const itm of this.arrAgrupados) {
      let j = {
        y: itm.cantidad,
        name: itm.clave
      };
      ar.push(j);
    }
    let chart = new CanvasJS.Chart('chartContainer', {
      theme: 'light2',
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: this.agrupador
      },
      data: [{
        type: 'pie',
        showInLegend: true,
        toolTipContent: '<b>{name}</b>: {y} ',
        indexLabel: '{name} - #percent%',
        dataPoints: ar
      }]
    });

    chart.render();
  }

  exportExcel() {
    if (this.searchI.idContrato != 0 || (this.searchI.fechaInicio != null && this.searchI.fechaFin != null) || this.searchI.idEstado != '0' || this.searchI.idEvento != 0) {
      $('.loading').removeClass('hide-loading').find('.text-load').text('Exportando Eventos...');
      let b = {
        idContrato: this.searchI.idContrato
      };

      this.apiService.postByData(this.searchI, 'pdfServices/generarExcelEventos')
      .subscribe((res: any) => {
        this.util.saveAsFile(res, 'eventos', 'application/vnd.ms-excel', '.xls', true);
        $('.loading').addClass('hide-loading');
      }, err => this.util.messageError(err));
    }
  }

  async showDetail(status: string, filter: boolean = true) {
    this.sumatoriasDetail = {totalContratado: 0,totalEjecutado: 0,totalFacturado: 0,totalProveedores: 0,totalGastos: 0};
    let data = [];
    if (filter) {
      this.arrayFilterEventos.data = this.arrayReports.data.filter((eve: EventoHomeI) => ((this.agrupador == 'coordinador')?eve.nombreCoordinador:eve.nombreEstado) == status);
      data = this.arrayFilterEventos.data;
    }else {
      data = this.arrayReports.data;
    }

    for await (const iterator of data) {
      this.sumatoriasDetail.totalContratado += iterator.totalContratado;
      this.sumatoriasDetail.totalEjecutado += iterator.totalEjecutado;
      this.sumatoriasDetail.totalFacturado += iterator.totalFacturado;
      this.sumatoriasDetail.totalProveedores += iterator.totalProveedores;
      this.sumatoriasDetail.totalGastos += iterator.totalGastos;
    }
    if (filter)
      $('#modalDetail').modal('show');
  }

  openReport() {
    this.empTbSort.last.disableClear = true;
    this.arrayReports.sort = this.empTbSort.last;
    this.showDetail('', false);
    $('#modalReporte').modal('show');
  }
}
