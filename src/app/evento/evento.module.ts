import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventoRoutingModule } from './evento-routing.module';
import { EventoComponent } from './evento.component';
import { SharedModule } from '../shared/shared.module';
// import { EventosComponent } from '../eventos/eventos.component';


@NgModule({
  declarations: [
    EventoComponent,
    // EventosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EventoRoutingModule
  ]
})
export class EventoModule { }
