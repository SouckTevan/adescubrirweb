import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTable'
})
export class FilterTablePipe implements PipeTransform {

  transform(value: any, args?: any, camposABuscar?: string[]): any {
    if (!value) return null;
    if (!args) return value;
    args = args.toLowerCase();
    if (!camposABuscar) {
      return value.filter(function (item) {
        return JSON.stringify(item).toLowerCase().includes(args);
      });
    }
    else {
      return value.filter(item => {
        var encontro: false;
        var BreakException = {};

        try {
          camposABuscar.forEach(campo => {
            if (!encontro) {
              if (item['nombresRelacionados'] != undefined && item['nombresRelacionados'][campo] != undefined) {
                encontro = item['nombresRelacionados'][campo].toLowerCase().includes(args);
              } else
                encontro = item[campo].toLowerCase().includes(args);

            }

            if (encontro)
              throw BreakException
          });
        } catch (e) {
        }
        return encontro;
      });
    }
  }

}
