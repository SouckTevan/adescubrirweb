import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadorRetencionComponent } from './indicador-retencion.component';

describe('IndicadorRetencionComponent', () => {
  let component: IndicadorRetencionComponent;
  let fixture: ComponentFixture<IndicadorRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadorRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadorRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
