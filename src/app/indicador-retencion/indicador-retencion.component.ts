import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { IndicadorRetencion } from '../models/contable/indicador-retencion/indicador-retencion';
import { UtilityService } from '../utility.service';
declare var $: any;

@Component({
  selector: 'app-indicador-retencion',
  templateUrl: './indicador-retencion.component.html',
  styleUrls: ['./indicador-retencion.component.scss']
})
export class IndicadorRetencionComponent implements OnInit {

  indicadorRetencion: IndicadorRetencion = new IndicadorRetencion();;

  // -------------- Array para llenar la tabla principal
  arrayIndRetencion = new MatTableDataSource<IndicadorRetencion>();
  dataRangoNumeracion = [];

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.get();
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getIndicadoresRetencion')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayIndRetencion.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.indicadorRetencion = new IndicadorRetencion();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getIndicadorRetencion/'+id)
    .subscribe((resp: IndicadorRetencion) => {
      this.indicadorRetencion = resp;
      $('.loading').addClass('hide-loading');
      $('#modalIndicadorRetencion').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');

    if (this.validate()) {
      this.apiServices.post(this.indicadorRetencion, 'administracion/saveIndicadorRetencion')
      .subscribe((resp: IndicadorRetencion) => {
        this.indicadorRetencion = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalIndicadorRetencion').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.indicadorRetencion.nombre != '';
    const es = this.indicadorRetencion.estado != '';
    const tR = this.indicadorRetencion.tipoRetencion != '';
    const pR = this.indicadorRetencion.porcentaje != 0;

    return nm && es && tR && pR;
  }

  exportExcel() {
    if (this.arrayIndRetencion.data != undefined && this.arrayIndRetencion.data.length > 0)
      this.util.exportAsExcelFile(this.arrayIndRetencion.data, 'IndicadorRetencion');
  }

  applyFilter(filterValue: string) {
    this.arrayIndRetencion.filter = filterValue.trim().toLowerCase();
  }

}
