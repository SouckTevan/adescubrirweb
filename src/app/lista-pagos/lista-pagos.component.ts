import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AdescubrirService } from '../adescubrir.service';
import { EventoTemplateComponent } from '../evento-template/evento-template.component';
import { Persona } from '../models/admin/persona/persona';
import { DocumentoContable } from '../models/contable/documento-contable/documento-contable';
import { Pago } from '../models/contable/pago/pago';
import { UtilityService } from '../utility.service';
declare var $: any;

@Component({
  selector: 'app-lista-pagos',
  templateUrl: './lista-pagos.component.html',
  styleUrls: ['./lista-pagos.component.scss']
})
export class ListaPagosComponent implements OnInit {

  d = {id: 0,fechaInicial: new Date(),tipo: 3,idEvento:0,fechaFin: new Date(),estado: '',contrato:''};
  groupData: any[] = [];
  arrayPagosTable = new MatTableDataSource<DocumentoContable>([]);
  showColsPagos: string[] = ['id','idEvento','estadoEvento','nombreConcepto','fechaInicio','identificacion','nombre','total','valor','saldo','usuario','estado','accion'];
  dataBancos = [];
  dataPayments = [];
  pago: Pago = new Pago();
  documento: any;
  isAnticipoNew = false;
  valorAnticipo = 0;
  anticipoPagos = [];
  total: number = 0;
  cantidad: number = 0;
  isGroup: boolean = true;
  assetUrl: SafeResourceUrl;

  // ------ File of Documents
  files: File[] = [];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(EventoTemplateComponent, {static: true}) eventoTempModal: EventoTemplateComponent;
  @ViewChildren('tb') tb: QueryList<(MatTable<MatTableDataSource<DocumentoContable>>)>;

  constructor(private apiService: AdescubrirService, public util: UtilityService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    // ---------- Paginador
    // this.arrayPagos.paginator = this.paginator;
  }

  consult() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    this.apiService.post(this.d, 'documentoContable/getDocumentosContables')
    .subscribe((res: any) => {
      this.arrayPagosTable.data = [];
      
      this.groupData = [];
      for (const key in res) {
        if (key == 'total') {
          this.total = res['total'];
        }else if (key == 'cantidad') {
          this.cantidad = res['cantidad'];
        }else {
          (res[key].data).sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
          for (const iterator of res[key].data) {
            this.arrayPagosTable.data.push(iterator);
          }
          this.groupData.push(res[key]);
        }
      }
      $('.loading').addClass('hide-loading');
    }, (e:any) => this.util.messageError(e));
  }

  async sortData(sort: Sort, dataParam: any, position: number) {
    const data = dataParam.slice();

    if (!sort.active || sort.direction === '') {
      if (position != -1)
        (this.tb.toArray()[position]).dataSource = new MatTableDataSource(data);
      else
        this.arrayPagosTable.data = data;
      return;
    }

    let sortData = await data.sort((a, b) => {
      return this.compare(a[sort.active], b[sort.active], (sort.direction === 'asc'));
    });
    if (position != -1)
      (this.tb.toArray()[position]).dataSource = new MatTableDataSource(sortData);
    else
      this.arrayPagosTable.data = sortData;
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  applyFilter(filterValue: string) {
    this.arrayPagosTable.filter = filterValue.trim().toLowerCase();
  }

  autorizar(id: number, isAnticipo: boolean) {
    let api = (isAnticipo)?'autorizarAnticipo':'autorizar';
    this.apiService.get('documentoContable/'+api+'/'+id)
    .subscribe((res: any) => {
      if (res) {
        this.util.notification('success','','Anticipo Autorizado!');
        this.consult();
      }else {
        this.util.notification('error','','Anticipo no autorizado!');
      }
    }, (e:any) => this.util.messageError(e));
  }

  generar(id: number, changeStatus: boolean, isAnticipo: boolean) {
    let api = (isAnticipo)?'generarAnticipo':'generar';
    this.apiService.get('documentoContable/'+api+'/'+id+'/'+changeStatus)
    .subscribe((res: any) => {
      //console.log(res);
      if (res) {
        this.util.notification('success','','Anticipo '+((changeStatus)?'generado!':'reenviado!'));
        this.consult();
      }else {
        this.util.notification('error','','Anticipo no '+((changeStatus)?'generado!':'reenviado!'));
      }
    }, (e:any) => this.util.messageError(e));
  }

  getDocument(idDocument: number, isAnticipo: boolean) {
    this.isAnticipoNew = isAnticipo;
    $('.loading').removeClass('hide-loading').find('.text-load').text('Buscando...');
    this.pago = new Pago();
    this.files = [];
    let api = (isAnticipo)?'getAnticipo/'+idDocument:'getDocumentoContable/'+idDocument+'/'+true;
    this.apiService.get('documentoContable/'+api)
    .subscribe((res: DocumentoContable) => {
      this.documento = res;
      if (this.documento.total != undefined) {
        this.documento.total = this.util.miles(this.documento.total);
        this.valorAnticipo = this.util.miles(this.documento.total);
      }else {
        this.documento.valor = this.util.miles(this.documento.valor);
        this.valorAnticipo = this.util.miles(this.documento.valor);
      }
      this.documento.rangonumeracion = (res.rangonumeracion == 0)?-1:res.rangonumeracion;
      this.getBanks();
    }, (e:any) => this.util.messageError(e));
  }

  getBanks() {
    this.apiService.get('administracion/getBancos')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
      this.dataBancos = res;
      this.getPaymentMethod();
    }, (e:any) => this.util.messageError(e));
  }

  getPaymentMethod() {
    this.apiService.get('administracion/getMediosPago')
    .subscribe((res) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.dataPayments = res;
      $('#modalDocument').modal('show');
      $('.loading').addClass('hide-loading');
    }, (e:any) => this.util.messageError(e));
  }

  accountSelected(d: any) {
    this.pago.cuenta = d.cuenta;
    this.pago.idBanco = d.banco_id;
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.files.length > 0 ) {
      if (this.validate()) {
        if (!this.isAnticipoNew) {
          this.documento.persona = new Persona();
        }else {
          delete this.pago.idDocumentoContable;
        }
        if (this.documento.total != undefined) { // ---> Es viejo
          this.documento.total = Number(this.util.removeMiles(this.documento.total));
          this.pago.valor = this.documento.total;
          this.pago.total = Number(this.util.removeMiles(this.valorAnticipo));
          if (this.documento.total <= this.documento.saldo) {
            this.documento.saldo = (this.documento.saldo - this.documento.total);
          }else {
            this.util.notification('info','','El valor a pagar no debe de superar el saldo');
          }
        }else { // ---> Es nuevo
          this.documento.valor = Number(this.util.removeMiles(this.documento.valor));
          this.pago.total = Number(this.util.removeMiles(this.valorAnticipo));
          if (this.documento.valor <= this.documento.saldo) {
            this.documento.saldo = (this.documento.saldo - this.pago.total);
          }else {
            this.util.notification('info','','El valor a pagar no debe de superar el saldo');
          }
        }
        //this.pago.total = (this.documento.saldo + this.documento.valor);
        //this.pago.total = Number(this.util.removeMiles(this.pago.total));
        let d = {
          documento: this.documento,
          pago: this.pago
        }

        const formData = new FormData();

        formData.append('file', this.files[0], this.files[0].name);
        formData.append('data', JSON.stringify(d));

        let api = (this.isAnticipoNew)?'saveAnticipoPago':'savePago';
        //console.log(formData);
        this.apiService.postWithFile('documentoContable/'+api, formData)
          .subscribe((res: any) => {
            $('.loading').addClass('hide-loading');
            this.util.notification('success','','Se ha realizado el pago!');
            $('#modalDocument').modal('hide');
            this.consult();
          }, (e:any) => this.util.messageError(e));
      }else {
        $('.loading').addClass('hide-loading');
      this.util.notification('info','','Los campos con (*) son obligatorios!');
      }
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('info','','El soporte es obligatorio!');
    }
  }

  onSelect(event: any) {
    if (this.files.length < 1) {
      let fileName = event.addedFiles[0].name.replace(/[^a-zA-Z 0-9.]+/g,'');
      fileName = fileName.replace(/\s/g, '');
      Object.defineProperty(event.addedFiles[0], 'name', {
        writable: true,
        value: fileName
      });
      this.files.push(...event.addedFiles);
      setTimeout(() => {
        var chi = $('.soporte-dropzone ngx-dropzone-preview:last-child');
        var tp = chi.find('ngx-dropzone-label').attr('tp');
        var imgLoad = 'icon_image-min.png';
        if (tp === 'application/pdf') {
          imgLoad = 'icono_pdf-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
          imgLoad = 'icono_excel.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
          imgLoad = 'icon_word-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
          imgLoad = 'icono_powerpoint.png';
        } else if (tp === 'text/plain') {
          imgLoad = 'icono_txt-min.png';
        }
        chi.css({'background':'url(./assets/img/'+imgLoad+') no-repeat','background-size':'contain'});
      },1000);
    }
  }

  validate(): boolean {
    let md = this.pago.idMedioPago != null && this.pago.idMedioPago != 0,
    c = (this.pago.idMedioPago == 1)?true:this.pago.cuenta != '',
    b = (this.pago.idMedioPago == 1)?true:this.pago.idBanco != 0,
    tt = (this.documento.total != undefined)?this.documento.total != '':this.documento.valor != '',
    mp = (this.pago.idMedioPago == 2)?this.pago.cheque != '':(this.pago.idMedioPago == 3)?this.pago.nroTransaccion != '':true;
    //console.log(md+' - '+c+' - '+b+' - '+tt+' - '+mp);

    return md && c && b && tt && mp;
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }
  // generateReport() {
  //   this.apiService.post({},'administracion/dinamic/ConsultaAnticipos')
  //   .subscribe((res: any) => {
  //     console.log(res);
  //   })
  // }

  loadPayments(idAnticipo: number, isNew: boolean) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Consultando...');
    let api = (isNew)?'getAnticipoPagos/'+idAnticipo:'getDocumentoPagos/'+idAnticipo;
    this.apiService.get('documentoContable/'+api)
    .subscribe((res: any) => {
      //console.log(res);
      if (res != null && res != undefined) {
        this.anticipoPagos = res;
      }
      $('.loading').addClass('hide-loading');
      $('#modalViewPayments').modal('show');
    }, (e:any) => this.util.messageError(e));
  }

  openPyment(path: string) {
    this.apiService.postByData({path}, 'pdfServices/openFile')
      .subscribe((res: any) => {
        this.assetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.util.saveAsFile(res, '', 'application/pdf', '', false));
        $('#modalPDF').modal('show');
        $('#modalViewPayments').addClass('z-index-1');
      }, err => this.util.messageError(err));
  }

  closeModal() {
    $('#modalViewPayments').removeClass('z-index-1');
  }

  openEvent(idEvento: number) {
    this.eventoTempModal.open(idEvento);
  }
}
