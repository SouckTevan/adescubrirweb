import { Component, OnInit } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Router } from '@angular/router';
import { UtilityService } from '../utility.service';
import { Usuario } from '../models/usuario/usuario';
declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  user: string = '';
  pass: string = '';
  loading: boolean = false;
  loadingGoogle: boolean = false;

  constructor(private authService: AuthService, public apiService: AdescubrirService, public router: Router, private util: UtilityService) { }

  ngOnInit() {
    this.authService.authState.subscribe((usr) => {
      this.apiService.setLoggedIn(true);
      this.loadingGoogle = true;
      if (usr != null) {
        this.getByEmail(usr.email, usr.photoUrl);
      }else {
        this.router.navigate(['/']);
        this.loadingGoogle = false;
      }
    });

    this.jqueryFunction();
  }

  jqueryFunction() {
    $('.container-img').css({'background':'url("./assets/img/event_1.jpg") no-repeat ', 'background-size': 'cover'});
    $('.btn-google').css({'background':'url("./assets/img/google_img.png") no-repeat #fff 5%','background-size': '12%'});
    $('.logoDca').css({'background':'url("./assets/img/logoDCA.png") no-repeat','background-size': 'contain'});   
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInCredentials() {
    this.loading = true;
    if (this.user != '' && this.pass != '') {
      this.generateToken(false, null);
    }else {
      this.util.notification('info', '', 'El usuario y la contraseña son requeridos!');
      this.loading = false;
    }
  }

  getAccessCredentials() {
    this.apiService.get('administracion/login/'+this.user+'/'+this.pass).subscribe((resp: Usuario) => {
      localStorage.setItem('usuario',JSON.stringify(resp));
      if (resp != null) {
        
          this.apiService.setLoggedIn(true);
          this.apiService._usuarioSesion = {
            id: resp.id,
            nombre: resp.nombre,
            nombres: resp.nombres,
            apellidos: resp.apellidos,
            identificacion: resp.identificacion,
            email: resp.email,
            usuario: resp.usuario,
            photoUrl: '',
            roles: resp.roles,
            opciones: resp.opciones,
            isGoogle: false
          };
          setTimeout(() => {
            this.router.navigate(['/dashboard']);
            this.loading = false;
          }, 1000);
      }else {
        this.util.notification('info', '', 'El usuario y/o la contraseña son incorrectas');
        this.loading = false;
      }
    },(err) => {
      this.util.messageError(err);
      this.loading = false;
    });
  }

  generateToken(isGoogle: boolean, usr: any) {
    this.apiService.generateToken({userName: this.user, password: this.pass}).subscribe((res: string) => {
      if (res != null && res != '') {
        this.apiService.setToken(res);
        if (!isGoogle)
          this.getAccessCredentials();
      }else {
        this.util.notification('info', '', 'El usuario y/o la contraseña son incorrectas');
        this.loading = false;
      }
    }, err => {
      console.log(err);
      if (!isGoogle) {
        this.util.notification('info', '', 'El usuario y/o la contraseña son incorrectas');
        this.loading = false;
      }
    });
  }

  getByEmail(email: string, photo: string) {
    this.apiService.get('administracion/getPersonaEmail/'+email)
      .subscribe((resp: Usuario) => {
        if (resp != null) {
          this.generateToken(true, resp);
          this.apiService._usuarioSesion = {
            id: resp.id,
            nombres: resp.nombre,
            apellidos: resp.apellidos,
            email: resp.email,
            photoUrl: photo,
            roles: resp.roles,
            opciones: resp.opciones,
            isGoogle: true
          };
          setTimeout(() => {
            this.router.navigate(['/dashboard']);
            this.loadingGoogle = false;
          }, 1000);
        }else {
          this.loadingGoogle = false;
          this.util.notification('error', '', 'El usuario no esta relacionado con Adescubrir');
        }
      });
  }

}
