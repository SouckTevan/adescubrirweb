import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { MedioPago } from '../models/contable/medio-pago/medio-pago';
import { UtilityService } from '../utility.service';
declare var $: any;


@Component({
  selector: 'app-medios-pago',
  templateUrl: './medios-pago.component.html',
  styleUrls: ['./medios-pago.component.scss']
})
export class MediosPagoComponent implements OnInit {

  medioPago: MedioPago = new MedioPago();;

  // -------------- Array para llenar la tabla principal
  arrayMedioPago = new MatTableDataSource<MedioPago>();
  dataRangoNumeracion = [];

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.get();
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getMediosPago')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayMedioPago.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.medioPago = new MedioPago();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getMedioPago/'+id)
    .subscribe((resp: MedioPago) => {
      this.medioPago = resp;
      $('.loading').addClass('hide-loading');
      $('#modalMedioPago').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');

    if (this.validate()) {
      this.apiServices.post(this.medioPago, 'administracion/saveMedioPago')
      .subscribe((resp: MedioPago) => {
        this.medioPago = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalMedioPago').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.medioPago.nombre != '';
    const es = this.medioPago.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayMedioPago.data != undefined && this.arrayMedioPago.data.length > 0)
      this.util.exportAsExcelFile(this.arrayMedioPago.data, 'MediosPago');
  }

  applyFilter(filterValue: string) {
    this.arrayMedioPago.filter = filterValue.trim().toLowerCase();
  }

}
