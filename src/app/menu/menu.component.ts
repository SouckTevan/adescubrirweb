import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';
import { UtilityService } from '../utility.service';
import { UsuarioSesion } from '../models/usuarioSesion/usuario-sesion';
declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Output() clickedItem:EventEmitter<any> = new EventEmitter();
  @Output() openMenu:EventEmitter<any> = new EventEmitter();

  name: string;
  user: UsuarioSesion;
  showMenu = false;
  openDrop = false;

  oldPassword: string = '';
  newPassword: string = '';
  confirmPassword: string = '';

  constructor(private authService: AuthService, public apiService: AdescubrirService, public router: Router, public util: UtilityService) { }

  ngOnInit() {
    this.jqueryFunction();
    this.user = this.apiService._usuarioSesion;
    this.name = this.user.nombre.substring(0,1);
  }

  jqueryFunction():void {
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();

      $('#profile-drop').on('hide.bs.dropdown', function () {
        this.openDrop = false;
      })
    });
  }

  changeMenu(item: string, positionItem: number):void {
    $('.content-items li').removeClass('active-item');
    $('.content-items li:eq('+positionItem+')').addClass('active-item');

    if (item == 'eventos') {
      $('.panel-sub-menu').removeClass('show-sub-menu');
    }else {
      if ($('.panel-sub-menu').hasClass('show-sub-menu') && $('.panel-sub-menu').hasClass('show-sub-menu sl-'+item)) {
        $('.panel-sub-menu').removeClass('show-sub-menu');
      }else {
        $('.panel-sub-menu').removeClass('sl-datos sl-tesoreria').addClass('show-sub-menu sl-'+item);
      }
    }
    let itm = {name: item, title: item};
    this.clickedItem.emit(itm);
  }

  changePassword() {
    this.oldPassword = '';
    this.newPassword = '';
    $('#modalPassword').modal('show');
  }

  signOut(): void {
    this.apiService.setLoggedIn(false);
    this.apiService.setToken('');

    if (this.apiService._usuarioSesion.isGoogle)
      this.authService.signOut()
    else
      this.router.navigate(['/']);

    this.apiService._usuarioSesion = null;
  }

  savePassword(): void {
    if (this.newPassword == this.confirmPassword) {
      $('.loading').removeClass('hide-loading');
      this.apiService.get('administracion/getPersona/' + this.user.id)
      .subscribe((res: any) => {
        if (res != null) {
          if (res.clave == this.oldPassword) {
            let pass = btoa(this.confirmPassword);
            let bdy = {
              user: this.user.id,
              _clave_: pass
            };
            this.apiService.post(bdy, 'administracion/updatePassword')
              .subscribe((res: any) => {
                console.log(res);
                $('#modalPassword').modal('hide');
                $('.loading').addClass('hide-loading');
                this.util.notification('success','Genial!','Se ha modificado la contraseña');
              }, err => this.util.messageError(err));
          }else {
            this.util.notification('warning','Contraseña','La contraseña actual es incorrecta');
          $('.loading').addClass('hide-loading');
          }
        }else {
          this.util.notification('warning','Contraseña','No existe el usuario');
          $('.loading').addClass('hide-loading');
        }
      }, err => this.util.messageError(err));
    }else {
      this.util.notification('warning','Contraseña','Las contraseñas no coinciden');
    }
  }
}
