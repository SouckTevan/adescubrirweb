export class ActivoFijo {
    id: number = 0;
    codigo: string = '';
    nombre: string = '';
    tipoActivo: string = '0';
    descripcion: string = '';
    datosTecnicos: string = '';
    serial: string = '';
    marca: string = '';
    estado: string = 'A';
    usuarioActual: string = '';
    deEventos: boolean = false;
}
