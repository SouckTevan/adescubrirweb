export class Banco {
    id: number = 0;
    nombre: string = '';
    codigoBancario: string = '';
    estado: string = 'A';
}