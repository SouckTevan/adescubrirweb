import { TipoConcepto } from '../tipo-concepto/tipo-concepto';
import { IndicadorRetencion } from '../../contable/indicador-retencion/indicador-retencion';

export class Concepto {
    id: number = 0;
    nombre: string = '';
    descripcion: string = '';
    estado: string = 'A';
    tipoConcepto: TipoConcepto = new TipoConcepto();
    nombreConcepto: string = '';
    seguroHotelero: boolean = false;
    porcentajeIva: number = 0;
    indicador: IndicadorRetencion = new IndicadorRetencion();
    noAplicaDescuento: boolean = false;
    tipoConcepto_id: number = 0;
    indicador_id: number = 0;
}