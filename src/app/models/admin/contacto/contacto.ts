import { Municipio } from '../municipio/municipio';
import { TipoContacto } from '../tipo-contacto/tipo-contacto';

export class Contacto {
    id: number = 0;
    tipoContacto_id: number = 0;
    tipoContacto: TipoContacto;
    nombre: string = '';
    direccion: string = '';
    municipio: Municipio;
    municipio_id: number = 0;
    email: string = '';
    telefono: string = '';
    fax: string = '';
    celular: string = '';
}