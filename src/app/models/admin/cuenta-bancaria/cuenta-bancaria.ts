import { Banco } from '../banco/banco';

export class CuentaBancaria {
    id: number = 0;
    banco: Banco;
    banco_id: number = 0;
    tipoCuenta: string = '';
    cuenta: string = '';
    fechaVencimiento: Date;
    franquicia: string = '';
    ccv: string = '';
    titular: string = '';
    nombreTitular: string = '';
}