import { Pais } from '../pais/pais';

export class Departamento {
    id: number = 0;
    nombre: string = '';
    estado: string = '';
    dane: string = '';
    pais_id: number = 0;
    pais: Pais = new Pais();
}