import { Persona } from '../persona/persona';

export class Estado {
    id: number = 0;
    estado: string = 'A';
    nombre: string = '';
    modulo: string = '';
    esInicial: boolean = false;
    esFinal: boolean = false;
    activo: boolean = false;
    sumarTotales: boolean = false;
    responsableEvento: boolean = false;
    coordinadorEvento: boolean = false;
    tipo: string = '';
    color: string = '';
    colorText: string = '';
    nuevoResponsable: Persona = new Persona();
    nuevo_responsable: number = -1;
}