import { Departamento } from '../departamento/departamento';

export class Municipio {
    id: number = 0;
    nombre: string = '';
    estado: string = '';
    dane: string = '';
    departamento = new Departamento();
    departamento_id: number = 0;
}