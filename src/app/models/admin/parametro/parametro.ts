export class Parametro {

    static TIPO_AUTENTICACION: string = 'TIPO_AUTENTICACION';
    static SKIN_THEME: string = 'SKIN_THEME';
    static LOGO_GRANDE_EMPRESA: string = 'LOGO_GRANDE_EMPRESA';
    static ESTADO_ACTIVO: string = 'A';
    static ESTADO_INACTIVO: string = 'I';

    id: number = 0;
    nombre: string = '';
    descripcion: string = '';
    estado: string = Parametro.ESTADO_ACTIVO;
    valor: string = '';
    fechaInicio: Date = null;
    fechaFin: Date = null;
}
