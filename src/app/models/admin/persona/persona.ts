import { Contacto } from '../contacto/contacto';
import { Municipio } from '../municipio/municipio';
import { CuentaBancaria } from '../cuenta-bancaria/cuenta-bancaria';
import { RegistroDocumento } from '../registro-documento/registro-documento';
import { Sucursal } from '../sucursal/sucursal';

export class Persona {
    id: number = 0;
    nombre: string = '';
    nombres: string = '';
    apellidos: string = '';
    razonSocial: string = '';
    nombreComercial: string = '';
    estado: string = '';
    tipoIdentificacion: string = '0';
    identificacion: string = '';
    expedicionIdentificacion: string = '';
    fechaNacimiento: Date = null;
    direccion: string = '';
    municipio: Municipio = new Municipio();
    municipio_id: number = 0;
    telefono: string = '';
    fax: string = '';
    celular: string = '';
    email: string = '';
    foto: string = '';
    contactos = new Array<Contacto>();
    cuentasBancarias = new Array<CuentaBancaria>();
    sucursales = new Array<Sucursal>();
    tipoPersona: string = '';
    granContribuyente: boolean = false;
    regimenComun: boolean = false;
    regimenEspecial: boolean = false;
    regimenSimplificado: boolean = false;
    autoRetenedorRenta: boolean = false;
    autoRetenedorIVA: boolean = false;
    autoRetenedorICA: boolean = false;

// ------ Datos para la autenticacion -----------
    usuario: string = '';
    clave: string = '';
    apoyoLogistico: boolean = false;

    documentos = new Array<RegistroDocumento>();
}