import { Persona } from '../persona/persona';
import { ProveedorTipo } from '../proveedorTipo/proveedor-tipo';
import { Tarifa } from '../tarifa/tarifa';

export class Proveedor extends Persona {
    tipoProveedor_id: number = 0;
    // private TipoProveedor tipoProveedor = new TipoProveedor();
    tarifas = new Array<Tarifa>();
    tipos = new Array<ProveedorTipo>();
}
