import { TipoProveedor } from "../tipo-proveedor/tipo-proveedor";

export class ProveedorTipo {
  idProveedor: number = 0;
  idTipo: number = 0;
  objTipo: TipoProveedor = null;
}
