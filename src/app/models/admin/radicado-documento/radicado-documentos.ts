export class RadicadoDocumentos {
    radicadoId: number = 0;
    documentoId: number = 0;
    fechaHora: Date = new Date();
    observacion: string = '';
    rutaArchivo: string = '';
    nombreArchivo: string = '';
    usuarioId: number = 0;
}