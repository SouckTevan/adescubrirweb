import { Usuario } from '../../usuario/usuario';
import { Persona } from '../persona/persona';
import { RadicadoDocumentos } from '../radicado-documento/radicado-documentos';

export class Radicado {
    id: number;
    fechaHora: Date = new Date();
    fechaRecepcion: Date = new Date();
    recibeId: number = 0;
    observaciones: string = '';
    destino: string = '0';
    ubicacion: string = '';
    tipoDocumental: string = '0';
    personaResponsableId: number = 0;
    file: File;
    clave: string = '';
    clave1: string = '';
    clave2: string = '';

    recibe: Usuario = new Usuario();
    personaResponsable: Persona = new Persona();
    anexos: RadicadoDocumentos[] = [];
    files: File[] = [];
}
