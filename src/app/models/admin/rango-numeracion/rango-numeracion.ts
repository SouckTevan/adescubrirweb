import { Parametro } from '../parametro/parametro';

export class RangoNumeracion {
    id: number = 0;
    nombre: string = '';
    estado: string = Parametro.ESTADO_ACTIVO;
    prefijo: string = '';
    numeroInicial: number = 0;
    numeroFinal: number = 0;
    numeroActual: number = 0;
    fechaInicial: Date = new Date();
    fechaFinal: Date = new Date();
    descripcion: string = '';
    textoLegal: string = '';
    mailVencimientos: string = '';
}