import { Documento } from '../documento/documento';
import { Usuario } from '../../usuario/usuario';

export class RegistroDocumento {
    id: number = 0;
    persona_id: number;
    documento_id: number = 0;
    fechaHora: Date;
    documento: Documento = new Documento();
    usuario_id: number;
    usuario: Usuario = new Usuario();
    fileType: string;
    rutaArchivo: string;
    nombreArchivo: string;
    observacion: string;
}