import { Usuario } from '../../usuario/usuario';

export class RegistroObservacion {
    id = 0;
    contrato_id: number = 0;
    fechaHora: Date = new Date();
    usuario_id: number = 0;
    usuario: Usuario = new Usuario();
    observacion: string = '';
    contador: number = 1;
}