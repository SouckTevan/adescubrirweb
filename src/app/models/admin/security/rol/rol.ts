import { RolOpcion } from '../rol-opcion/rol-opcion';

export class Rol {
    id: number = 0;
    nombre: string;
    estado: string = '0';
    contratoId: any = '-1';
    opciones = new Array<RolOpcion>();
    contratos = new Array<number>();
    estados = new Array<number>();
}