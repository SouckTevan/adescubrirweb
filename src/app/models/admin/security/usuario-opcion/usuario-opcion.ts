import { Usuario } from 'src/app/models/usuario/usuario';
import { Opcion } from '../opcion/opcion';

export class UsuarioOpcion {
    usuarioId: number = 0;
    usuario: Usuario;
    opcionId: string = '';
    opcion: Opcion;
}
