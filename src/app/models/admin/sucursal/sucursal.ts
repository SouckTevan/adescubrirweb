import { Municipio } from '../municipio/municipio';
import { CuentaBancaria } from '../cuenta-bancaria/cuenta-bancaria';

export class Sucursal {
    id: number = 0;
    municipio: Municipio;
    municipio_id: number = 0;
    nombre: string = '';
    direccion: string = '';
    telefono: string = '';
    fax: string = '';
    email: string = '';
    cuentaBancaria: CuentaBancaria;
    banco_id: number = 0;
}