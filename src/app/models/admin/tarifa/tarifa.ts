import { Sucursal } from '../sucursal/sucursal';
import { Concepto } from '../concepto/concepto';
import { TipoTarifa } from '../tipo-tarifa/tipo-tarifa';

export class Tarifa {
    proveedor_id: number = 0;
    municipio_id: number = 0;
    nombresucursal: string = '';
    concepto_id: number = 0;
    tipotarifa_id: number = 0;
    valor: number = 0;
    descripcion: string = '';
    sucursal: Sucursal = new Sucursal();
    concepto: Concepto = new Concepto();
    tipoTarifa: TipoTarifa;
}