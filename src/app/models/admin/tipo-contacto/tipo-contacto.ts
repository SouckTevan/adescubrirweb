export class TipoContacto {
    id: number = 0;
    nombre: string = '';
    estado: string = 'A';
}