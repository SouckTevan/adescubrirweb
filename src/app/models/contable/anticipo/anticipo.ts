import { Usuario } from '../../usuario/usuario';
import { Persona } from '../../admin/persona/persona';

export class Anticipo {
    id: number = 0;
    idBeneficiario: number = 0;
    beneficiario: Persona;
    concepto: string = '';
    valor: number = 0;
    idEvento: number = 0;
    fecha: Date = new Date();
    fechaVencimiento: Date = new Date();
    observacion: string = '';
    prioridad: string = '';
    creador: Usuario;
    idCreador: number = 0;
    estado: string = '';
    idAutorizador = 0;
    fechaAutorizado = null;
    tipodocumento_id = 3;

    // --> @Transient
    generar = true;
    pagado = false;
    viejo = false;
}
