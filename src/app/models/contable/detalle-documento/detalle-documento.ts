import { Concepto } from '../../admin/concepto/concepto';

export class DetalleDocumento {
    documentocontable_id: number = null;
    concepto_id: number = null;
    concepto: Concepto = new Concepto();
    cantidad: number = 0;
    valor: number = 0;
    base: number = 0;
    impuestos: number = 0;
    poImpuestos: number = 0;
    descuento: number = 0;
    total: number = 0;
    retencion: number = 0;
    poRetencion: number = 0;
    fecha: Date = new Date();
    hora: Date = new Date();
}
