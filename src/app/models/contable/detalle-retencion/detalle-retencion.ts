import { IndicadorRetencion } from '../indicador-retencion/indicador-retencion';

export class DetalleRetencion {
    documentocontable_id: number = 0;
    indicador: IndicadorRetencion = new IndicadorRetencion();
    indicador_id: number = 0;
    base: number = 0;
    valor: number = 0;
    porcentaje: number = 0;
}