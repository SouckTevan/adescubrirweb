import { Usuario } from '../../usuario/usuario';
import { Municipio } from '../../admin/municipio/municipio';
import { Persona } from '../../admin/persona/persona';
import { RangoNumeracion } from '../../admin/rango-numeracion/rango-numeracion';
import { TipoDocumentoContable } from '../tipo-documento-contable/tipo-documento-contable';
import { DetalleDocumento } from '../detalle-documento/detalle-documento';
import { Pago } from '../pago/pago';
import { DetalleRetencion } from '../detalle-retencion/detalle-retencion';

export class DocumentoContable {
    id: number = 0;
    numero: string = '';
    tipoDocumento: TipoDocumentoContable = new TipoDocumentoContable();
    tipoDocumentoTxt: string = '';
    tipodocumento_id: number;
    rangoNumeracion: RangoNumeracion;
    rangonumeracion: number = -1;
    totalPagos: number = null;
    saldo: number = null;
    persona: Persona = new Persona();
    persona_id: number;
    fecha: any = new Date();
    hora: Date = new Date();
    creador: Usuario = new Usuario();
    creador_id: number;
    municipio: Municipio = new Municipio();
    municipio_id: number;
    estado: string;
    observaciones: string;
    detalle: DetalleDocumento[] = [];
    pagos: Pago[] = new Array<Pago>();
    // Retenciones
    detalleRetencion: DetalleRetencion[] = new Array<DetalleRetencion>();
    // valores
    base: number = 0;
    impuestos: number = 0;
    total: number = 0;
    retenciones: number = 0;
    descuentos: number = 0;
    comisiones: number = 0;
    fechaVencimiento: any = new Date();
    fechaFactura: any = new Date();

    // Campos para eventos
    eventoId: number = 0;
    conceptoPago: string = '';
    prioridad: string = '';
    poDescuento: number = null;
    ivaComisiones: number = 0;
}