import { Parametro } from '../../admin/parametro/parametro';

export class IndicadorRetencion {
    id: number = 0;
    nombre: string = '';
    descripcion: string = '';
    estado: string = Parametro.ESTADO_ACTIVO;
    tipoRetencion: string = '';
    porcentaje: number;
}