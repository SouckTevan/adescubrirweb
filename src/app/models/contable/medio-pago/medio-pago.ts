import { Parametro } from '../../admin/parametro/parametro';

export class MedioPago {
    id: number = 0;
    nombre: string = '';
    estado: string = Parametro.ESTADO_ACTIVO;
}