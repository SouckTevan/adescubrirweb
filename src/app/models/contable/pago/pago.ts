import { Banco } from '../../admin/banco/banco';
import { MedioPago } from '../medio-pago/medio-pago';

export class Pago {
    id: number = 0;
    idDocumentoContable: number = 0;
    // tipoPago: string = '';
    estado: string = '';
    medioPago: MedioPago = new MedioPago();
    idMedioPago: number = 0;
    banco: Banco = new Banco;
    idBanco: number = 0;
    cheque: string = '';
    cuenta: string = '';
    franquicia: string = '';
    nroTransaccion: string = '';
    valor: number = 0;
    impuestos: number = 0;
    total: number = 0;
    texto1: string = '';
    texto2: string = '';
    observaciones: string = '';
    soporte: string = '';
}