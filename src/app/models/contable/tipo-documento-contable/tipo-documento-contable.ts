import { Parametro } from '../../admin/parametro/parametro';
import { RangoNumeracion } from '../../admin/rango-numeracion/rango-numeracion';

export class TipoDocumentoContable {
    id: number = null;
    nombre = '';
    estado: string = Parametro.ESTADO_ACTIVO;
    rangoNumeracion: RangoNumeracion;
    rango_id: number = 0;
    especial: string;
    tipoCuenta: string;
    numeracionManual: boolean = false;
    numeroObligatorio: boolean = false;
}
