import { Concepto } from '../../admin/concepto/concepto';

export class ConsumoEvento {
    concepto: Concepto = new Concepto();
    fecha: Date = new Date();
    cantidad: number = 0;
    valor: number = 0;
}
