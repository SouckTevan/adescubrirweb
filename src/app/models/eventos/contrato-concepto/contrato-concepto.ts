import { Concepto } from '../../admin/concepto/concepto';

export class ContratoConcepto {
    contrato_id: number = 0;
    concepto_id: number = 0;
    concepto: Concepto = new Concepto();
}
