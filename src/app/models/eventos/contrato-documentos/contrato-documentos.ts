export class ContratoDocumentos {
    contrato_id: number = 0;
    documento_id: number = 0;
    fechahora: Date = null;
    observacion: string = '';
    rutaarchivo: string = '';
    nombrearchivo: string = '';
    usuario_id: number = 0;
}
