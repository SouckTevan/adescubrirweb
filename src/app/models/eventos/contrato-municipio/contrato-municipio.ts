import { Municipio } from '../../admin/municipio/municipio';

export class ContratoMunicipio {
    contrato_id: number = 0;
    municipo_id: number = 0;
    municipio: Municipio = new Municipio();
}