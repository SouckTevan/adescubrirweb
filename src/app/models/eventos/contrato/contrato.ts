import { Parametro } from '../../admin/parametro/parametro';
import { Persona } from '../../admin/persona/persona';
import { RegistroObservacion } from '../../admin/registro-observacion/registro-observacion';
import { Municipio } from '../../admin/municipio/municipio';
import { TarifaContrato } from '../tarifa-contrato/tarifa-contrato';
import { Concepto } from '../../admin/concepto/concepto';
import { RegistroDocumento } from '../../admin/registro-documento/registro-documento';

export class Contrato {
    id: number = 0;
    estado: string = Parametro.ESTADO_ACTIVO;
    nombre: string = '';
    descripcion: string = '';
    numero: string = '';
    fechaFirma: any = null;
    contratante_id: any = 0;
    rutaContrato: string = '';
    valorTotal: number = 0;
    fechaInicial: any = null;
    fechaFin: any = null;
    poSobreProveedor: number = 0;
    nitCompania: string = '';
    nombreCompania: string = '';
    direccionCompania: string = '';
    ciudadCompania: string = '';
    cuentaCompania: string = '';
    poIntermediacion: number = 0;
    validarPresupuestoEvento: boolean = false;
    poDescuento: number = 0;
    observacionesFactura: string = '';

    contratante: Persona = new Persona();

    observaciones: RegistroObservacion[] = new Array<RegistroObservacion>();
    municipios: Municipio[] = new Array<Municipio>();
    tarifas: TarifaContrato[] = new Array<TarifaContrato>();
    conceptos: Concepto[] = new Array<Concepto>();
    documentos: RegistroDocumento[] = new Array<RegistroDocumento>();
    parametros: string[] = new Array<string>();
}