import { Proveedor } from '../../admin/proveedor/proveedor';
import { Sucursal } from '../../admin/sucursal/sucursal';
import { TipoTarifa } from '../../admin/tipo-tarifa/tipo-tarifa';
import { DocumentoContable } from '../../contable/documento-contable/documento-contable';
import { Usuario } from '../../usuario/usuario';
import { ConsumoEvento } from '../consumo-evento/consumo-evento';
import { TarifaCotizacion } from '../tarifa-cotizacion/tarifa-cotizacion';

export class CotizacionEvento {
    id: number = 0;
    proveedor: Proveedor = null;
    idProveedor:any = '0';
    sucursal: Sucursal = null;
    idSucursal:any = '0';
    nombreSucursal = '';
    tipoTarifa: TipoTarifa = null;
    idTipoTarifa:any = '0';
    porcentajeIncremento: number = 0;
    tarifas: TarifaCotizacion[] = [];
    consumos = {};
    observaciones: string = '';
    estado: string = 'INICIAL';
    creador: Usuario = new Usuario();
    idCreador = 0;
    factura: DocumentoContable = new DocumentoContable();
    idFactura = -1;
    fechaAprobacion: Date = null;
    porcentajeComision: number = 0;
    totalCotizacion: number = 0;
    porcentajeDescuento: number = 0;
    tipo: string = '0';
    aprobado: boolean = false;
    fechaFacturaProveedor: Date = null;
}
