import { Documento } from '../../admin/documento/documento';
import { Usuario } from '../../usuario/usuario';

export class EventoDocumentos {
    idEvento: number;
    idDocumento: number = 0;
    fechaHora: any;
    documento: Documento = new Documento();
    idUsuario: number;
    usuario: Usuario = new Usuario();
    fileType: string;
    rutaArchivo: string;
    nombreArchivo: string;
    observacion: string;
}