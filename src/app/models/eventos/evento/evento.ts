import { Municipio } from '../../admin/municipio/municipio';
import { EventoDocumentos } from '../evento-documento/evento-documentos';
import { TarifaEvento } from '../tarifa-evento/tarifa-evento';
import { ConsumoEvento } from '../consumo-evento/consumo-evento';
import { TarifaContrato } from '../tarifa-contrato/tarifa-contrato';
import { DocumentoContable } from '../../contable/documento-contable/documento-contable';
import { Anticipo } from '../../contable/anticipo/anticipo';
import { Bitacora } from '../bitacora/bitacora';
import { EventoObservaciones } from '../evento-observaciones/evento-observaciones';
import { ObservacionFacturacion } from '../observacion-facturacion/observacion-facturacion';
import { Contrato } from '../contrato/contrato';
import { CotizacionEvento } from '../cotizacion-evento/cotizacion-evento';
import { LegalizacionGasto } from '../legalizacion-gasto/legalizacion-gasto';

export class Evento {
    id: number = 0;
    idContrato: number = 0;
    contrato: Contrato = null;
    idTipoEvento: number = 0;
    nombre: string;
    sede: Municipio = new Municipio();
    idSede: any = 0;
    fechaInicioEvento: any;
    horaInicioEvento: any = '0';
    fechaFinEvento: any;
    horaFinEvento: any = '0';
    idResponsable: number;
    idCoordinador: number; // -------
    solicitante: string;
    numeroAsistentes: number;
    desdeDiaAnterior: boolean;
    hastaDiaSiguiente: boolean;
    idTipoTarifa: number = 0;
    formatoEscarapela: string;
    tipoRegistroIngreso: string;
    orden: string;
    fechaSolicitud: any;
    texto1: string;
    texto2: string;
    texto3: any = false;
    programa: string;
    cdp: string;
    centroCostos: string;
    rubroPresupuestal: string;
    valorPresupuesto: number;
    contactoCliente: string;
    apoyoLogistico: string;
    idResponsableActual: number; // ---------
    lugarDelEvento: string;
    observacionesCarpeta: string;
    idEstado: any = '1'; // --> Tabla: Estado
    idFactura: number = 16614; // --> Tabla: Documento Contable
    idCreador: number = 1; // --> Tabla: Usuario // ------
    idLugar: number = 4337; // --> Tabla: Cotizacion evento
    lugar: CotizacionEvento = new CotizacionEvento();
    idSucursal: number = null;
    fechaCreacion: Date = new Date();
    alertas: string = '';
    conAlerta: boolean = false;
    tipoMunicipio: string = '';
    totalContratado: number = 0;
    totalEjecutado: number = 0;
    totalFacturado: number = 0;
    totalProveedores: number = 0;
    totalGastos: number = 0;
    resumenConsumos: string = '';
    observacionesConsumos: string = '';

    municipio: Municipio;

    //-------- Campos Nuevos (nc: Nacional)
    terPirmerNomber: string;
    terSegundoNomber: string;
    terPirmerApellido: string;
    terSegundoApellido: string;
    terTipoIdentificacion: string = '0';
    terNumeroIdentificacion: string;
    terCelularContacto: string;
    terCorreoInstitucional: string;

    nacPirmerNomber: string;
    nacSegundoNomber: string;
    nacPirmerApellido: string;
    nacSegundoApellido: string;
    nacTipoIdentificacion: string = '';
    nacNumeroIdentificacion: string;
    nacCelularContacto: string;
    nacCorreoInstitucional: string;

    tipoComunidad: string;

    vereda: number = 0;
    resguardo: number = 0;
    consejoCom: number = 0;
    kumpania: number = 0;
    etnicaCom: string;

    marcoPolitico: string;
    observacionesMP: string;
    descripcionEvento: string;
    soporteAnticipo: string;
    soporteExcedentes: string;

    documentos = new Array<EventoDocumentos>();
    eventoConsumos: any = {};
    eventoEjecucion: any = {};

    consumos: any = [];
    rangoFechas = [];
    tarifasDelEvento: TarifaEvento[] = [];
    tarifasContrato = new Array<TarifaContrato>();
    cotizaciones = [];
    ejecucion = new Array<ConsumoEvento>();
    anticipos = new Array<Anticipo>();
    bitacora: Bitacora[] = [];
    eventoObservacion: EventoObservaciones[] = [];
    facturacion: ObservacionFacturacion[] = [];
    gastos: LegalizacionGasto[] = [];
    viaticos: LegalizacionGasto[] = [];

    factura: DocumentoContable = new DocumentoContable();
}