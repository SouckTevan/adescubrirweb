import { Persona } from '../../admin/persona/persona';
import { Usuario } from '../../usuario/usuario';

export class LegalizacionGasto {
    id: number = 0;
    idEvento: number = 0;
    tipo: string = '0';
    persona: Persona = new Persona();
    valor: number = 0;
    valorEvento: number = 0;
    concepto: string = '';
    identificacion: string = '';
    nombres: string = '';
    apellidos: string = '';
    usuario: Usuario = new Usuario();
    idUsuario: number = 0;
    valorTransporte: number = 0;
    valorAlojamiento: number = 0;
    valorAlimentacion: number = 0;
    terrestre: boolean = false;
    fluvial: boolean = false;
    ruta: string = '';
    fecha: any = new Date();
    fechaRegistro: Date = new Date();
    documentoId: number = 0;
    numeroDocEquivalente: string = '';
    direccion: string = '';
    telefono: string = '';
    retencionRenta: number = 0;
    retencionIVA: number = 0;
    nombreUsuario = '';
    totalGastosEvento: number;
}
