import { Usuario } from '../../usuario/usuario';

export class ObservacionFacturacion {
    evento_id: number = 0;
    fechahora: Date = new Date();
    observacion: string = '';
    usuario: Usuario;
    usuario_id: number = 0;
}
