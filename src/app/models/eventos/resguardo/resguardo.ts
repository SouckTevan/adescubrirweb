import { Municipio } from '../../admin/municipio/municipio';

export class Resguardo {
    id: number = 0;
    nombre: string = '';
    estado: string = '';
    dane: string = '';
    idMunicipio: number = 0;
    municipio: Municipio = new Municipio();
    nombreMunicipio = '';
}