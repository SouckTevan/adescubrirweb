export interface SearchI {
    // ------- Eventos
    idEvento?: number;
    idContrato?: number;
    fechaInicio?: Date;
    fechaFin?: Date;
    idMunicipio?: string;
    idEstado?: string;
}
