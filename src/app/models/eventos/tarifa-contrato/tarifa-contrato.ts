import { Municipio } from '../../admin/municipio/municipio';
import { Concepto } from '../../admin/concepto/concepto';
import { TipoTarifa } from '../../admin/tipo-tarifa/tipo-tarifa';

export class TarifaContrato {
    contrato_id: number = 0
    municipio_id: any = 0;
    tipotarifa_id: any = 0;
    concepto_id: any = 0;
    valor: ConstrainDouble = 0;
    ano: number = (new Date()).getFullYear();
    
    municipio: Municipio = new Municipio();
    tipoTarifa: TipoTarifa = new TipoTarifa();
    concepto: Concepto = new Concepto();
}