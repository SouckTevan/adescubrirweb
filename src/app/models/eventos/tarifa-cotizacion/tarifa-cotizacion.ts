import { Concepto } from '../../admin/concepto/concepto';

export class TarifaCotizacion {
    concepto:Concepto = new Concepto();
    consumos = {};
    idCotizacion: number = 0;
    idConcepto: number = 0;
    valor: number = 0;
    totalTarifa: number = 0;
    totalCantidad: number = 0;
}
