import { Concepto } from '../../admin/concepto/concepto';

export class TarifaEvento {
    concepto: Concepto = new Concepto();
    valor: number = 0;
    evento_id: number = 0;
    concepto_id: number = 0;
    cantidad: number = 0;
    conDescuento: boolean = false;
}