import { Aerolinea } from "../../admin/aerolinea/aerolinea";
import { Concepto } from "../../admin/concepto/concepto";
import { Persona } from "../../admin/persona/persona";

export class TiqueteEvento {
    idEvento: number = 0;
    numero: string = '';
    concepto: Concepto = new Concepto();
    ruta: string = '';
    pasajero: Persona = new Persona();
    idPersona: number = -1;
    dniPasajero: string = '';
    nombrePasajero: string = '';
    aerolinea: Aerolinea = null;
    idAerolinea: number = 0;
    total: number = null;
    impuestos: number = null;
    valor: number = null;
    tasas: number = null;
    administrativo: number = null;
    valorEvento: number = null;
    fee: number = null;
    modificacion: number = null;
    motivoModificacion: string = '';
    factura: string = '';
    localizador: string = '';
    estado: string = 'SOLICITADO';
    fechaSalida: any = null;
    horaSalida: Date = null;
    horaRegreso: Date = null;
    fechaRegreso: any = null;
    itinerarioCompleto: string = '';
    observaciones: string = '';
    valorRevisado: number = null;
    revisado: boolean = false;
}