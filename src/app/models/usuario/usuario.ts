import { SocialUser } from 'angularx-social-login';
import { Persona } from '../admin/persona/persona';
import { Rol } from '../admin/security/rol/rol';
import { UsuarioOpcion } from '../admin/security/usuario-opcion/usuario-opcion';

export class Usuario extends Persona {
    tipoUsuario: string;
    roles = new Array<Rol>();
    opciones = new Array<UsuarioOpcion>();
    contratos = new Array<number>();
    estados = new Array<number>();
    socialUser: SocialUser = null;
}