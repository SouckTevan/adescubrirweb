import { Rol } from '../admin/security/rol/rol';
import { Opcion } from '../admin/security/opcion/opcion';

export class UsuarioSesion {
    id: number;
    nombre: string;
    nombres: string;
    apellidos: string;
    identificacion:string;
    email: string;
    usuario: string;
    photoUrl: string;
    roles = new Array<Rol>();
    opciones = new Array<Opcion>();
    isGoogle: boolean;
}