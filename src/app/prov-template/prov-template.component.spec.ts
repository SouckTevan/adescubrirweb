import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvTemplateComponent } from './prov-template.component';

describe('ProvTemplateComponent', () => {
  let component: ProvTemplateComponent;
  let fixture: ComponentFixture<ProvTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
