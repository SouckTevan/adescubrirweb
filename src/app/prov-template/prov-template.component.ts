import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { UtilityService } from '../utility.service';
import { Proveedor } from '../models/admin/proveedor/proveedor';
import { Contacto } from '../models/admin/contacto/contacto';
import { CuentaBancaria } from '../models/admin/cuenta-bancaria/cuenta-bancaria';
import { Sucursal } from '../models/admin/sucursal/sucursal';
import { Tarifa } from '../models/admin/tarifa/tarifa';
import { RegistroDocumento } from '../models/admin/registro-documento/registro-documento';
import { TipoConcepto } from '../models/admin/tipo-concepto/tipo-concepto';
import { ProveedorTipo } from '../models/admin/proveedorTipo/proveedor-tipo';
declare var $: any;

@Component({
  selector: 'proveedor-template',
  templateUrl: './prov-template.component.html',
  styleUrls: ['./prov-template.component.scss']
})
export class ProvTemplateComponent implements OnInit {

  // --------- Instancia de objetos
  proveedor: Proveedor = new Proveedor();
  contacto: Contacto = new Contacto();
  cuentaBancaria: CuentaBancaria = new CuentaBancaria();
  sucursal: Sucursal = new Sucursal();
  tarifa: Tarifa = new Tarifa();
  regDocumento: RegistroDocumento = new RegistroDocumento();
  departamentoDg;
  departamento;
  isNew = true;
  isEdit = {
    contact: false,
    cuentaBancaria: false,
    sucursal: false,
    tarifa: false
  };

  // ------ File of Documents
  files: File[] = [];
  filesTemp: File[] = [];

  // -------- Variables temporales para llenar datos
  departamentoDtsGen: number = 0;
  departamentoSucursal: number = 0;
  objTempTipoConcepto: TipoConcepto;
  idGlobal: number = 0;

  typeProvSelected = '0';

  // ------ reset campos
  departamentoContacto: number = 0;
  tipoConceptoTarifas: number = 0;
  sucursalEnTarifa: any = 0;

  // -------- arrays para llenar las tablas
  arrayContactos = new MatTableDataSource<Contacto>();
  arrayCuentaBancaria = new MatTableDataSource<CuentaBancaria>();
  arraySucursal = new MatTableDataSource<Sucursal>();
  arrayTypesProv = new MatTableDataSource<any>();

  arrayTarifa = new MatTableDataSource<Tarifa>();
  arrayDocumentos = new MatTableDataSource<RegistroDocumento>();
  arrayCuentaEnSucursal: any[] = new Array<any>();
  arraySucursalEnTarifa: any[] = new Array<any>();

  dataEstado: any = [{option: 'Seleccione una opcion...', val: ''},{option: 'Activo', val: 'A'},{option: 'Inactivo', val: 'I'},{option: 'Para Borrar', val: 'B'}];
  dataTipoCuenta: any = [{option: 'Seleccione una opcion...', val: ''},{option: 'AH', val: 'AH'},{option: 'CC', val: 'CC'}];

  // ----------- Columnas ---------
  showColsContacto: string[] = ['id', 'tipoContacto', 'municipio', 'nombre', 'dirección', 'telefono','fax', 'celular', 'email', 'accion'];
  showColsProveedor: string[] = ['banco', 'tipoCuenta', 'cuenta', 'nombreTitular', 'dniTitular','accion'];
  showColsSucursal: string[] = ['municipio','sucursal','direccion','telefono','email','cuentaBancaria', 'accion'];
  showColsTarifa: string[] = ['id','municipio','nombre', 'tipoTarifa', 'concepto', 'valor', 'accion'];
  showColsDocumento: string[] = ['id','fecha','usuario', 'tipoDocumento', 'archivo', 'accion'];
  showColsTypeProv: string[] = ['item','accion'];

  @Output() response: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('municipioGenComponent', {static: true}) municipioGenComponent: SelectGeneralComponent;
  @ViewChild('municipioComponent', {static: true}) municipioComponent: SelectGeneralComponent;
  @ViewChild('municipioSucursal', {static: true}) municipioSucursal: SelectGeneralComponent;
  @ViewChild('tipoConceptoComponent', {static: true}) tipoConceptoComponent: SelectGeneralComponent;

  ruta = 'administracion/getProveedor/';

  constructor(public apiServices: AdescubrirService, public util: UtilityService) { }

  ngOnInit() {
    // ----------- Sort
    this.arrayContactos.sort = this.sort;
    this.arrayTypesProv.sort = this.sort;

    this.reset('arrays');
    if(this.proveedor.id == 0)
      $('#collapseOne').collapse("show");
  }

  changeSelectGeneral(id: number, table: string) {
    switch(table) {
      case 'datosGenerales':
        this.departamentoDtsGen = id;
        this.municipioGenComponent.onChangePadre(id);
        break;

      case 'contacto':
        this.departamentoContacto = id;
        this.municipioComponent.onChangePadre(id);
        break;

      case 'sucursal':
        this.departamentoSucursal = id;
        this.municipioSucursal.onChangePadre(id);
        break;

      case 'tarifa':
        this.tipoConceptoTarifas = id;
        this.tipoConceptoComponent.onChangePadre(id);
        break;
    }
  }

  async addType(obj: any) {
    if (obj != undefined) {
      const existe = await this.arrayTypesProv.data.find((item: any) => item.id == obj.id);
      if (existe == undefined) {
        this.arrayTypesProv.data = [...this.arrayTypesProv.data, obj];
      }
      this.typeProvSelected = '0';
    }
  }

  add(tabla: string) {
    switch (tabla) {
      case 'contacto':
        if (this.validate(tabla)) {
          this.isEdit.contact = false;
          this.contacto.id = this.idGlobal;
          this.arrayContactos.data = [...this.arrayContactos.data, this.contacto];
          this.reset('contacto');
        }
        break;

      case 'cuentaBancaria':
        if (this.validate(tabla)) {
          this.isEdit.cuentaBancaria = false;
          this.cuentaBancaria.id = this.idGlobal;
          this.arrayCuentaBancaria.data = [...this.arrayCuentaBancaria.data, this.cuentaBancaria];
          this.arrayCuentaEnSucursal.push({option: this.cuentaBancaria.cuenta, val: this.cuentaBancaria.banco_id });
          this.reset('cuentaBancaria');
        }
        break;

      case 'sucursal':
        if (this.validate(tabla)) {
          this.isEdit.sucursal = false;
          this.sucursal.id = this.idGlobal;
          this.sucursal.cuentaBancaria = this.arrayCuentaBancaria.data.filter((ctB) => ctB.banco_id == this.sucursal.banco_id)[0];
          this.arraySucursal.data = [...this.arraySucursal.data, this.sucursal];
          this.arraySucursalEnTarifa.push({option: this.sucursal.nombre, val: this.sucursal.municipio_id+'-'+this.sucursal.nombre});
          this.reset('sucursal');
        }
        break;

      case 'tarifa':
        if (this.validate(tabla)) {
          this.isEdit.tarifa = false;
          this.tarifa.proveedor_id = this.idGlobal;
          this.tarifa.municipio_id = this.tarifa.sucursal.municipio_id;
          this.tarifa.nombresucursal = this.tarifa.sucursal.nombre;
          this.tarifa.concepto.tipoConcepto = this.objTempTipoConcepto;
          this.arrayTarifa.data = [...this.arrayTarifa.data, this.tarifa];
          this.reset('tarifa');
        }
        break;

      case 'documento':
        if (this.validate(tabla)) {
          this.regDocumento.usuario_id = this.apiServices._usuarioSesion.id;
          this.regDocumento.usuario.nombres = this.apiServices._usuarioSesion.nombres;
          this.regDocumento.fechaHora = new Date();
          this.regDocumento.fileType = this.filesTemp[0].type;
          this.regDocumento.nombreArchivo = this.filesTemp[0].name;
          this.files.push(this.filesTemp[0]);
          this.arrayDocumentos.data = [...this.arrayDocumentos.data, this.regDocumento];
          this.regDocumento = new RegistroDocumento();
          this.filesTemp = [];
        }
        break;

      default:
        this.changeSelectGeneral(this.departamentoDtsGen, 'sucursal');
        this.sucursal.id           = this.idGlobal;
        this.sucursal.nombre       = 'Principal';
        this.departamentoSucursal  = this.departamentoDtsGen;
        this.municipioSucursal.val = this.proveedor.municipio_id;
        this.sucursal.telefono     = this.proveedor.telefono;
        this.sucursal.email        = this.proveedor.email;
        this.sucursal.municipio    = this.proveedor.municipio;
        this.sucursal.municipio_id = this.proveedor.municipio_id;
        this.sucursal.direccion    = this.proveedor.direccion;
        this.sucursal.fax          = this.proveedor.fax;
        break;
    }
  }

  edit(table: string, obj: any) {
    switch(table) {
      case 'contacto':
        this.isEdit.contact = true;
        this.contacto = obj;
        this.changeSelectGeneral(this.contacto.municipio.departamento_id, table);
        break;

      case 'cuentaBancaria':
        this.isEdit.cuentaBancaria = true;
        this.cuentaBancaria = obj;
        this.cuentaBancaria.banco_id = obj.banco_id;
        break;

      case 'sucursal':
        this.isEdit.sucursal = true;
        this.sucursal = obj;
        this.departamentoSucursal = this.sucursal.municipio.departamento_id;
        this.changeSelectGeneral(this.sucursal.municipio.departamento_id, table);
        break;

      case 'tarifa':
        this.isEdit.tarifa = true;
        this.tarifa = obj;
        this.sucursalEnTarifa = this.tarifa.sucursal.municipio_id+'-'+this.tarifa.sucursal.nombre;
        this.changeSelectGeneral(this.tarifa.concepto.tipoConcepto_id, table);
        break;
    }
    this.delete(table, obj);
  }

  delete(tabla: string, obj: any) {
    switch (tabla) {
      case 'contacto':
        this.arrayContactos.data = this.arrayContactos.data.filter(ob => ob !== obj);
        break;

      case 'cuentaBancaria':
        this.arrayCuentaBancaria.data = this.arrayCuentaBancaria.data.filter(ob => ob !== obj);
        this.arrayCuentaEnSucursal = this.arrayCuentaEnSucursal.filter(ob => ob !== obj);
        break;

      case 'sucursal':
        this.arraySucursal.data = this.arraySucursal.data.filter(ob => ob !== obj);
        this.arraySucursalEnTarifa = this.arraySucursalEnTarifa.filter(ob => ob !== obj);
      break;

      case 'tarifa':
        this.arrayTarifa.data = this.arrayTarifa.data.filter(ob => ob !== obj);
        break;

      case 'documento':
        this.arrayDocumentos.data = this.arrayDocumentos.data.filter(ob => ob !== obj);
        break;

      case 'provTipo':
        this.arrayTypesProv.data = this.arrayTypesProv.data.filter(ob => ob !== obj);
        break;
    }
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (!this.isEdit.contact && !this.isEdit.cuentaBancaria && !this.isEdit.sucursal && !this.isEdit.tarifa) {
      this.proveedor.id = this.idGlobal;
      this.proveedor.contactos = this.arrayContactos.data;
      this.proveedor.cuentasBancarias = this.arrayCuentaBancaria.data;
      this.proveedor.sucursales = this.arraySucursal.data;
      this.proveedor.tarifas = this.arrayTarifa.data;
      this.proveedor.documentos = this.arrayDocumentos.data;
      this.proveedor.tipos = [];
      let nombreTipos = '';

      const formData = new FormData();

      for (const fl of this.files) {
        formData.append('file[]', fl, fl.name);
      }

      if (this.proveedor.contactos.length > 0) {
        for (const con of this.proveedor.contactos) {
          delete con.municipio['codigo'];
          delete con['key'];
        }
      }
      if (this.proveedor.cuentasBancarias.length > 0) {
        for (const cB of this.proveedor.cuentasBancarias) {
          delete cB['key'];
        }
      }
      if (this.proveedor.sucursales.length > 0) {
        for (const suc of this.proveedor.sucursales) {
          if (suc != null)
            delete suc.municipio['codigo'];
          delete suc['key'];
          if (suc.cuentaBancaria != null)
            delete suc.cuentaBancaria['key'];
        }
      }
      if (this.proveedor.documentos.length > 0) {
        for (const doc of this.proveedor.documentos) {
          if (doc.usuario != null) {
            delete doc.usuario.socialUser;
            delete doc.usuario.municipio['codigo'];
          }
        }
      }
      if (this.proveedor.municipio != null) {
        delete this.proveedor.municipio['codigo'];
      }

      if (this.proveedor.tarifas.length > 0) {
        for (const itm of this.proveedor.tarifas) {
          itm.valor = this.util.removeMiles(itm.valor);
          delete itm.concepto['codigo'];
          delete itm['key'];
          delete itm.sucursal['key'];
          if (itm.sucursal.cuentaBancaria != null)
            delete itm.sucursal.cuentaBancaria['key'];

          if (itm.sucursal.municipio != null)
            delete itm.sucursal.municipio['codigo'];
        }
      }

      if (this.arrayTypesProv.data.length > 0) {
        for (const itm of this.arrayTypesProv.data) {
          let typePrv: ProveedorTipo = new ProveedorTipo();
          typePrv.idProveedor = this.proveedor.id;
          typePrv.idTipo = itm.id;
          nombreTipos += itm.nombre + ',';
          this.proveedor.tipos.push(typePrv);
        }
      }

      let routeSave = 'administracion/saveProveedor';

      if (this.ruta == 'administracion/getPersona/') {
        routeSave = 'administracion/savePersona';
        delete this.proveedor.tarifas;
        delete this.proveedor.tipoProveedor_id;
        delete this.proveedor.tipos;
        delete this.proveedor['tipoUsuario'];
        delete this.proveedor['socialUser'];
        delete this.proveedor['foto'];
        delete this.proveedor['roles'];
        delete this.proveedor['opciones'];
        delete this.proveedor['contratos'];
        delete this.proveedor['estados'];
      }

      formData.append('persona', JSON.stringify(this.proveedor));

      if (this.validate('all')) {
        this.apiServices.postWithFile(routeSave, formData)
        .subscribe(
          (res) => {
            $('.loading').addClass('hide-loading');
            this.util.notification('success','','Proveedor guardado!');

            this.response.emit({provedor: res, tipos: nombreTipos});
            $('#modalProvider').modal('hide');
            if (this.isNew) {
              this.reset('datosGenerales');
              $('#modalProvider').modal('hide');
            }
          },
          err => this.util.messageError(err));
      }else {
        $('.loading').addClass('hide-loading');
      }
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('info','Validación','No se puede guardar porque estas editando');
    }
  }

  validate(tabla: string) {
    let bandera: boolean = false;

    switch(tabla) {
      case 'contacto':
        let tipoContacto = this.contacto.tipoContacto_id != 0;
        let municipioC   = this.contacto.municipio_id != 0;
        bandera = tipoContacto && municipioC;
        break;

      case 'cuentaBancaria':
        let banco            = this.cuentaBancaria.banco_id != 0;
        let cuenta           = this.cuentaBancaria.cuenta != '';
        let tipoCuenta       = this.cuentaBancaria.tipoCuenta != '';
        bandera = banco && cuenta && tipoCuenta;
        break;

      case 'sucursal':
        let municipioS = this.sucursal.municipio_id != 0;
        let nombreS    = this.sucursal.nombre != '';
        let cuentaBan  = this.sucursal.banco_id != 0;
        bandera = municipioS && nombreS && cuentaBan;
        break;

      case 'tarifa':
        let sucursalTarifa   = this.tarifa.sucursal != undefined;
        let valorTarifa = true;
        if (this.tarifa.valor > 0) {
          valorTarifa = this.util.removeMiles(this.tarifa.valor) != 0;
        }
        bandera = sucursalTarifa && valorTarifa;
        break;

      case 'documento':
        let tipoDoc = this.regDocumento.documento_id != 0;
        let fl      = this.filesTemp.length > 0;

        bandera = tipoDoc && fl;
        break;

      default:
        let municipio      = this.proveedor.municipio_id != 0;
        let tipoProveedor  = this.proveedor.tipoProveedor_id != 0;
        let estado         = this.proveedor.estado != '';

        // let contacto       = this.arrayContactos.data.length > 0;
        // let cuentaBancaria = this.arrayCuentaBancaria.data.length > 0;
        // let sucursal       = this.arraySucursal.data.length > 0;
        // let tarifa         = this.arrayTarifa.data.length > 0;
        // bandera = municipio && tipoProveedor && estado && contacto && cuentaBancaria && sucursal && tarifa;
        bandera = municipio && estado && tipoProveedor;
        break;
    }

    if (!bandera) {
      this.util.notification('warning','','Los campos marcados con * son requeridos');
    }

    return bandera;
  }

  reset(table: string) {
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    switch(table) {
      case 'datosGenerales':
        this.departamentoDtsGen = 0;
        this.proveedor = new Proveedor();
        break;

      case 'contacto':
        this.departamentoContacto = 0;
        this.contacto = new Contacto();
        break;

      case 'cuentaBancaria':
        this.cuentaBancaria = new CuentaBancaria();
        break;

      case 'sucursal':
        this.departamentoSucursal = 0;
        this.sucursal = new Sucursal();
        break;

      case 'tarifa':
        this.tipoConceptoTarifas = 0;
        this.sucursalEnTarifa = 0;
        this.tarifa = new Tarifa();
        break;

      case 'documento':
        this.files = [];
        this.filesTemp = [];
        this.regDocumento = new RegistroDocumento();
        break;

      case 'arrays':
        this.arrayContactos = new MatTableDataSource<Contacto>();
        this.arrayCuentaBancaria = new MatTableDataSource<CuentaBancaria>();
        this.arraySucursal = new MatTableDataSource<Sucursal>();
        this.arrayTarifa = new MatTableDataSource<Tarifa>();
        this.arrayDocumentos = new MatTableDataSource<RegistroDocumento>();
        this.arrayCuentaEnSucursal = new Array<any>();
        this.arraySucursalEnTarifa = new Array<any>();

        this.arrayCuentaEnSucursal.push({option: 'Seleccione una opcion...', val: 0});
        this.arraySucursalEnTarifa.push({option: 'Seleccione una opcion...', val: 0});
        $('.nav-tabs a:first-child').tab('show');
        // $('#modalProvider').find('.nav-tabs').find('a').removeClass('active').prop('aria-selected', false).parent().find('a:first-child').addClass('active').prop('aria-selected', true).parents().find('.tab-content .tab-pane').removeClass('show active').parent().find('.tab-pane:first-child').addClass('show active');
        break;

      default:
        this.reset('datosGenerales');
        this.reset('contacto');
        this.reset('cuentaBancaria');
        this.reset('sucursal');
        this.reset('tarifa');
        this.reset('documento');
        this.reset('arrays');
        this.idGlobal = 0;
        this.isNew = true;
        this.arrayTypesProv.data = [];
        break;
    }
  }

  assignSucursal(event: any) {
    const val = event.target.value;
    let idMunicipio: number = Number(val.substr(0, val.indexOf('-')));
    let nombreSucursal = val.substr(val.indexOf('-')+1, val.length-1);
    const rs = this.arraySucursal.data.filter((sucursal: Sucursal) => sucursal.municipio_id == idMunicipio && sucursal.nombre == nombreSucursal)[0];
    this.tarifa.sucursal = rs;
  }

  addProveedor() {
    this.reset('');
    $('#modalProvider').modal('show');
  }

  getProveedor(id: number, showModal: boolean) {
    this.isNew = false;
    this.reset('');
    $('.loading').removeClass('hide-loading');
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    this.proveedor = new Proveedor();
    console.log(this.ruta+id);

    this.apiServices.get(this.ruta+id)
      .subscribe((resp) => {
        console.log(resp);

        if (resp != null) {
          this.proveedor = resp;
          this.idGlobal = this.proveedor.id;

          if (showModal && this.proveedor.municipio != null)
            this.changeSelectGeneral(this.proveedor.municipio.departamento_id, 'datosGenerales');

          if (this.proveedor.contactos != null) {
            for (const contact of this.proveedor.contactos) {
              this.arrayContactos.data = [...this.arrayContactos.data, contact];
            }
          }

          if (this.proveedor.tipos != null) {
            for (const type of this.proveedor.tipos) {
              this.arrayTypesProv.data = [...this.arrayTypesProv.data, type.objTipo];
            }
          }

          if (this.proveedor.cuentasBancarias != null) {
            for (const cuentaBan of this.proveedor.cuentasBancarias) {
              this.arrayCuentaBancaria.data = [...this.arrayCuentaBancaria.data, cuentaBan];
              this.arrayCuentaEnSucursal.push({option: cuentaBan.cuenta, val: cuentaBan.banco_id });
            }
          }

          if (this.proveedor.sucursales != null) {
            for (const sucursal of this.proveedor.sucursales) {
              this.arraySucursal.data = [...this.arraySucursal.data, sucursal];
              this.arraySucursalEnTarifa.push({option: sucursal.nombre, val: sucursal.municipio_id+'-'+sucursal.nombre});
            }
          }

          if (this.proveedor.tarifas != null) {
            for (const tarifa of this.proveedor.tarifas) {
              tarifa.valor = this.util.miles(tarifa.valor);
              this.arrayTarifa.data = [...this.arrayTarifa.data, tarifa];
            }
          }

          if (this.proveedor.documentos != null) {
            for (const doc of this.proveedor.documentos) {
              this.arrayDocumentos.data = [...this.arrayDocumentos.data, doc];
            }
          }

          if (showModal)
            $('#modalProvider').modal('show');

          $('.loading').addClass('hide-loading');
          return this.proveedor;
        }else {
          if (this.ruta == 'administracion/getPersona/') {
            this.ruta = 'administracion/getProveedor/';
            $('.loading').addClass('hide-loading');
            this.util.notification('info','Ups!','Usuario no encontrado');
            return null;
          }else {
            this.ruta = 'administracion/getPersona/';
            this.getProveedor(id, showModal);
          }
        }
      },
      (err:HttpErrorResponse) => this.util.messageError(err));
  }

  onSelect(event: any) {
    if (this.filesTemp.length < 1) {
      let fileName = event.addedFiles[0].name.replace(/[^a-zA-Z 0-9.]+/g,'');
      fileName = fileName.replace(/\s/g, '');
      Object.defineProperty(event.addedFiles[0], 'name', {
        writable: true,
        value: fileName
      });
      this.filesTemp.push(...event.addedFiles);
      setTimeout(() => {
        var chi = $('.document-dropzone ngx-dropzone-preview:last-child');
        var tp = chi.find('ngx-dropzone-label').attr('tp');
        var imgLoad = 'icon_image-min.png';
        if (tp === 'application/pdf') {
          imgLoad = 'icono_pdf-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
          imgLoad = 'icono_excel.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
          imgLoad = 'icon_word-min.png';
        } else if (tp === 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
          imgLoad = 'icono_powerpoint.png';
        } else if (tp === 'text/plain') {
          imgLoad = 'icono_txt-min.png';
        }
        chi.css({'background':'url(./assets/img/'+imgLoad+') no-repeat','background-size':'contain'});
      },1000);
    }
  }

  onRemove(event: any) {
    this.filesTemp.splice(this.files.indexOf(event), 1);
  }

  openFile(id: number, fileName: string, type: string, isDownload: boolean) {
    this.apiServices.downloadFileById('administracion/getFileSystemById/'+id)
    .subscribe((res: Blob) => {
      if (res != null) {
        this.util.saveAsFile(res,fileName, type,'', isDownload);
      }
    });
  }

}
