import { Component, OnInit, ViewChild } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UtilityService } from '../utility.service';
import { ProvTemplateComponent } from '../prov-template/prov-template.component';
declare var $: any;

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.scss']
})
export class ProveedoresComponent implements OnInit {

  arrayProveedores = new MatTableDataSource<any>();

  // ----------- Columnas ---------
  showColsProveedores: string[] = ['id', 'identificacion', 'nombre', 'celular', 'telefono', 'razonSocial', 'email', 'municipio', 'tipoProveedor', 'accion'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(ProvTemplateComponent, {static: true}) provModal: ProvTemplateComponent;

  constructor(public apiServices: AdescubrirService, public util: UtilityService) { }

  ngOnInit() {
    this.getProveedores()

    // ----------- Sort
    // this.arrayCuentaBancaria.sort = this.sort;
    this.arrayProveedores.sort = this.sort;

    // ---------- Paginador
    this.arrayProveedores.paginator = this.paginator;
  }

  getProveedores() {
    $('.loading').removeClass('hide-loading');
    this.arrayProveedores.data = [];
    this.apiServices.get('administracion/getProveedores')
      .subscribe((res) => {
        for (const key in res) {
          const element = res[key];
          this.arrayProveedores.data = [...this.arrayProveedores.data, element];
        }
        $('.loading').addClass('hide-loading');
      },
      (err:HttpErrorResponse) => this.util.messageError(err));
  }

  getProveedor(id: number, showModal: boolean) {
    this.provModal.getProveedor(id, showModal);
  }

  addProvider() {
    this.provModal.addProveedor();
  }

  applyFilter(filterValue: string) {
    this.arrayProveedores.filter = filterValue.trim().toLowerCase();
  }

  addTo(objData: any) {
    let prov = this.arrayProveedores.data.find((prov) => prov.id == objData.provedor.id);
    if (prov != undefined) {
      prov.celular = objData.provedor.celular;
      prov.email = objData.provedor.email;
      prov.identificacion = objData.provedor.identificacion;
      prov.municipio = (objData.provedor.municipio.nombre.split('(')[0]).trim();
      prov.nombre = objData.provedor.nombre;
      prov.razonSocial = objData.provedor.razonSocial;
      prov.telefono = objData.provedor.telefono;
      prov.tipoProveedor = objData.tipos.substr(0, objData.tipos.length - 1);
    }
  }

}
