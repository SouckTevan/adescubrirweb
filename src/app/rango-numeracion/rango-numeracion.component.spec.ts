import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RangoNumeracionComponent } from './rango-numeracion.component';

describe('RangoNumeracionComponent', () => {
  let component: RangoNumeracionComponent;
  let fixture: ComponentFixture<RangoNumeracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangoNumeracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangoNumeracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
