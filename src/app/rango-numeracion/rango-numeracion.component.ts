import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { RangoNumeracion } from '../models/admin/rango-numeracion/rango-numeracion';
declare var $:any;

@Component({
  selector: 'app-rango-numeracion',
  templateUrl: './rango-numeracion.component.html',
  styleUrls: ['./rango-numeracion.component.scss']
})
export class RangoNumeracionComponent implements OnInit {

  rango: RangoNumeracion;

  // -------------- Array para llenar la tabla principal
  arrayRNum = new MatTableDataSource<RangoNumeracion>();

  // -------------- Columnas
  showCols: string[] = ['id','nombre','estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.rango = new RangoNumeracion();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayRNum.paginator = this.paginator;
    // ---------- Sort
    this.arrayRNum.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getRangosNum')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayRNum.data = resp;
      $('.loading').addClass('hide-loading');
    }, (err) => this.util.messageError(err));
  }

  add() {
    this.rango = new RangoNumeracion();
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    $('.modal-body').animate({scrollTop: $('.modal-body').offset().top}, 1000);
    this.apiServices.get('administracion/getRangoNum/'+id)
    .subscribe((resp: RangoNumeracion) => {
      this.rango = resp;
      $('.loading').addClass('hide-loading');
      $('#modalRNumeracion').modal('show');
    }, (err) => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.rango, 'administracion/saveRangoNum')
      .subscribe((resp: RangoNumeracion) => {
        this.rango = resp;
        $('#modalRNumeracion').modal('hide');
        $('.loading').addClass('hide-loading');
        this.util.notification('success', '', 'Datos Guardados!');
        this.get();
      }, (err) => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.rango.nombre != '';
    const es = this.rango.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayRNum.data != undefined && this.arrayRNum.data.length > 0)
      this.util.exportAsExcelFile(this.arrayRNum.data, 'Rango Numeración');
  }

  applyFilter(filterValue: string) {
    this.arrayRNum.filter = filterValue.trim().toLowerCase();
  }

}
