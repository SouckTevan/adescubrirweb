import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGeneralComponent } from './select-general.component';

describe('SelectGeneralComponent', () => {
  let component: SelectGeneralComponent;
  let fixture: ComponentFixture<SelectGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
