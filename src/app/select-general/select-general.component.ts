import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';

@Component({
  selector: 'select-general',
  templateUrl: './select-general.component.html',
  styleUrls: ['./select-general.component.scss']
})
export class SelectGeneralComponent implements OnInit {

  @Input()
  picklistId: string;

  @Input()
  picklistPadre: string = '';

  @Input()
  tipoDato: string = '';

  @Input()
  val: any;

  valTemp: any = 0;

  @Input()
  estado: any = '';
  
  @Input()
  isDependes = false;
  
  @Input()
  read: boolean;

  @Input()
  css: string = '';

  @Input()
  valObj: any = '';

  @Output()
  valueChange: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  valueObjChange: EventEmitter<any> = new EventEmitter<any>();

  data: any = [];

  @Input()
  set _val(value: any) {
    this.val = value;
    this.valTemp = value;
  }

  get _val(): any { return this.val; }

  @Input()
  set _read(value: any) {
    this.read = value;
  }

  get _read(): any { return this.read; }

  constructor(private apiService: AdescubrirService) { }

  ngOnInit() {
    this.validarDefault()
  }

  validarDefault() {
    this.data = [];
    if (this.tipoDato != '') {
      let restUrl = '';
      switch (this.tipoDato) {
        case 'tipoIdentificacion': 
          this.data = [{option: 'Seleccione una opcion...', val: 0},{option: 'Cédula', val: 'CC'},{option: 'Nit', val: 'Nit'}];
          break;

        case 'aerolinea':
          restUrl = 'administracion/getAerolineas';
          break;

        case 'tipoProveedor':
          restUrl = 'administracion/getTiposProveedor';
          break;

        case 'tipoContacto':
          restUrl = 'administracion/getTiposContactos';
          break;

        case 'departamento':
          restUrl = 'administracion/getDepartamentos';
          break;

        case 'municipio':
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getMunicipioByDep/'+this.val;
          break;

        case 'veredas':
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getVeredasByMunicipio/'+this.val;
          break;

        case 'resguardos':
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getResguardosByMunicipio/'+this.val;
          break;

        case 'kumpanias':
          restUrl = 'administracion/getKumpanias';
          break;
        
        case 'consejoComunitarios':
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getConsejosComByMunicipio/'+this.val;
          break;
        
        case 'kumpanias':
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getKumpaniasByMunicipio/'+this.val;
          break;

        case 'banco':
          restUrl = 'administracion/getBancos';
          break;

        case 'tipoTarifa':
          restUrl = 'administracion/getTipoTarifa';
          break;
        
          case 'tipoEventos':
          restUrl = 'eventos/getTiposEventos';
          break;

        case 'tipoConcepto':
          restUrl = 'administracion/getTiposConceptos';
          break;

        case 'concepto': 
          if (this.val != null && this.val != undefined)
            restUrl = 'administracion/getConceptoByTpConcepto/'+this.val;
          break;

        case 'indicadorRetencion':
          restUrl = 'administracion/getIndicadoresRetencion';
          break;

        case 'estados':
          restUrl = 'administracion/getEstados';
          break;

        case 'usuarios':
          restUrl = 'administracion/getUsuarios';
          break;

        case 'documentos':
          restUrl = 'administracion/getDocumentos';
          break;
      }

      if (this.tipoDato !== 'tipoIdentificacion' && restUrl != '') {
        this.data.push({option: 'Seleccione una opcion...', val: 0});
        this.apiService.get(restUrl)
          .subscribe((res) => {
            if (res != null) {
              res.sort((a:any, b:any) => (a.nombre > b.nombre) ? 1 : ((b.nombre > a.nombre) ? -1 : 0));
              this.valObj = res;
              if (this.estado != '') {
                for (const iterator of res) {
                  if (this.estado == iterator.estado)
                  this.data.push({option: iterator.nombre, val: iterator.id});
                }
              }else {
                for (const iterator of res) {
                  this.data.push({option: iterator.nombre, val: iterator.id});
                }
              }
              this.val = this.valTemp;
            }
          });
      }
      this.val = 0;
    }
  }

  onChange(event: any) {
    if (this.valueObjChange != undefined && event.target != undefined) {
      this.valueObjChange.emit(this.valObj[(event.target.selectedIndex)-1]);
    }
  }

  onChangePadre(opcionPadre: any) {
    this.reset();
    this.val = opcionPadre;
    this.validarDefault()
  }

  backData(val: any, opcion: string): any[] {
    this.reset()
    this.val = val;
    this.tipoDato = opcion;
    this.validarDefault();
    return this.valObj;
  }

  reset() {
    this.data = [];
    this.data.push({option: 'Seleccione una opcion...', val: 0});
    this._val = 0;
  }
}
