import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SnotifyModule, ToastDefaults, SnotifyService } from 'ng-snotify';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { AuthGuard } from '../auth.guard';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { WriteSearchComponent } from '../write-search/write-search.component';
import { CustomSelectComponent } from '../custom-select/custom-select.component';
import { RouterModule } from '@angular/router';
import { FilterTablePipe } from '../filter-table.pipe';
import { ProvTemplateComponent } from '../prov-template/prov-template.component';
import { EventoTemplateComponent } from '../evento-template/evento-template.component';
// import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    SelectGeneralComponent,
    WriteSearchComponent,
    CustomSelectComponent,
    ProvTemplateComponent,
    EventoTemplateComponent,
    FilterTablePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // HttpClientModule,
    SnotifyModule,
    FormsModule,
    RouterModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    NgxDropzoneModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    SnotifyModule,
    // HttpClientModule,
    FormsModule,
    MatTableModule,
    RouterModule,
    MatSortModule,
    FilterTablePipe,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    NgxDropzoneModule,
    SelectGeneralComponent,
    ProvTemplateComponent,
    WriteSearchComponent,
    CustomSelectComponent,
    EventoTemplateComponent
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    DatePipe,
    AuthGuard,
    FilterTablePipe,
    SelectGeneralComponent,
    SnotifyService
  ]
})
export class SharedModule { }
