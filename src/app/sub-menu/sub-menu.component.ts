import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService } from '../utility.service';
declare var $: any;

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss']
})
export class SubMenuComponent implements OnInit {

  // @Input()
  _item: any = {};
  read: boolean = false;
  departamento: any;

  private itemSelected: number = -1;

  listItems: any;

  // ------ When I need to detect the change of item
  get item(): any {
    return this._item;
  }

  @Input()
  set item(val: any) {
    this.changeViewMenu(val);
  }

  constructor(public router: Router, private route:ActivatedRoute, public util: UtilityService) {
  }

  ngOnInit() {
    this.jqueryFunction();
  }

  jqueryFunction() {
    // const that = this;

    // $(document).ready(function() {

      // $('[data-toggle="popover"]').popover();

      // $('body').on('inserted.bs.popover', function(e: any) {
      //   e.preventDefault();
      //   $('.popover').find('.popover-body').css({'box-shadow': '2px 4px 10px -1px #777'}).find('ul').css({padding: 0, margin: 0, width: '180px'}) //180px
      //   .find('li').css({'list-style': 'none', 'font-family': 'oswald-regular',
      //     padding: '2%', 'white-space': 'nowrap', cursor: 'pointer'}).mouseenter(function() {
      //       $(this).css({background: '#dadddf', color: '#293462'});
      //     }).mouseleave(function() {
      //       $(this).css({background: 'unset', color: '#000'});
      //     });

      //   $('.subMenu-p').on('click', 'li', function(event: any) {
      //     var txt = event.target.innerText;
      //     var itm = '';
      //     switch(txt.substring(1,txt.length)) {
      //         case 'Tipos de Concepto':            itm = 'tipoConcepto';       break;
      //         case 'Tipos de Evento':              itm = 'tipoEvento';         break;
      //         case 'Tipos de Proveedor':           itm = 'tipoProveedor';      break;
      //         case 'Tipos de Tarifa':              itm = 'tipoTarifa';         break;
      //         case 'Tipos de Contacto':            itm = 'tipoContacto';       break;
      //         case 'Tipos de Documento':           itm = 'tipoDocumento';      break;
      //         case 'Entidades Financieras':        itm = 'entidadFinanciera';  break;
      //         case 'Rangos de Numeración':         itm = 'rangoNumeracion';    break;
      //         case 'Tipos de Documentos Contable': itm = 'documentoContable';  break;
      //         case 'Indicadores de Retención':     itm = 'indicadorRetencion'; break;
      //         case 'Medios de Pago':               itm = 'mediosPago';         break;
      //     }
      //     that.navigate(itm);
      //     $('.popover').popover('hide');
      //   });
      // });

      $('.panel-sub-menu').on('click', 'ul li', function(e) {
        e.preventDefault();
        var li = $(this);

        if (!li.hasClass('_content-menu')) {
          $('.panel-sub-menu ul li').removeClass('selected-item').find('i.fa-caret-right').remove();
          li.addClass('selected-item');
          $('<i class="fas fa-caret-right mr-2"></i> ').insertBefore(li.find('i'));

            if ($('.panel-sub-menu').hasClass('show-sub-menu')) {
              $('.panel-sub-menu').removeClass('show-sub-menu');
            }else {
              $('.panel-sub-menu').addClass('show-sub-menu');
            }
        }else {
          $('._params-menu').toggleClass('open-menu');
          if (!$('._params-menu').hasClass('open-menu')) {
            // $('.panel-sub-menu').removeClass('show-sub-menu');
          }
        }

      })
    // });
  }

  changeViewMenu(itemToShow: any) {
    this._item = itemToShow;
    if (this.itemSelected != -1) {
      // setTimeout(function() {
        // $('.sub-menu-content li i.fa-caret-right').remove();
        // $('.sub-menu-content li:eq('+this.itemSelected+')').addClass('active-sub-item').prepend('<i class="fas fa-caret-right"></i> ');
      // },3000)
    }
  }

}
