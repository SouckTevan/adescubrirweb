import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { TesoreriaRoutingModule } from './tesoreria-routing.module';
import { ListaPagosComponent } from '../lista-pagos/lista-pagos.component';

@NgModule({
  declarations: [ListaPagosComponent],
  imports: [
    CommonModule,
    SharedModule,
    TesoreriaRoutingModule
  ]
})
export class TesoreriaModule { }
