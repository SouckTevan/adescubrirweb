import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoDocumentoContableComponent } from './tipo-documento-contable.component';

describe('TipoDocumentoContableComponent', () => {
  let component: TipoDocumentoContableComponent;
  let fixture: ComponentFixture<TipoDocumentoContableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoDocumentoContableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoDocumentoContableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
