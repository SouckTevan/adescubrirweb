import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { TipoDocumentoContable } from '../models/contable/tipo-documento-contable/tipo-documento-contable';
import { UtilityService } from '../utility.service';
declare var $: any;

@Component({
  selector: 'app-tipo-documento-contable',
  templateUrl: './tipo-documento-contable.component.html',
  styleUrls: ['./tipo-documento-contable.component.scss']
})
export class TipoDocumentoContableComponent implements OnInit {

  tipoDocContable: TipoDocumentoContable = new TipoDocumentoContable();;

  // -------------- Array para llenar la tabla principal
  arrayDocContable = new MatTableDataSource<TipoDocumentoContable>();
  dataRangoNumeracion = [];

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit(): void {
    this.get();
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getDocumentosContables')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayDocContable.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  getNumeracion() {
    this.apiServices.get('administracion/getRangosNum')
    .subscribe((res: any) => {
      res.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.dataRangoNumeracion = res;
    }, err => this.util.messageError(err));
  }

  add() {
    this.tipoDocContable = new TipoDocumentoContable();
    this.getNumeracion();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoDocumentoContable/'+id)
    .subscribe((resp: TipoDocumentoContable) => {
      this.getNumeracion();
      this.tipoDocContable = resp;
      $('.loading').addClass('hide-loading');
      $('#modalDocumentoContable').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');

    if (this.validate()) {
      this.apiServices.post(this.tipoDocContable, 'administracion/saveDocumentoContable')
      .subscribe((resp: TipoDocumentoContable) => {
        this.tipoDocContable = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalDocumentoContable').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoDocContable.nombre != '';
    const es = this.tipoDocContable.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayDocContable.data != undefined && this.arrayDocContable.data.length > 0)
      this.util.exportAsExcelFile(this.arrayDocContable.data, 'DocumentoContable');
  }

  applyFilter(filterValue: string) {
    this.arrayDocContable.filter = filterValue.trim().toLowerCase();
  }
}
