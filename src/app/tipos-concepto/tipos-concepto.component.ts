import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { TipoConcepto } from '../models/admin/tipo-concepto/tipo-concepto';
declare var $: any;

@Component({
  selector: 'app-tipos-concepto',
  templateUrl: './tipos-concepto.component.html',
  styleUrls: ['./tipos-concepto.component.scss']
})
export class TiposConceptoComponent implements OnInit {

  tipoConcepto: TipoConcepto;

  // -------------- Array para llenar la tabla principal
  arrayTipoConcepto = new MatTableDataSource<TipoConcepto>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.tipoConcepto = new TipoConcepto();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayTipoConcepto.paginator = this.paginator;
    // ---------- Sort
    this.arrayTipoConcepto.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTiposConceptos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayTipoConcepto.data = resp;
      $('.loading').addClass('hide-loading');
    }, (err) => this.util.messageError(err));
  }

  add() {
    this.tipoConcepto = new TipoConcepto();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoConcepto/'+id)
    .subscribe((resp: TipoConcepto) => {
      this.tipoConcepto = resp;
      $('.loading').addClass('hide-loading');
      $('#modalTiposConcepto').modal('show');
    }, (err) => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.tipoConcepto, 'administracion/saveTipoConcepto')
      .subscribe((resp: TipoConcepto) => {
        this.tipoConcepto = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalTiposConcepto').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, (err) => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoConcepto.nombre != '';
    const es = this.tipoConcepto.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayTipoConcepto.data != undefined && this.arrayTipoConcepto.data.length > 0)
      this.util.exportAsExcelFile(this.arrayTipoConcepto.data, 'TipoConcepto');
  }

  applyFilter(filterValue: string) {
    this.arrayTipoConcepto.filter = filterValue.trim().toLowerCase();
  }

}
