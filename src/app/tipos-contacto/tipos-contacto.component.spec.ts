import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposContactoComponent } from './tipos-contacto.component';

describe('TiposContactoComponent', () => {
  let component: TiposContactoComponent;
  let fixture: ComponentFixture<TiposContactoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposContactoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposContactoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
