import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { TipoContacto } from '../models/admin/tipo-contacto/tipo-contacto';
declare var $: any;

@Component({
  selector: 'app-tipos-contacto',
  templateUrl: './tipos-contacto.component.html',
  styleUrls: ['./tipos-contacto.component.scss']
})
export class TiposContactoComponent implements OnInit {

  tipoContacto: TipoContacto;

  // -------------- Array para llenar la tabla principal
  arrayTipoContacto = new MatTableDataSource<TipoContacto>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.tipoContacto = new TipoContacto();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayTipoContacto.paginator = this.paginator;
    // ---------- Sort
    this.arrayTipoContacto.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTiposContactos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayTipoContacto.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.tipoContacto = new TipoContacto();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoContacto/'+id)
    .subscribe((resp: TipoContacto) => {
      this.tipoContacto = resp;
      $('#modalTipoContacto').modal('show');
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.tipoContacto, 'administracion/saveTipoContacto')
      .subscribe((resp: TipoContacto) => {
        this.tipoContacto = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalTipoContacto').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoContacto.nombre != '';
    const es = this.tipoContacto.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayTipoContacto.data != undefined && this.arrayTipoContacto.data.length > 0)
      this.util.exportAsExcelFile(this.arrayTipoContacto.data, 'TipoContacto');
  }

  applyFilter(filterValue: string) {
    this.arrayTipoContacto.filter = filterValue.trim().toLowerCase();
  }

}
