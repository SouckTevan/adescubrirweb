import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { Documento } from '../models/admin/documento/documento';
declare var $:any;

@Component({
  selector: 'app-tipos-documento',
  templateUrl: './tipos-documento.component.html',
  styleUrls: ['./tipos-documento.component.scss']
})
export class TiposDocumentoComponent implements OnInit {

  documento: Documento;

  // -------------- Array para llenar la tabla principal
  arrayDocumento = new MatTableDataSource<Documento>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.documento = new Documento();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayDocumento.paginator = this.paginator;
    // ---------- Sort
    this.arrayDocumento.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getDocumentos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayDocumento.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.documento = new Documento();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getDocumento/'+id)
    .subscribe((resp: Documento) => {
      this.documento = resp;
      $('.loading').addClass('hide-loading');
      $('#modalDocumento').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.documento, 'administracion/saveDocumento')
      .subscribe((resp: Documento) => {
        this.documento = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalDocumento').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.documento.nombre != '';
    const es = this.documento.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayDocumento.data != undefined && this.arrayDocumento.data.length > 0)
      this.util.exportAsExcelFile(this.arrayDocumento.data, 'Documento');
  }

  applyFilter(filterValue: string) {
    this.arrayDocumento.filter = filterValue.trim().toLowerCase();
  }

}
