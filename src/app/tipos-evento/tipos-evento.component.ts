import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { TipoEvento } from '../models/eventos/tipo-evento/tipo-evento';
declare var $: any;

@Component({
  selector: 'app-tipos-evento',
  templateUrl: './tipos-evento.component.html',
  styleUrls: ['./tipos-evento.component.scss']
})
export class TiposEventoComponent implements OnInit {

  tipoEvento: TipoEvento;

  // -------------- Array para llenar la tabla principal
  arrayTipoEvento = new MatTableDataSource<TipoEvento>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.tipoEvento = new TipoEvento();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayTipoEvento.paginator = this.paginator;
    // ---------- Sort
    this.arrayTipoEvento.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoEventos')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayTipoEvento.data = resp;
      $('.loading').addClass('hide-loading');
    }, (err) => this.util.messageError(err));
  }

  add() {
    this.tipoEvento = new TipoEvento();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoEvento/'+id)
    .subscribe((resp: TipoEvento) => {
      this.tipoEvento = resp;
      $('#modalTipoEvento').modal('show');
      $('.loading').addClass('hide-loading');
    }, (err) => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.tipoEvento, 'administracion/saveTipoEvento')
      .subscribe((resp: TipoEvento) => {
        this.tipoEvento = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalTipoEvento').modal('hide');
        this.get();
        $('.loading').addClass('hide-loading');
      }, (err) => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoEvento.nombre != '';
    const es = this.tipoEvento.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayTipoEvento.data != undefined && this.arrayTipoEvento.data.length > 0)
      this.util.exportAsExcelFile(this.arrayTipoEvento.data, 'TipoEvento');
  }

  applyFilter(filterValue: string) {
    this.arrayTipoEvento.filter = filterValue.trim().toLowerCase();
  }

}
