import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposProveedorComponent } from './tipos-proveedor.component';

describe('TiposProveedorComponent', () => {
  let component: TiposProveedorComponent;
  let fixture: ComponentFixture<TiposProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
