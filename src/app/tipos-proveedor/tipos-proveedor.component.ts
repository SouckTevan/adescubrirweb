import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { TipoProveedor } from '../models/admin/tipo-proveedor/tipo-proveedor';
declare var $: any;

@Component({
  selector: 'app-tipos-proveedor',
  templateUrl: './tipos-proveedor.component.html',
  styleUrls: ['./tipos-proveedor.component.scss']
})
export class TiposProveedorComponent implements OnInit {

  tipoProveedor: TipoProveedor;

  // -------------- Array para llenar la tabla principal
  arrayTipoProveedor = new MatTableDataSource<TipoProveedor>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.tipoProveedor = new TipoProveedor();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayTipoProveedor.paginator = this.paginator;
    // ---------- Sort
    this.arrayTipoProveedor.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTiposProveedor')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayTipoProveedor.data = resp;
      $('.loading').addClass('hide-loading');
    }, (err) => this.util.messageError(err));
  }

  add() {
    this.tipoProveedor = new TipoProveedor();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoProveedor/'+id)
    .subscribe((resp: TipoProveedor) => {
      this.tipoProveedor = resp;
      $('.loading').addClass('hide-loading');
      $('#modalTipoProveedor').modal('show');
    }, (err) => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.tipoProveedor, 'administracion/saveTipoProveedor')
      .subscribe((resp: TipoProveedor) => {
        this.tipoProveedor = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalTipoProveedor').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, (err) => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoProveedor.nombre != '';
    const es = this.tipoProveedor.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayTipoProveedor.data != undefined && this.arrayTipoProveedor.data.length > 0)
      this.util.exportAsExcelFile(this.arrayTipoProveedor.data, 'TipoProveedor');
  }

  applyFilter(filterValue: string) {
    this.arrayTipoProveedor.filter = filterValue.trim().toLowerCase();
  }

}
