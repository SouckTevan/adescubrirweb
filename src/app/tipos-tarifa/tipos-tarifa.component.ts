import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdescubrirService } from '../adescubrir.service';
import { UtilityService } from '../utility.service';
import { TipoTarifa } from '../models/admin/tipo-tarifa/tipo-tarifa';
declare var $: any;

@Component({
  selector: 'app-tipos-tarifa',
  templateUrl: './tipos-tarifa.component.html',
  styleUrls: ['./tipos-tarifa.component.scss']
})
export class TiposTarifaComponent implements OnInit {

  tipoTarifa: TipoTarifa;

  // -------------- Array para llenar la tabla principal
  arrayTipoTarifa = new MatTableDataSource<TipoTarifa>();

  // -------------- Columnas
  showColsActivos: string[] = ['id', 'nombre', 'estado','accion'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private apiServices: AdescubrirService, private util: UtilityService) {
    this.tipoTarifa = new TipoTarifa();
  }

  ngOnInit() {
    this.get();

    // ---------- Paginador
    this.arrayTipoTarifa.paginator = this.paginator;
    // ---------- Sort
    this.arrayTipoTarifa.sort = this.sort;
  }

  get() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoTarifa')
    .subscribe((resp) => {
      resp.sort((a:any, b:any) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
      this.arrayTipoTarifa.data = resp;
      $('.loading').addClass('hide-loading');
    }, err => this.util.messageError(err));
  }

  add() {
    this.tipoTarifa = new TipoTarifa();
  }

  open(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.apiServices.get('administracion/getTipoTarifaById/'+id)
    .subscribe((resp: TipoTarifa) => {
      this.tipoTarifa = resp;
      $('.loading').addClass('hide-loading');
      $('#modalTipoTarifa').modal('show');
    }, err => this.util.messageError(err));
  }

  save() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if (this.validate()) {
      this.apiServices.post(this.tipoTarifa, 'administracion/saveTipoTarifa')
      .subscribe((resp: TipoTarifa) => {
        this.tipoTarifa = resp;
        this.util.notification('success', '', 'Datos Guardados!');
        $('#modalTipoTarifa').modal('hide');
        $('.loading').addClass('hide-loading');
        this.get();
      }, err => this.util.messageError(err));
    }else {
      $('.loading').addClass('hide-loading');
      this.util.notification('warning', '', 'Los campos marcados con "*" son obligatorios');
    }
  }

  validate(): boolean {
    const nm = this.tipoTarifa.nombre != '';
    const es = this.tipoTarifa.estado != '';

    return nm && es;
  }

  exportExcel() {
    if (this.arrayTipoTarifa.data != undefined && this.arrayTipoTarifa.data.length > 0)
      this.util.exportAsExcelFile(this.arrayTipoTarifa.data, 'TipoTarifa');
  }

  applyFilter(filterValue: string) {
    this.arrayTipoTarifa.filter = filterValue.trim().toLowerCase();
  }

}
