import { Component, OnInit, ViewChild } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SelectGeneralComponent } from '../select-general/select-general.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UtilityService } from '../utility.service';
import { Usuario } from '../models/usuario/usuario';
import { Rol } from '../models/admin/security/rol/rol';
import { UsuarioOpcion } from '../models/admin/security/usuario-opcion/usuario-opcion';

declare var $: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  // --------- Instancia de objetos
  usuario: Usuario = new Usuario();

  // -------- arrays para llenar las tablas
  arrayUsuarios = new MatTableDataSource<Usuario>();
  departamento: any = 0;

  rols = [];
  rolesTemp = [];
  dataCurrentOptions = [];
  options = [];
  optionsTemp = [];
  dataCurrentRoles = [];
  searchAllOptions = '';
  searchCurOptions = '';
  searchAllRol = '';
  searchCurRol = '';
  DIR_RIGHT = 'right';
  currentDirection = 'right';
  DIR_LEFT = 'left';

  dataEstado: any = [{option: 'Seleccione una opcion...', val: ''},{option: 'Activo', val: 'A'},{option: 'Inactivo', val: 'I'},{option: 'Para Borrar', val: 'B'}];

  // ----------- Columnas ---------
  showColsUsuarios: string[] = ['nombre', 'usuario', 'telefono', 'estado','accion']

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('municipioComponent', {static: true}) municipioComponent: SelectGeneralComponent;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  sortedData: Usuario[];

  constructor(public apiServices: AdescubrirService, private util: UtilityService) { }

  ngOnInit() {
    this.reset();
    this.getRoles();
    this.getOpciones();
    this.getUsuarios();
    //this.arrayUsuarios.sort = this.sort;
    // ---------- Paginador
    // this.arrayUsuarios.paginator = this.paginator;
  }

  add() {
    $('#nav-tab a:first-child').tab('show');
    this.reset();
    this.getRoles();
    this.getOpciones();
  }

  getUsuarios() {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    this.arrayUsuarios.data = [];
    this.apiServices.get('administracion/getUsuarios')
      .subscribe((resp) =>{
        // for(const key in resp) {
        //   const element = resp[key];
        //   this.arrayUsuarios.data = [...this.arrayUsuarios.data, element];
        // }
        this.arrayUsuarios = new MatTableDataSource(resp);
        this.arrayUsuarios.sort = this.sort;
        $('.loading').addClass('hide-loading');
      },
      (err:HttpErrorResponse)=> this.util.messageError(err));
  }

  getRoles() {
    this.apiServices.get('administracion/getRoles')
      .subscribe((res) => {
        this.rols = res;
      },
      (err:HttpErrorResponse) => this.util.messageError(err));
  }

  getOpciones() {
    this.apiServices.get('administracion/getOpciones')
      .subscribe((res) => {
        this.options = res;
      },
      (err:HttpErrorResponse)=> this.util.messageError(err));
  }

  changeSelectGeneral(id: number, tabla: string) {
    this.municipioComponent.onChangePadre(id);
  }

  save(){
    $('.loading').removeClass('hide-loading').find('.text-load').text('Guardando...');
    if(this.validate()) {
      this.usuario.roles = [];
      this.usuario.opciones = [];
      for (const it of this.dataCurrentRoles) {
        let rol: Rol = new Rol();
        rol.id = it.id;
        this.usuario.roles.push(rol);
      }
      for (const it of this.dataCurrentOptions) {
        let opcion: UsuarioOpcion = new UsuarioOpcion();
        opcion.opcionId = it.nombre;
        this.usuario.opciones.push(opcion);
      }
      this.apiServices.post(this.usuario, 'administracion/saveUsuario')
      .subscribe(() => {
          this.util.notification('success', '', 'Usuario Guardado!.');
          $('#modalProvider').modal('hide');
          this.getUsuarios();
          $('.loading').addClass('hide-loading');
        },
        err => { this.util.messageError(err); });
    }else{
      $('.loading').addClass('hide-loading');
      this.util.notification
    }

  }

  validate(){
    let bandera: boolean = true;
    if (this.usuario.identificacion == ''){bandera = false;}
    if (this.usuario.nombres == ''){bandera = false;}
    if (this.usuario.municipio_id == 0){bandera = false;}
    if (this.usuario.direccion == ''){bandera = false;}
    if (this.usuario.usuario == ''){bandera = false;}
    if (this.usuario.clave == ''){bandera = false;}
    if (this.usuario.estado == ''){bandera = false;}
    return bandera;
  }

  reset(){
    this.usuario = new Usuario();
    this.dataCurrentRoles = [];
    this.dataCurrentOptions = [];
  }

  getUsuario(id: number) {
    $('.loading').removeClass('hide-loading').find('.text-load').text('Cargando...');
    $('#nav-tab a:first-child').tab('show');
    this.reset();
    this.apiServices.get('administracion/getUsuario/'+id+'/true')
      .subscribe((res) => {
        this.usuario = res;
        this.departamento = this.usuario.municipio.departamento_id;
        this.changeSelectGeneral(this.usuario.municipio.departamento_id, 'datosGenerales');

        if(res.roles == undefined) { res.roles = []; }

        this.dataCurrentRoles = this.rols.filter(r =>
          res.roles.some((rA: any) => {
            if (r.nombre == rA.nombre) {
              let i = this.rols.indexOf(r);
              this.rols = this.rols.slice(0, i).concat(this.rols.slice(i + 1, this.rols.length));
              return true;
            }
          })
        );

        this.dataCurrentOptions = this.checkRolsOptions(res.opciones, this.options, 'opcionId');
        $('#modalProvider').modal('show');
        $('.loading').addClass('hide-loading');
      },
      (err:HttpErrorResponse) => this.util.messageError(err));
  }

  checkRolsOptions(current: [], allItems: any[], field: string): any[] {
    return allItems = allItems.filter((o: any) =>
      current.some((c: any) => {
        if (o.nombre == c[field]) {
          let i = allItems.indexOf(o);
          allItems = allItems.slice(0, i).concat(allItems.slice(i + 1, allItems.length));
          return true;
        }
      })
    );
  }

  applyFilter(filterValue: string) {
    this.arrayUsuarios.filter = filterValue.trim().toLowerCase();
  }

  // ---------- Move options
  moveAllItems(direction: string, isOption: boolean) {
    if (direction === this.DIR_RIGHT) {
      if (isOption) {
        for (const itm of this.options) {
          this.dataCurrentOptions.push(itm);
        }
        this.options = [];
      }else {
        for (const itm of this.rols) {
          this.dataCurrentRoles.push(itm);
        }
        this.rols = [];
      }
    }else if (direction === this.DIR_LEFT) { // ---- Move option(s) to
      if (isOption) {
        for (const itm of this.dataCurrentOptions) {
          this.options.push(itm);
        }
        this.dataCurrentOptions = [];
      } else {
        for (const itm of this.dataCurrentRoles) {
          this.rols.push(itm);
        }
        this.dataCurrentRoles = [];
      }
    }
  }

  addToMoveItem(opt: any, direction: string, elem: any, isOption: boolean) {
    this.currentDirection = direction;
    let tempArr = [];

    (isOption) ? tempArr = this.optionsTemp : tempArr = this.rolesTemp;

    let index = tempArr.indexOf(opt);
    if (index === -1) {
      elem.target.classList.add('selected-li');
      tempArr.push(opt);
    }else {
      elem.target.classList.remove('selected-li');
      tempArr.splice(index, 1);
    }

    (isOption) ? this.optionsTemp = tempArr : this.rolesTemp = tempArr;
  }

  moveItem(direction: string, isOption: boolean) {
    if (this.currentDirection === direction) {
      if (isOption) {
        for (const opt of this.optionsTemp) {
          if (direction === this.DIR_RIGHT) { // --- Add Option(s)
            this.dataCurrentOptions.push(opt);
            this.options = this.options.filter((dt) => dt.id !== opt.id);
          }else if (direction === this.DIR_LEFT) { // ---- Remove Option(s)
            this.options.push(opt);
            this.dataCurrentOptions = this.dataCurrentOptions.filter((dt) => dt.id !== opt.id);
          }
        }
        this.optionsTemp = [];
      }else {
        for (const rl of this.rolesTemp) {
          if (direction === this.DIR_RIGHT) { // --- Add Rol(s)
            this.dataCurrentRoles.push(rl);
            this.rols = this.rols.filter((dt) => dt.id !== rl.id);
          }else if (direction === this.DIR_LEFT) { // ---- Remove Rol(s)
            this.rols.push(rl);
            this.dataCurrentRoles = this.dataCurrentRoles.filter((dt) => dt.id !== rl.id);
          }
        }
        this.rolesTemp = [];
      }
    }
  }
}
