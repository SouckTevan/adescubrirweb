import { Injectable } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(private snotify: SnotifyService) { }

  public notification(tipo: string, titulo: string, mensaje: string) {
    const config: any = {
      timeout: 2000,
      showProgressBar: true,
      closeOnClick: false,
      position: 'rightTop',
      pauseOnHover: true
    };
    switch (tipo) {
      case 'success':
        this.snotify.success(mensaje, titulo, config);
        break;

      case 'info':
        this.snotify.info(mensaje, titulo, config);
        break;

      case 'warning':
        this.snotify.warning(mensaje, titulo, config);
        break;

      case 'error':
        this.snotify.error(mensaje, titulo, config);
        break;
    }
  }

  public containsOption(opcion: string) {
    if (localStorage.getItem('usuario') != null) {
      //const opciones = JSON.parse(localStorage.getItem('usuario')).opciones;
      const opciones = JSON.parse(localStorage.getItem('usuario')).listaOpciones;
      /*for (const op of opciones) {
        if (op.opcionId === opcion || op.opcionId === 'ADESCUBRIR_ALL') {
          return true;
        }
      }*/
      for (const op of opciones) {
        if (op === opcion || op === 'ADESCUBRIR_ALL') {
          return true;
        }
      }
    }
    return false;
  }

  miles(num: any) {
    num = String(num).replace(/\D/g, '');
    return (num === '') ? num : Number(num).toLocaleString('en-CO').split(',').join('.');
  }

  removeMiles(num: any) {
    // if (num != null && num != undefined && num.includes('.')) {
    if (num != null && num != undefined && num.toString().includes('.')) {
      num = num.replace(/\./g, '');
    }
    if (num != null && num != undefined && num.toString().includes(',')) {
      num = num.replace(/\,/g, '');
    }
    return num;
  }

  validateField(amount: any, amountMax: any): boolean {
    const amountWithoutMiles = this.removeMiles(amount);
    return (amountWithoutMiles != '' && amountWithoutMiles <= amountMax) ? false : true;
  }

  validateOnlyNumbers(e: any) {
    var code = e.which || e.keyCode,
    allowedKeys = [8, 9, 13, 27, 35,36,37,38,39,46,110, 190];

    if(allowedKeys.indexOf(code) > -1) {
      return;
    }

    if((e.shiftKey || (code < 48 || code > 57)) && (code < 96 || code > 105)) {
      e.preventDefault();
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsFile(excelBuffer, excelFileName, EXCEL_TYPE, EXCEL_EXTENSION, true);
  }

  saveAsFile(buffer: any, fileName: string, _type: string, extension: string, isDownload: boolean): any {
    console.log(isDownload);
    console.log(_type);

    const data: Blob = new Blob([buffer], {type: _type});
    if (isDownload) {
      FileSaver.saveAs(data, fileName+extension);
    }else {
      const url = URL.createObjectURL(data);
      return url;
      // window.open(url,'_blank')
    }
  }

  messageError(err: any) {
    console.log(err);
    $('.loading').addClass('hide-loading');
    this.notification('error','A ocurrido un error','Por favor contactar con el administrador de la plataforma');
  }
}
