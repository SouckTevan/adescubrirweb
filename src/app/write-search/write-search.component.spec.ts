import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteSearchComponent } from './write-search.component';

describe('WriteSearchComponent', () => {
  let component: WriteSearchComponent;
  let fixture: ComponentFixture<WriteSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WriteSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
