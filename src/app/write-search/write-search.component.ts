import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AdescubrirService } from '../adescubrir.service';
import { Persona } from '../models/admin/persona/persona';

@Component({
  selector: 'app-write-search',
  templateUrl: './write-search.component.html',
  styleUrls: ['./write-search.component.scss']
})
export class WriteSearchComponent implements OnInit {

  @Output() word = new EventEmitter<any>();
  @Output() obj = new EventEmitter<any>();

  @Input()
  val: any;

  arrayPersonas: Persona[] = [];
  result: Persona[] = [];
  showContent: boolean = false;

  @Input()
  read: boolean;

  @Input()
  set _read(value: any) { this.read = value; }

  get _read(): any { return this.read; }

  constructor(public apiService: AdescubrirService) { }

  ngOnInit() {
  }

  getData(idSet: any) {
    if (this.arrayPersonas.length === 0) {
      this.apiService.get('administracion/getPersonas')
        .subscribe((res: Persona[]) => {
          this.arrayPersonas = res.filter((dt: Persona) => dt.id !== -1);
          this.loadValue(idSet);
        });
    }else {
      this.loadValue(idSet);
    }
  }

  loadValue(id: any) {
    if (id != 0) {
      const dt = this.arrayPersonas.filter(dt => dt.id === id)[0];
      if (dt !== undefined) {
        this.obj.emit(dt);
        if (dt.nombre !== null && dt.nombre !== '') {
          this.val = dt.nombre+' '+dt.apellidos;
        }else if (dt.razonSocial !== null && dt.razonSocial !== '') {
          this.val = dt.razonSocial;
        }
      }else {
        this.val = '';
      }
    }else {
      this.val = '';
    }
  }

  onKeydown(wd: string) {
    this.result = [];
    if (wd.length > 3) {
      for (const persona of this.arrayPersonas) {
        let flag = false;
        if ((persona.nombre != null && persona.nombre != '') && (persona.nombre.toLowerCase()).match(wd.toLowerCase()) != null) {
          flag = true;
        }
        if ((persona.razonSocial != null && persona.razonSocial != '') && (persona.razonSocial.toLowerCase()).match(wd.toLowerCase()) != null) {
          flag = true;
        }
        if (flag) {
          this.result.push(persona);
          this.showContent = true;
        }
      }
    }else {
      this.showContent = false;
    }
  }

  itemSelected(data: any) {
    this.showContent = !this.showContent;
    if (data.nombre !== null && data.nombre !== '') {
      this.val = data.nombre+' '+data.apellidos;
    }else if (data.razonSocial !== null && data.razonSocial !== '') {
      this.val = data.razonSocial;
    }
    console.log(data);

    this.word.emit(this.val);
    this.obj.emit(data);
  }

  @Input()
  set _setData(id: any) {
    this.getData(id);
  }
}
